package com.android.bodegasadmin.presentation.utils.animation

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.widget.ImageView

class LoadingAnimationImageView constructor(private val ivLoading: ImageView) {

    private var scaleXAnimator: ValueAnimator? = null
    private var scaleYAnimator: ValueAnimator? = null
    private var scaleXAnimatorDecrease: ValueAnimator? = null
    private var scaleYAnimatorDecrease: ValueAnimator? = null
    var scaleIncrease = AnimatorSet()


    init {
        buildAnimators()
        setAnimatorsListeners()
    }

    private fun buildAnimators() {
        scaleXAnimator = ObjectAnimator.ofFloat(ivLoading, "scaleX", 1.0f, 1.4f)
        scaleXAnimator?.duration = 2000

        scaleYAnimator = ObjectAnimator.ofFloat(ivLoading, "scaleY", 1.0f, 1.4f)
        scaleYAnimator?.duration = 2000

        scaleXAnimatorDecrease = ObjectAnimator.ofFloat(ivLoading, "scaleX", 1.4f, 1.0f)
        scaleXAnimatorDecrease?.duration = 2000

        scaleYAnimatorDecrease = ObjectAnimator.ofFloat(ivLoading, "scaleY", 1.4f, 1.0f)
        scaleYAnimatorDecrease?.duration = 2000

    }

    private fun setAnimatorsListeners() {
        scaleIncrease.playTogether(
            scaleXAnimator,
            scaleYAnimator
        )

        val scaleDecrease = AnimatorSet()
        scaleDecrease.playTogether(
            scaleXAnimatorDecrease,
            scaleYAnimatorDecrease
        )
        scaleDecrease.addListener(
            object : Animator.AnimatorListener {
                override fun onAnimationEnd(p0: Animator?) {
                    scaleIncrease.start()
                }

                override fun onAnimationCancel(p0: Animator?) {
                }

                override fun onAnimationStart(p0: Animator?) {
                }

                override fun onAnimationRepeat(p0: Animator?) {
                }

            })

        scaleIncrease.addListener(
            object : Animator.AnimatorListener {
                override fun onAnimationEnd(p0: Animator?) {
                    scaleDecrease.start()
                }

                override fun onAnimationCancel(p0: Animator?) {
                }

                override fun onAnimationStart(p0: Animator?) {
                }

                override fun onAnimationRepeat(p0: Animator?) {
                }

            })
    }

    fun startAnimation() {
        scaleIncrease.start()
    }

}