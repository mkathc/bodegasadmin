package com.android.bodegasadmin.presentation.ordersHistory.adapter

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.row_customer_order.view.*

class EstablishmentOrderViewHolder (itemView: View, viewHolderListener: ViewHolderListener):
    RecyclerView.ViewHolder(itemView) {

    val tvUserName : TextView = itemView.tvStoreName
    val tvTotal : TextView = itemView.tvTotal
    val tvDate : TextView = itemView.tvDate
    val tvTitleSendDate : TextView = itemView.tvTitleSendDate
    val tvSendDate : TextView = itemView.tvSendDate
    val tvDeliveryType : TextView = itemView.tvDeliveryType
    init {
        itemView.setOnClickListener {
            viewHolderListener.onClick(
                layoutPosition
            )
        }
    }

    interface ViewHolderListener {
        fun onClick(
            position: Int
        )
    }
}