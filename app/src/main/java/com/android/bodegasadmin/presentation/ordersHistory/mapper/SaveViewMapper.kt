package com.android.bodegasadmin.presentation.ordersHistory.mapper

interface SaveViewMapper<V, D> {

    fun mapToUseCase(type: V): D

    fun mapToView(type: D): V
}