package com.android.bodegasadmin.presentation.ordersHistory

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.android.bodegasadmin.presentation.ordersHistory.mapper.EstablishmentOrderViewMapper
import com.android.bodegasadmin.domain.establishmentOrders.GetOrdersListByEstablishmentAndState
import com.android.bodegasadmin.domain.establishmentOrders.OrderDetailBody
import com.android.bodegasadmin.domain.establishmentOrders.UpdateOrderStatus
import com.android.bodegasadmin.domain.util.Resource
import com.android.bodegasadmin.domain.util.Status
import com.android.bodegasadmin.presentation.ordersHistory.model.EstablishmentOrderView
import com.android.bodegasadmin.presentation.ordersHistory.model.OrdersItemView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class EstablishmentOrdersViewModel constructor(
    private val getCustomerOrders: GetOrdersListByEstablishmentAndState,
    private val establishmentOrdersViewMapper: EstablishmentOrderViewMapper,
    private val updateOrderStatus: UpdateOrderStatus
) : ViewModel(), CoroutineScope {
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO

    private val _establishmentCustomerOrders = MutableLiveData<Resource<List<EstablishmentOrderView>>>()
    val establishmentCustomerOrders: LiveData<Resource<List<EstablishmentOrderView>>> = _establishmentCustomerOrders

    private val _updateOrder = MutableLiveData<Resource<Boolean>>()
    val updateOrder: LiveData<Resource<Boolean>> = _updateOrder

    fun getOrdersListByEstablishmentAndState(
        customerId: Int,
        orderState: String
    ) {
        viewModelScope.launch {
            _establishmentCustomerOrders.value =
                Resource(Status.LOADING, mutableListOf(), "")

            val establishmentOrdersResult =
                getCustomerOrders.getOrdersListByEstablishmentAndState(customerId, orderState)

            if (establishmentOrdersResult.status == Status.SUCCESS) {
                val establishmentOrdersResultData = establishmentOrdersResult.data
                if (establishmentOrdersResultData.isNotEmpty()) {
                    val establishmentOrdersView = establishmentOrdersResultData.map { establishmentOrdersViewMapper.mapToView(it) }
                    _establishmentCustomerOrders.value = Resource(Status.SUCCESS, establishmentOrdersView, "")
                } else {
                    _establishmentCustomerOrders.value = Resource(Status.SUCCESS, mutableListOf(), "")
                }
            } else {
                _establishmentCustomerOrders.value = Resource(Status.ERROR, mutableListOf(), "")
            }
        }
    }

    fun updateStateOrder(deliveryCharge:Double,orderId: Int, state:String, list:List<OrdersItemView>){
        _updateOrder.value = Resource(Status.LOADING, true, "")
        viewModelScope.launch {
            val sendList = mutableListOf<OrderDetailBody>()

            list.forEach {
                if(it.isAttended){
                    if(state == "R"){
                        sendList.add(OrderDetailBody(it.orderDetailOrderDetailId.toInt(), "N"))
                    }else{
                        sendList.add(OrderDetailBody(it.orderDetailOrderDetailId.toInt(), "A"))
                    }
                }else{
                    sendList.add(OrderDetailBody(it.orderDetailOrderDetailId.toInt(), "N"))
                }
            }
            val result = updateOrderStatus.updateStaterOrder(deliveryCharge, orderId, state, sendList)

            if (result.status == Status.SUCCESS){
                _updateOrder.value = Resource(Status.SUCCESS, result.data, result.message)
            }else{
                _updateOrder.value = Resource(Status.ERROR, result.data, result.message)
            }
        }

    }

    fun cleanUpdateState(){
        _updateOrder.value = Resource(Status.LOADING, false, "")
    }
}