package com.android.bodegasadmin.presentation.ordersHistory.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class OrderDetailBodyView (
    val orderDetailId: Int,
    val state: String
) : Parcelable