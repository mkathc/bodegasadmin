package com.android.bodegasadmin.presentation.login

import androidx.lifecycle.*
import com.android.bodegasadmin.data.PreferencesManager
import com.android.bodegasadmin.domain.establishment.login.LoginEstablishment
import com.android.bodegasadmin.domain.establishment.login.LoginEstablishmentToken
import com.android.bodegasadmin.domain.establishment.update.RecoveryPassword
import com.android.bodegasadmin.domain.util.Resource
import com.android.bodegasadmin.domain.util.Status
import com.android.bodegasadmin.presentation.establishment.mapper.EstablishmentLoginTokenViewMapper
import com.android.bodegasadmin.presentation.establishment.mapper.EstablishmentViewMapper
import com.android.bodegasadmin.presentation.establishment.model.EstablishmentLoginTokenView
import com.android.bodegasadmin.presentation.establishment.model.EstablishmentView
import kotlinx.coroutines.launch

class LoginViewModel constructor(
    private val loginEstablishment: LoginEstablishment,
    private val recoveryPassword: RecoveryPassword,
    private val establishmentViewMapper: EstablishmentViewMapper,
    private val loginEstablishmentToken: LoginEstablishmentToken,
    private val establishmentLoginTokenViewMapper: EstablishmentLoginTokenViewMapper
) :
    ViewModel() {

    private val _loginCodeToken = MutableLiveData<Resource<EstablishmentLoginTokenView>>()
    val loginCodeToken: LiveData<Resource<EstablishmentLoginTokenView>> = _loginCodeToken

    private val _loginCode = MutableLiveData<Resource<EstablishmentView>>()
    val loginCode: LiveData<Resource<EstablishmentView>> = _loginCode

    private val _sendEmail = MutableLiveData<Resource<Boolean>>()
    val sendEmail : LiveData<Resource<Boolean>> = _sendEmail

    fun getDataEstablishment(establishmentId: Int) {
        _loginCode.value = Resource(
            Status.LOADING, EstablishmentView(
                0, "", "", "",0.0, 0.0, mutableListOf(), mutableListOf(),
                "", "", false, "", "", "", "","","","","","","","", "", "", false,
                mutableListOf()), ""
        )

        viewModelScope.launch {
            val loginResult = loginEstablishment.getDataEstablishment(establishmentId)
            val establishmentView = establishmentViewMapper.mapToView(loginResult.data)

            if (loginResult.status == Status.SUCCESS) {
                PreferencesManager.getInstance().setEstablishmentSelected(establishmentView)
                _loginCode.value = Resource(loginResult.status, establishmentView, "")
            } else {
                _loginCode.value = Resource(Status.ERROR, establishmentView, loginResult.message)
            }
        }
    }

    fun sendEmailToRecoveryAccount(body: ForgetPassDTO) {
        _sendEmail.value = Resource(Status.LOADING, false, "")
        viewModelScope.launch {
            val updateResult = recoveryPassword.recoveryPassword(body)
            if(updateResult.status == Status.SUCCESS){
                if(updateResult.data){
                    _sendEmail.value = Resource(Status.SUCCESS, updateResult.data, updateResult.message)
                }
                else{
                    _sendEmail.value = Resource(Status.ERROR, updateResult.data, "")
                }
            }
            else{
                _sendEmail.value = Resource(Status.ERROR, updateResult.data, "")
            }
        }
    }

    fun loginEstablishmentToken(email: String, password: String) {
        _loginCodeToken.value = Resource(Status.LOADING, EstablishmentLoginTokenView( "", ""),"")

        viewModelScope.launch {
            val loginResult = loginEstablishmentToken.loginEstablishmentToken(email, password)
            val establishmentView = establishmentLoginTokenViewMapper.mapToView(loginResult.data)

            if (loginResult.status == Status.SUCCESS) {
                PreferencesManager.getInstance().setEstablishmentLoginTokenSelected(establishmentView)
                _loginCodeToken.value = Resource(loginResult.status, establishmentView, "")
            } else {
                _loginCodeToken.value = Resource(Status.ERROR, establishmentView, loginResult.message)
            }
        }
    }
}