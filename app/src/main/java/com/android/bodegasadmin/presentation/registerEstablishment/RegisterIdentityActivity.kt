package com.android.bodegasadmin.presentation.registerEstablishment

import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import com.android.bodegasadmin.R
import com.android.bodegasadmin.data.PreferencesManager
import com.android.bodegasadmin.presentation.BaseActivity
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.activity_register_identification.*


class RegisterIdentityActivity : BaseActivity() {

    private var registerBody = PreferencesManager.getInstance().getBodyEstablishment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_identification)
        setOnClickListener()
    }

    override fun onResume() {
        super.onResume()
        loadDataFromLocalStorage()
    }

    private fun setOnClickListener() {

        btnSetDocumentType.setOnClickListener {
            showDialogDocumentTypeSelect()
        }

        btnNext.setOnClickListener {
            if (validationAlerts(inputDocumentType, boxDocumentType) &&
                validateExtensionDocumentNumber() && validateExtensionRUC()
            ) {
                nextViewSuccessfully()
            }
        }
    }

    private fun showDialogDocumentTypeSelect() {
        lateinit var dialog: AlertDialog
        val array = arrayOf("DNI", "Pasaporte", "Carné de extranjería")
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Elige un tipo de documento.")
        builder.setSingleChoiceItems(array, -1) { _, which ->
            val color = array[which]
            inputDocumentType.setText(color)
            dialog.dismiss()
        }
        dialog = builder.create()
        dialog.show()
    }

    private fun validateExtensionDocumentNumber(): Boolean {
        if (inputDocumentType.equals("DNI")) {
            return if (inputDocumentNumber.text.toString().length != 8) {
                boxDocumentNumber.error = null
                boxDocumentNumber.error = "Ingresa un número con 8 dígitos"
                false
            } else {
                true
            }
        } else if (inputDocumentType.equals("Pasaporte")) {
            return if (inputDocumentNumber.text.toString().length != 9) {
                boxDocumentNumber.error = null
                boxDocumentNumber.error = "Ingresa un número con 9 dígitos"
                false
            } else {
                true
            }
        } else { //"Carné de extranjería"
            if (inputDocumentNumber.text.toString().length != 8) {
                boxDocumentNumber.error = null
                boxDocumentNumber.error = "Ingresa un número con 8 dígitos"
                return false
            } else {
                return true
            }
        }
    }

    private fun validateExtensionRUC(): Boolean {
       return if (inputRuc.text.toString().length != 11) {
            boxRuc.error = null
            boxRuc.error = "Ingresa un número con 11 dígitos"
            false
        } else {
            true
        }
    }

    private fun validationAlerts(input: TextInputEditText, inputTitle: TextInputLayout): Boolean {
        val campo = input.text.toString()
        inputTitle.error = null
        if (campo == "Seleccionar") {
            inputTitle.error = "Selecciona un item de la lista."
            return false
        }
        return true
    }

    private fun saveDataInLocalStorage() {
        registerBody.ruc = inputRuc.text.toString()
        registerBody.dni = inputDocumentNumber.text.toString()
        PreferencesManager.getInstance().saveBodyEstablishment(registerBody)
    }

    private fun loadDataFromLocalStorage() {
        if (registerBody.dni != "") {
            inputDocumentType.setText("DNI")
            inputDocumentNumber.setText(registerBody.dni)
        }
    }

    private fun nextViewSuccessfully() {
        saveDataInLocalStorage()
        nextActivity(null, RegisterEstablishmentDataActivity::class.java, true)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        comeBack()
    }

    private fun comeBack() {
        nextActivity(null, RegisterBasicDataActivity::class.java, true)
    }

}
