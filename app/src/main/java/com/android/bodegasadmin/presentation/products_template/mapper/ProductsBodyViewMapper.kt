package com.android.bodegasadmin.presentation.products_template.mapper

import com.android.bodegasadmin.domain.products_template.ProductsBody
import com.android.bodegasadmin.presentation.products_template.model.ProductsBodyView

class ProductsBodyViewMapper : ProductsMapper<ProductsBody, ProductsBodyView> {

    override fun mapToUseCase(type: ProductsBodyView): ProductsBody {
        return ProductsBody(
            type.productTemplateId,
            type.price
        )
    }


}