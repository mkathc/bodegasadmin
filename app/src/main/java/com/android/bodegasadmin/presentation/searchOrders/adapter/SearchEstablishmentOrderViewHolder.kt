package com.android.bodegasadmin.presentation.searchOrders.adapter

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.row_customer_order_search.view.*

class SearchEstablishmentOrderViewHolder (itemView: View, viewHolderListener: ViewHolderListener):
    RecyclerView.ViewHolder(itemView) {

    val tvUserName : TextView = itemView.tvStoreName
    val tvTotal : TextView = itemView.tvTotal
    val tvTitleDate : TextView = itemView.tvTitleDate
    val tvDate : TextView = itemView.tvDate
    val tvDeliveryType : TextView = itemView.tvDeliveryType
    val tvStatus : TextView = itemView.tvStatus

    init {
        itemView.setOnClickListener {
            viewHolderListener.onClick(
                layoutPosition
            )
        }
    }

    interface ViewHolderListener {
        fun onClick(
            position: Int
        )
    }
}