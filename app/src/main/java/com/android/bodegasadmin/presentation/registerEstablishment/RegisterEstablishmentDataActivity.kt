package com.android.bodegasadmin.presentation.registerEstablishment

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.android.bodegasadmin.R
import com.android.bodegasadmin.data.PreferencesManager
import com.android.bodegasadmin.domain.util.Status
import com.android.bodegasadmin.presentation.BaseActivity
import com.android.bodegasadmin.presentation.allPaymentMethods.GetAllPaymentMethodsViewModel
import com.android.bodegasadmin.presentation.allPaymentMethods.model.AllPaymentMethodsView
import com.android.bodegasadmin.presentation.allSchedules.GetAllSchedulesViewModel
import com.android.bodegasadmin.presentation.allSchedules.model.AllScheduleView
import com.android.bodegasadmin.presentation.utils.animation.LoadingAnimationImageView
import com.android.bodegasadmin.presentation.utils.animation.LoadingDots
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.activity_register_establishment_data.*
import kotlinx.android.synthetic.main.activity_register_establishment_data.btnNext
import kotlinx.android.synthetic.main.activity_register_references.layoutLoading
import kotlinx.android.synthetic.main.layout_loading.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class RegisterEstablishmentDataActivity : BaseActivity() {

    private var registerBody = PreferencesManager.getInstance().getBodyEstablishment()

    private val getAllSchedulesViewModel: GetAllSchedulesViewModel by viewModel()
    private val getAllPaymentMethodsViewModel: GetAllPaymentMethodsViewModel by viewModel()
    private var operationScheduleViewList = mutableListOf<AllScheduleView>()
    private var stringOperationList = mutableListOf<String>()
    private var operationIdSelected: Int = 0
    private var deliveryScheduleViewList = mutableListOf<AllScheduleView>()
    private var paymentMethodsViewList = mutableListOf<AllPaymentMethodsView>()
    private var stringDeliveryList = mutableListOf<String>()
    private var stringPaymentMethodsList = mutableListOf<String>()
    private var selectedDelivery = booleanArrayOf()
    private var selectedPaymentMethod = booleanArrayOf()
    private var deliveryIdSelected = mutableListOf<Int>()
    private var paymentMethodsIdSelected = mutableListOf<Int>()
    private var loadingAnimationImageView: LoadingAnimationImageView? = null
    private var loadingDots: LoadingDots? = null

    //Values to save
    private var establishmentType = 1
    private var hasDelivery = "S"
    private var hasChargeForDelivery = "S"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_establishment_data)
        setOnClickListener()
    }

    override fun onStart() {
        super.onStart()
        getScheduleForOperation()
        getSchedulesForDelivery()
        getMethodsForPayment()
    }


    private fun setOnClickListener() {
        tvBodegaSelect.setOnClickListener {
            establishmentType = 1
            tvMarketSelect.setTextColor(
                ContextCompat.getColor(
                    applicationContext,
                    R.color.color_text_grey_100
                )
            )
            tvBodegaSelect.setTextColor(
                ContextCompat.getColor(
                    applicationContext,
                    R.color.color_brand_primary_default
                )
            )
        }
        tvMarketSelect.setOnClickListener {
            establishmentType = 2
            tvMarketSelect.setTextColor(
                ContextCompat.getColor(
                    applicationContext,
                    R.color.color_brand_primary_default
                )
            )
            tvBodegaSelect.setTextColor(
                ContextCompat.getColor(
                    applicationContext,
                    R.color.color_text_grey_100
                )
            )
        }

        tvYes.setOnClickListener {
            hasDelivery = "S"
            tvNo.setTextColor(
                ContextCompat.getColor(
                    applicationContext,
                    R.color.color_text_grey_100
                )
            )
            tvYes.setTextColor(
                ContextCompat.getColor(
                    applicationContext,
                    R.color.color_brand_primary_default
                )
            )

            layoutDelivery.visibility = View.VISIBLE
            tvQuestionChargeForDelivery.visibility = View.VISIBLE
            linearLayoutChargeForDeliverySelect.visibility = View.VISIBLE
        }

        tvNo.setOnClickListener {
            hasDelivery = "N"
            tvYes.setTextColor(
                ContextCompat.getColor(
                    applicationContext,
                    R.color.color_text_grey_100
                )
            )
            tvNo.setTextColor(
                ContextCompat.getColor(
                    applicationContext,
                    R.color.color_brand_primary_default
                )
            )

            layoutDelivery.visibility = View.GONE
            tvQuestionChargeForDelivery.visibility = View.GONE
            linearLayoutChargeForDeliverySelect.visibility = View.GONE

        }

        tvYesChargeForDelivery.setOnClickListener {
            hasChargeForDelivery = "S"
            tvNoChargeForDelivery.setTextColor(
                ContextCompat.getColor(
                    applicationContext,
                    R.color.color_text_grey_100
                )
            )
            tvYesChargeForDelivery.setTextColor(
                ContextCompat.getColor(
                    applicationContext,
                    R.color.color_brand_primary_default
                )
            )
        }

        tvNoChargeForDelivery.setOnClickListener {
            hasChargeForDelivery = "N"
            tvYesChargeForDelivery.setTextColor(
                ContextCompat.getColor(
                    applicationContext,
                    R.color.color_text_grey_100
                )
            )
            tvNoChargeForDelivery.setTextColor(
                ContextCompat.getColor(
                    applicationContext,
                    R.color.color_brand_primary_default
                )
            )
        }

        btnAddOperationSchedule.setOnClickListener {
            showOperationSchedules()
        }

        btnAddDeliverySchedule.setOnClickListener {
            showDialogDeliveryList()
        }

        btnAddPaymentMethod.setOnClickListener {
            showDialogPaymentMethods()
        }

        btnNext.setOnClickListener {

            if(hasDelivery == "S"){
                if(extensionValidation(inputname, boxname, 5) &&
                    extensionValidation(inputcelular, boxcelular, 9) &&
                    validarAlerts(inputOperationSelected, boxOpertationSchedule)&&
                    validarAlerts(inputDeliverySelected, boxDeliverySchedule)&&
                    validarAlerts(inputPaymentMethodSelected, boxPaymentMethod)) {
                    nextViewSuccessfully()
                }
            }else{
                if(extensionValidation(inputname, boxname, 5) &&
                    extensionValidation(inputcelular, boxcelular, 9) &&
                    validarAlerts(inputOperationSelected, boxOpertationSchedule)){
                    nextViewSuccessfully()
                }
            }

        }
    }


    private fun getScheduleForOperation() {
        getAllSchedulesViewModel.operationSchedule.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    operationScheduleViewList = it.data.toMutableList()
                    btnAddOperationSchedule.isEnabled = true
                    getOperationStringArray()
                    btnAddOperationSchedule.isEnabled = true
                    hideLoading()
                }
                Status.ERROR -> {
                    hideLoading()
                    Toast.makeText(
                        applicationContext,
                        "Ocurrió un error al cargar los departamentos",
                        Toast.LENGTH_LONG
                    ).show()
                }

                Status.LOADING -> {
                    showLoading()
                }
            }
        })
        getAllSchedulesViewModel.getOperationSchedule("N")
    }

    private fun getOperationStringArray() {
        operationScheduleViewList.forEach {
            stringOperationList.add(it.range)
        }
    }

    private fun showOperationSchedules() {
        lateinit var dialog: AlertDialog
        val array: Array<CharSequence> =
            stringOperationList.map { i -> i as CharSequence }.toTypedArray()
        val builder = AlertDialog.Builder(this)
        var selectedValue: CharSequence = ""
        builder.setTitle("Selecciona un horario de atención:")
        builder.setSingleChoiceItems(array, -1)
        { _, which ->
            selectedValue = array[which]
            inputOperationSelected.setText(selectedValue)
            dialog.dismiss()
            for (i in operationScheduleViewList) {
                if (i.range == selectedValue) {
                    operationIdSelected = i.scheduleId
                }
            }
        }
        dialog = builder.create()
        dialog.show()

    }

    private fun getSchedulesForDelivery() {
        getAllSchedulesViewModel.deliverySchedule.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    deliveryScheduleViewList = it.data.toMutableList()
                    btnAddDeliverySchedule.isEnabled = true
                    getDeliveryStringsArray()
                    btnAddDeliverySchedule.isEnabled = true
                    hideLoading()
                }
                Status.ERROR -> {
                    Toast.makeText(
                        applicationContext,
                        "Ocurrió un error al cargar los horarios",
                        Toast.LENGTH_LONG
                    ).show()
                    hideLoading()
                }
                Status.LOADING -> {
                    showLoading()
                }
            }
        })
        getAllSchedulesViewModel.getDeliverySchedule("S")
    }

    private fun getDeliveryStringsArray() {
        stringDeliveryList.clear()
        deliveryScheduleViewList.forEach {
            stringDeliveryList.add(it.range)
        }

        selectedDelivery = stringDeliveryList.map { i -> false }.toBooleanArray()
    }



    private fun showDialogDeliveryList() {
        lateinit var alertDialog: AlertDialog
        val array: Array<CharSequence> =
            stringDeliveryList.map { i -> i as CharSequence }.toTypedArray()
        val builder = AlertDialog.Builder(this)

        builder.setTitle("Selecciona tus horarios de delivery:")

        builder.setMultiChoiceItems(array, selectedDelivery) { alertDialog, which, isChecked ->
            // Update the clicked item checked status
            selectedDelivery[which] = isChecked

            // Get the clicked item
            val color = array[which]

            // Display the clicked item text
            Toast.makeText(applicationContext, "$color clicked.", Toast.LENGTH_SHORT).show()
        }
        var texto:String = ""
        // Set the positive/yes button click listener
        builder.setPositiveButton("ACEPTAR") { _, _ ->
            // Do something when click positive button
            var isEmpty = true
            inputDeliverySelected.setText("")
                for (i in array.indices) {
                    val checked = selectedDelivery[i]
                    if (checked) {
                        isEmpty = false
                        texto = texto + " " + array[i]
                        //inputDeliverySelected.setText("${inputDeliverySelected.text} \n ${array[i]}")
                        getIdOfDelivery(array[i])
                    }
                }
            inputDeliverySelected.setText(texto)

            if (isEmpty){
                inputDeliverySelected.setText("")
            }

        }

        alertDialog = builder.create()
        alertDialog.show()
    }

    private fun getIdOfDelivery(value:CharSequence){
        deliveryScheduleViewList.forEach {
            if(value == it.range){
                deliveryIdSelected.add(it.scheduleId)
            }
        }
    }

    private fun nextViewSuccessfully() {
        saveDataInLocalStorage()
        nextActivity(null, RegisterDirectionActivity::class.java, true)
    }

    private fun saveDataInLocalStorage(){
        paymentMethodsIdSelected
        registerBody.businessName = inputname.text.toString()
        registerBody.phoneNumber = inputcelular.text.toString()
        val operationList = mutableListOf<Int>()
        operationList.add(operationIdSelected)
        registerBody.operationSchedule = operationList
        registerBody.delivery = hasDelivery
        registerBody.establishmentType = establishmentType
        registerBody.paymentMethod = paymentMethodsIdSelected
        if(hasDelivery == "S") {
            registerBody.shippingSchedule = deliveryIdSelected
            if(hasChargeForDelivery.equals("S"))
                registerBody.deliveryCharge = true
            else
                registerBody.deliveryCharge = false
        }
        PreferencesManager.getInstance().saveBodyEstablishment(registerBody)
    }

    private fun loadDataFromLocalStorage() {
        if(registerBody.businessName != "") inputname.setText(registerBody.businessName)
        if(registerBody.phoneNumber != "") inputcelular.setText(registerBody.phoneNumber)
        if(registerBody.delivery == "S") {
            tvNo.setTextColor(
                ContextCompat.getColor(
                    applicationContext,
                    R.color.color_text_grey_100
                )
            )
            tvYes.setTextColor(
                ContextCompat.getColor(
                    applicationContext,
                    R.color.color_brand_primary_default
                )
            )
            tvNoChargeForDelivery.setTextColor(
                ContextCompat.getColor(
                    applicationContext,
                    R.color.color_text_grey_100
                )
            )
            tvYesChargeForDelivery.setTextColor(
                ContextCompat.getColor(
                    applicationContext,
                    R.color.color_brand_primary_default
                )
            )
        }else{
            tvNo.setTextColor(
                ContextCompat.getColor(
                    applicationContext,
                    R.color.color_brand_primary_default
                )
            )
            tvYes.setTextColor(
                ContextCompat.getColor(
                    applicationContext,
                    R.color.color_text_grey_100
                )
            )
        }
        if(registerBody.establishmentType == 1){
            tvMarketSelect.setTextColor(
                ContextCompat.getColor(
                    applicationContext,
                    R.color.color_text_grey_100
                )
            )
            tvBodegaSelect.setTextColor(
                ContextCompat.getColor(
                    applicationContext,
                    R.color.color_brand_primary_default
                )
            )
        }else{
            tvMarketSelect.setTextColor(
                ContextCompat.getColor(
                    applicationContext,
                    R.color.color_brand_primary_default
                )
            )
            tvBodegaSelect.setTextColor(
                ContextCompat.getColor(
                    applicationContext,
                    R.color.color_text_grey_100
                )
            )
        }
    }

    private fun validarAlerts(
        campo_input: TextInputEditText,
        title_input: TextInputLayout
    ): Boolean {
        val campo = campo_input.text.toString()
        title_input.error = null
        if (campo == "") {
            title_input.error = "Selecciona un item de la lista."
            return false
        }
        return true
    }

    private fun extensionValidation(
        campo_input: TextInputEditText,
        inputTitle: TextInputLayout,
        minimunLenght: Int
    ): Boolean {
        val campo = campo_input.text.toString()
        inputTitle.error = null
        if ((campo.isEmpty()) || campo.length < minimunLenght) {
            inputTitle.error = "Introduce una valor más extensa."
            return false
        }
        return true
    }

    override fun onBackPressed() {
        super.onBackPressed()
        comeBack()
    }

    private fun comeBack() {
        val bundle = Bundle()
        nextActivity(bundle, RegisterIdentityActivity::class.java, true)
    }


    override fun onResume() {
        super.onResume()
        loadDataFromLocalStorage()
    }

    private fun showLoading() {
        layoutLoading.visibility = View.VISIBLE
        animateLoadingText()
        animateLoadingImage()
    }

    private fun hideLoading() {
        layoutLoading.visibility = View.GONE

    }

    private fun animateLoadingText() {
        if (loadingDots == null) {
            loadingDots =
                LoadingDots(dots)
            loadingDots?.animateLoadingText()
        }
    }

    private fun animateLoadingImage() {
        if (loadingAnimationImageView == null) {
            loadingAnimationImageView =
                LoadingAnimationImageView(
                    ivShrimp
                )
            loadingAnimationImageView?.startAnimation()
        }
    }

    private fun getMethodsForPayment() {
        getAllPaymentMethodsViewModel.paymentMethod.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    paymentMethodsViewList = it.data.toMutableList()
                    btnAddPaymentMethod.isEnabled = true
                    getPaymentMethodsStringsArray()
                    btnAddPaymentMethod.isEnabled = true
                    hideLoading()
                }
                Status.ERROR -> {
                    Toast.makeText(
                        applicationContext,
                        "Ocurrió un error al cargar los métodos de pago",
                        Toast.LENGTH_LONG
                    ).show()
                    hideLoading()
                }
                Status.LOADING -> {
                    showLoading()
                }
            }
        })
        getAllPaymentMethodsViewModel.getPaymentMethods()
    }

    private fun getPaymentMethodsStringsArray() {
        stringPaymentMethodsList.clear()
        paymentMethodsViewList.forEach {
            stringPaymentMethodsList.add(it.description)
        }

        selectedPaymentMethod = stringPaymentMethodsList.map { i -> false }.toBooleanArray()
    }

    private fun showDialogPaymentMethods() {
        lateinit var alertDialog: AlertDialog
        val array: Array<CharSequence> =
            stringPaymentMethodsList.map { i -> i as CharSequence }.toTypedArray()
        val builder = AlertDialog.Builder(this)

        builder.setTitle("Selecciona tus métodos de pago:")

        builder.setMultiChoiceItems(array, selectedPaymentMethod) { alertDialog, which, isChecked ->
            // Update the clicked item checked status
            selectedPaymentMethod[which] = isChecked

            // Get the clicked item
            val color = array[which]

            // Display the clicked item text
            Toast.makeText(applicationContext, "$color clicked.", Toast.LENGTH_SHORT).show()
        }
    var texto:String = ""
        // Set the positive/yes button click listener
        builder.setPositiveButton("ACEPTAR") { _, _ ->
            // Do something when click positive button
            var isEmpty = true
            inputPaymentMethodSelected.setText("")
            for (i in array.indices) {
                val checked = selectedPaymentMethod[i]
                if (checked) {
                    isEmpty = false
                    texto = texto + " " + array[i]
                 //   inputPaymentMethodSelected.setText("${inputPaymentMethodSelected.text} \n ${array[i]}")
                    getIdOfPaymentMethods(array[i])
                }
            }
            inputPaymentMethodSelected.setText(texto)

            if (isEmpty){
                inputPaymentMethodSelected.setText("")
            }

        }

        alertDialog = builder.create()
        alertDialog.show()
    }

    private fun getIdOfPaymentMethods(value:CharSequence){
        paymentMethodsViewList.forEach {
            if(value == it.description){
                paymentMethodsIdSelected.add(it.paymentMethodId)
            }
        }
    }

}
