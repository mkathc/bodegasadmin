package com.android.bodegasadmin.presentation.references.mapper

import com.android.bodegasadmin.domain.references.province.Province
import com.android.bodegasadmin.presentation.Mapper
import com.android.bodegasadmin.presentation.references.ProvinceView

class ProvinceViewMapper: Mapper<ProvinceView, Province> {
    override fun mapToView(type: Province): ProvinceView {
        return ProvinceView(
            type.id,
            type.name,
            type.department_id
        )
    }
}