package com.android.bodegasadmin.presentation.products_template.mapper

interface ProductsMapper<out V, in D> {

    fun mapToUseCase(type: D): V

}