package com.android.bodegasadmin.presentation.references.mapper

import com.android.bodegasadmin.domain.references.district.District
import com.android.bodegasadmin.presentation.Mapper
import com.android.bodegasadmin.presentation.references.DistrictView

class DistrictViewMapper: Mapper<DistrictView, District> {
    override fun mapToView(type: District): DistrictView {
        return DistrictView(
            type.id,
            type.name,
            type.province_id,
            type.department_id
        )
    }
}