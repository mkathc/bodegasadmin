package com.android.bodegasadmin.presentation.ordersHistory

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.fragment.app.FragmentStatePagerAdapter

class OrdersHistoryCollectionPagerAdapter constructor (fm: FragmentManager) : FragmentStatePagerAdapter(fm){

    override fun getCount(): Int  = 4

    override fun getItem(i: Int): Fragment {

        when (i){
            0-> {
               return InformedOrdersFragment.newInstance()
            }

            1 ->{
                return  ApprovedOrdersFragment.newInstance()
            }

            2 ->{
                return DeliveredOrdersFragment.newInstance()
            }

            else ->{
                return CanceledOrdersFragment.newInstance()

            }
        }

    }

    override fun getPageTitle(position: Int): CharSequence {
        var nombre = position+1;
        var tituloEntregado=""
        when (nombre){
            1-> { tituloEntregado = "Informados"}
            2-> { tituloEntregado = "Atendidos"}
            3-> { tituloEntregado = "Entregados"}
            else-> { tituloEntregado = "Anulados"}
        }
        return tituloEntregado
    }

}

private const val ARG_OBJECT = "object"
