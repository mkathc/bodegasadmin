package com.android.bodegasadmin.presentation.ordersHistory.mapper

import com.android.bodegasadmin.domain.establishmentOrders.EstablishmentOrders
import com.android.bodegasadmin.domain.establishmentOrders.OrderDetails
import com.android.bodegasadmin.presentation.Mapper
import com.android.bodegasadmin.presentation.ordersHistory.model.EstablishmentOrderView
import com.android.bodegasadmin.presentation.ordersHistory.model.OrdersItemView
import com.android.bodegasadmin.presentation.utils.DateTimeHelper

class EstablishmentOrderViewMapper(
    private val orderDetailsViewMapper: OrderDetailsViewMapper
) : Mapper<EstablishmentOrderView, EstablishmentOrders> {
    override fun mapToView(type: EstablishmentOrders): EstablishmentOrderView {
        return EstablishmentOrderView(
            type.creationDate,
            type.orderId,
            getDeliveryType(type.deliveryType),
            getIsDelivery(type.deliveryType),
            DateTimeHelper.parseDateOrder(type.shippingDateFrom),
            type.status,
            getStatusValue(type.status),
            type.establishmentEmail,
            type.establishmentName,
            type.total,
            type.customerPhoneNumber,
            type.customerName + " " + type.customerLastNamePaternal + " " + type.customerLastNameMaternal,
            type.customerAddress,
            type.customerLatitude,
            type.customerLongitude,
            getListOrdersDetail(type.orderDetails),
            type.updateDate,
            type.paymentMethodCustomerMessage,
            type.paymentMethodDescription,
            type.paymentMethodEstablishmentMessage,
            type.paymentMethodPaymentMethodId,
            type.paymentMethodRequestAmount,
            type.customerAmount,
            type.deliveryCharge,
            type.customerUrbanization
        )
    }

    private fun getIsDelivery(deliverType: String): Boolean {
        return deliverType == "E"
    }

    private fun getDeliveryType(deliverType: String): String {
        return if(deliverType=="E"){
            "Delivery"
        }else{
            "Recojo en tda."
        }
    }

    private fun getStatusValue(status: String): String {
        return when (status) {
            "I" -> "Informado"
            "A" -> "Atendido"
            "E" -> "Enviado"
            "T" -> "Entregado"
            "R" -> "Anulado por el proveedor"
            "N" -> "Anulado por el cliente"
            else -> ""
        }
    }

    private fun getListOrdersDetail(list: List<OrderDetails>): List<OrdersItemView> {
        val listSchedule = mutableListOf<OrdersItemView>()
        list.forEach {
            listSchedule.add(orderDetailsViewMapper.mapToView(it))
        }
        return listSchedule
    }
}