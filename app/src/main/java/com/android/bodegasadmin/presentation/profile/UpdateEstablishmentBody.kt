package com.android.bodegasadmin.presentation.profile

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

data class UpdateEstablishmentBody(
    @SerializedName("address")
    var address: String,

    @SerializedName("businessName")
    val businessName: String,
   // val country: String,

    @SerializedName("delivery")
    val delivery: String,

    @SerializedName("departament")
    val department: String,

    @SerializedName("district")
    val district: String,

    @SerializedName("dni")
    val dni: String,

    @SerializedName("establishmentType")
    val establishmentType: Int,

    @SerializedName("lastNameMaternal")
    val lastNameMaternal: String,

    @SerializedName("lastNamePaternal")
    val lastNamePaternal: String,

    @SerializedName("latitude")
    var latitude: Double,

    @SerializedName("longitude")
    var longitude: Double,

    @SerializedName("name")
    val name: String,

    @SerializedName("phoneNumber")
    val phoneNumber: String,

    @SerializedName("province")
    val province: String,

    @SerializedName("ruc")
    val ruc: String,

    @SerializedName("urbanization")
    val urbanization: String,

    @SerializedName("updateUser")
    val updateUser: String,

    @SerializedName("deliveryCharge")
    val deliveryCharge: Boolean,

    @SerializedName("paymentMethod")
    val paymentMethod: List<Int>


)