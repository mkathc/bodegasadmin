package com.android.bodegasadmin.presentation.allPaymentMethods.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AllPaymentMethodsView(
    val paymentMethodId: Int,
    val requestedAmount: Boolean,
    val description:String,
    val customerMessage: String
) : Parcelable