package com.android.bodegasadmin.presentation.registerEstablishment

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import com.android.bodegasadmin.R
import com.android.bodegasadmin.data.PreferencesManager
import com.android.bodegasadmin.domain.util.Status
import com.android.bodegasadmin.presentation.BaseActivity
import com.android.bodegasadmin.presentation.login.LoginViewModel
import com.android.bodegasadmin.presentation.main.MainActivity
import com.android.bodegasadmin.presentation.references.DepartmentView
import com.android.bodegasadmin.presentation.references.DistrictView
import com.android.bodegasadmin.presentation.references.ProvinceView
import com.android.bodegasadmin.presentation.utils.animation.LoadingAnimationImageView
import com.android.bodegasadmin.presentation.utils.animation.LoadingDots
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_register_references.*
import kotlinx.android.synthetic.main.layout_loading.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class RegisterReferencesActivity : BaseActivity() {

    private val loginViewModel: LoginViewModel by viewModel()

    private var pais: String = "PE"

    private var registerBody = PreferencesManager.getInstance().getBodyEstablishment()

    private val registerViewModel: RegisterEstablishmentViewModel by viewModel()

    private var departmentViewList: MutableList<DepartmentView> = mutableListOf()
    private var stringDeparmentList = ArrayList<CharSequence>()

    private var provinceViewList: MutableList<ProvinceView> = mutableListOf()
    private var stringProvinceList = ArrayList<CharSequence>()

    private var districtViewList: MutableList<DistrictView> = mutableListOf()
    private var stringDistrictList = ArrayList<CharSequence>()

    var selectedDepartmentId: String = ""
    var selectedProvinceId: String = ""

    private var loadingAnimationImageView: LoadingAnimationImageView? = null
    private var loadingDots: LoadingDots? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_references)
        setOnClickListener()
        getDepartments()
        observerRegister()
        //observeViewModel()
        observeViewModelGetData()
    }

    private fun setOnClickListener(){
        boton_set_departamento.setOnClickListener {
            showDialogDepartments()
        }
        boton_set_provincia.setOnClickListener {
            showDialogProvincia()
        }
        boton_set_distrito.setOnClickListener {
            showDialogDistrito()
        }

        btnRegister.setOnClickListener {
            validateDataToRegister()
        }
    }

    private fun observerRegister() {
        registerViewModel.registerEstablishment.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    //hideLoading()
                    //registerSuccessfully()
                    loginViewModel.getDataEstablishment(
                        PreferencesManager.getInstance().getEstablishmentLoginTokenSelected().idEntity.toInt()
                    )
                }
                Status.ERROR -> {
                    Toast.makeText(
                        applicationContext,
                        it.message,
                        Toast.LENGTH_LONG
                    ).show()
                    hideLoading()
                }
                Status.LOADING -> {
                    showLoading()
                }
            }
        })
    }

    private fun getDepartments() {
        registerViewModel.departments.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    departmentViewList = it.data.toMutableList()
                    getDepartmentStringsArray()
                    hideLoading()
                }
                Status.ERROR -> {
                    hideLoading()
                    Toast.makeText(
                        applicationContext,
                        "Ocurrió un error al cargar los departamentos",
                        Toast.LENGTH_LONG
                    ).show()
                }

                Status.LOADING -> {
                    showLoading()
                }
            }
        })
        registerViewModel.getDepartments()
    }

    private fun getDepartmentStringsArray() {
        stringDeparmentList.clear()
        departmentViewList.forEach {
            stringDeparmentList.add(it.name)
        }
    }

    private fun showDialogDepartments() {
        lateinit var dialog: AlertDialog
        val array: Array<CharSequence> =
            stringDeparmentList.map { i -> i as CharSequence }.toTypedArray()
        val builder = AlertDialog.Builder(this)
        var selectedValue: CharSequence = ""
        builder.setTitle("Elige un departamento.")
        builder.setSingleChoiceItems(array, -1)
        { _, which ->
            selectedValue = array[which]
            departamentoinput.setText(selectedValue)
            dialog.dismiss()
            boton_set_provincia.isEnabled = true
            provinciainput.setText("Seleccionar")
            distritoinput.setText("Seleccionar")

            for (i in departmentViewList) {
                if (i.name == selectedValue) {
                    selectedDepartmentId = i.id
                }
            }

            getProvinces()
        }
        dialog = builder.create()
        dialog.show()

    }


    private fun getProvinces() {
        registerViewModel.provinces.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    provinceViewList = it.data.toMutableList()
                    getProvinceStringsArray()
                    hideLoading()
                }
                Status.ERROR -> {
                    Toast.makeText(
                        applicationContext,
                        "Ocurrió un error al cargar las provincias",
                        Toast.LENGTH_LONG
                    ).show()
                    hideLoading()
                }
                Status.LOADING -> {
                    showLoading()
                }
            }
        })
        registerViewModel.getProvinces()
    }

    private fun getProvinceStringsArray() {
        stringProvinceList.clear()
        provinceViewList.forEach {
            if (it.department_id == selectedDepartmentId) {
                stringProvinceList.add(it.name)
            }
        }
    }

    private fun showDialogProvincia() {
        lateinit var dialog: AlertDialog
        val array: Array<CharSequence> =
            stringProvinceList.map { i -> i as CharSequence }.toTypedArray()
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Elige una provincia.")
        var selectedValue: CharSequence = ""
        builder.setSingleChoiceItems(array, -1) { _, which ->
            selectedValue = array[which]
            provinciainput.setText(selectedValue)
            dialog.dismiss()

            boton_set_distrito.isEnabled = true
            distritoinput.setText("Seleccionar")

            for (i in provinceViewList) {
                if (i.name == selectedValue) {
                    selectedProvinceId = i.id
                }
            }
            obtenerDistritos()
        }
        dialog = builder.create()
        dialog.show()
    }

    private fun obtenerDistritos() {
        registerViewModel.districts.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    districtViewList = it.data.toMutableList()
                    getDistrictsStringsArray()
                    hideLoading()

                }
                Status.ERROR -> {
                    Toast.makeText(
                        applicationContext,
                        "Ocurrió un error al cargar los distritos",
                        Toast.LENGTH_LONG
                    ).show()
                    hideLoading()
                }
                Status.LOADING -> {
                    showLoading()
                }
            }
        })
        registerViewModel.getDistricts()
    }

    private fun getDistrictsStringsArray() {
        stringDistrictList.clear()
        districtViewList.forEach {
            if (it.department_id == selectedDepartmentId && it.province_id == selectedProvinceId) {
                stringDistrictList.add(it.name)
            }
        }
    }


    private fun showDialogDistrito() {
        lateinit var dialog: AlertDialog
        val array: Array<CharSequence> =
            stringDistrictList.map { i -> i as CharSequence }.toTypedArray()
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Elige un distrito.")
        var selectedValue: CharSequence = ""
        builder.setSingleChoiceItems(array, -1) { _, which ->
            selectedValue = array[which]
            distritoinput.setText(selectedValue)
            dialog.dismiss()
        }
        dialog = builder.create()
        dialog.show()
    }

    private fun validateDataToRegister() {
        if (validarAlerts(departamentoinput, departamentobox) &&
            validarAlerts(provinciainput, provinciabox) &&
            validarAlerts(distritoinput, distritobox) &&
            extensionValidation(inputreferencia, boxreferencia, 5)
        ) {
            saveData()
            registerEstablishment()
            // registerSuccessfully()
        }
    }

    private fun registerSuccessfully() {
        val bundle = Bundle()
         nextActivity(bundle, MainActivity::class.java, true)
    }

    private fun validarAlerts(
        campo_input: TextInputEditText,
        title_input: TextInputLayout
    ): Boolean {
        val campo = campo_input.text.toString()
        title_input.error = null
        if (campo == "Seleccionar") {
            title_input.error = "Selecciona un item de la lista."
            return false
        }
        return true
    }

    private fun extensionValidation(
        campo_input: TextInputEditText,
        inputTitle: TextInputLayout,
        minimunLenght: Int
    ): Boolean {
        val campo = campo_input.text.toString()
        inputTitle.error = null
        if ((campo.isEmpty()) || campo.length < minimunLenght) {
            inputTitle.error = "Introduce una referencia más extensa."
            return false
        }
        return true
    }

    override fun onBackPressed() {
        super.onBackPressed()
        comeBack()
    }

    private fun comeBack() {
        val bundle = Bundle()
        nextActivity(bundle, RegisterDirectionActivity::class.java, true)
    }

    private fun saveData() {
        registerBody.department = departamentoinput.text.toString()
        registerBody.province = provinciainput.text.toString()
        registerBody.district = distritoinput.text.toString()
        registerBody.urbanization = inputreferencia.text.toString()
        registerBody.cardNumber = "1234567890123456"
        registerBody.cardOperator = "Visa"
        registerBody.country = "PE"
        registerBody.ruc = "12345678765"
        PreferencesManager.getInstance().saveBodyEstablishment(registerBody)
    }

    fun registerEstablishment() {
        registerViewModel.sendRegisterEstablishment(registerBody)
    }

    override fun onResume() {
        super.onResume()
        loadDataFromLocalStorage()
    }

    private fun loadDataFromLocalStorage() {
        if(registerBody.province != "") provinciainput.setText(registerBody.province)
        if(registerBody.department != "") provinciainput.setText(registerBody.department)
        if(registerBody.district != "") provinciainput.setText(registerBody.district)
        if(registerBody.urbanization != "") provinciainput.setText(registerBody.urbanization)
    }

    private fun showLoading() {
        layoutLoading.visibility = View.VISIBLE
        animateLoadingText()
        animateLoadingImage()
    }

    private fun hideLoading() {
        layoutLoading.visibility = View.GONE

    }

    private fun animateLoadingText() {
        if (loadingDots == null) {
            loadingDots =
                LoadingDots(dots)
            loadingDots?.animateLoadingText()
        }
    }

    private fun animateLoadingImage() {
        if (loadingAnimationImageView == null) {
            loadingAnimationImageView =
                LoadingAnimationImageView(
                    ivShrimp
                )
            loadingAnimationImageView?.startAnimation()
        }
    }

    /*private fun observeViewModel() {
        loginViewModel.loginCodeToken.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    PreferencesManager.getInstance().setIsLogin(true)
                    showErrorMessage(
                        it.message.let { "Login correcto"})
                    //loginSuccessfully()
                    loginViewModel.loginEstablishment(
                        PreferencesManager.getInstance().getEstablishmentLoginTokenSelected().idEntity.toInt()
                    )
                }
                Status.ERROR -> {
                    layoutProgressBar.visibility = View.GONE
                    if (it.message == "500") {
                        showErrorMessage(
                            it.message.let { getString(R.string.login_not_connection_message)})
                    } else {
                        it.message?.let { it1 ->
                            showErrorMessage(
                                it1
                            )
                        }
                    }
                }
                Status.LOADING -> {
                    layoutProgressBar.visibility = View.VISIBLE
                }
            }
        })
    }*/

    private fun observeViewModelGetData() {
        loginViewModel.loginCode.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    hideLoading()
                    PreferencesManager.getInstance().setIsLogin(true)
                    registerSuccessfully()
                }
                Status.ERROR -> {
                    hideLoading()
                    layoutProgressBar.visibility = View.GONE
                    if (it.message == "500") {
                    } else {
                        it.message?.let { it1 ->
                        }
                    }
                }
                Status.LOADING -> {
                    showLoading()
                }
            }
        })
    }

   /* private fun showErrorMessage(message: String) {
        Snackbar.make(
            mainView,
            message,
            Snackbar.LENGTH_LONG
        ).show()
    }*/

}
