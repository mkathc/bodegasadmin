package com.android.bodegasadmin.presentation.myproducts.adapter


import android.content.Context
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.widget.Filter
import android.widget.Filterable
import com.android.bodegasadmin.R
import com.android.bodegasadmin.presentation.myproducts.model.MyProductsView
import java.util.*
import kotlin.collections.ArrayList


class MyProductsAdapter(
    private val viewHolderListener: MyProductsViewHolder.ViewHolderListener
) : RecyclerView.Adapter<MyProductsViewHolder>() {

    private val productsList = mutableListOf<MyProductsView>()
    private lateinit var context: Context

  /*  var productFilterList = ArrayList<MyProductsView>()

    init {
        productFilterList = productsList.toCollection(ArrayList())
    }*/

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyProductsViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_my_products, parent, false)
        context = parent.context
        return MyProductsViewHolder(view, viewHolderListener)
    }

    override fun getItemCount(): Int {
        return productsList.size
    }

    override fun onBindViewHolder(holder: MyProductsViewHolder, position: Int) {
        val product = productsList[position]
        holder.bind(product, context)
    }


    fun setProductsList(poolList: List<MyProductsView>) {
        this.productsList.clear()
        this.productsList.addAll(poolList)
        notifyDataSetChanged()
    }

   /* override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSearch = constraint.toString()
                if (charSearch.isEmpty()) {
                    productFilterList = productsList.toCollection(ArrayList())
                } else {
                    val resultList = ArrayList<MyProductsView>()
                    for (row in productsList) {
                        if (row.name.toLowerCase(Locale.ROOT)
                                .contains(charSearch.toLowerCase(Locale.ROOT))
                        ) {
                            resultList.add(row)
                        }
                    }
                    productFilterList = resultList
                }
                val filterResults = FilterResults()
                filterResults.values = productFilterList
                return filterResults
            }

            @Suppress("UNCHECKED_CAST")
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                productFilterList = results?.values as ArrayList<MyProductsView>
                notifyDataSetChanged()
            }

        }

    }*/

}