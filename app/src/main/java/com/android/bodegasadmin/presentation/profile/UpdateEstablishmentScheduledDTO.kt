package com.android.bodegasadmin.presentation.profile

import com.google.gson.annotations.SerializedName

data class UpdateEstablishmentScheduledDTO (
    @SerializedName("operationSchedule")
    var operationSchedule : List<Int>,

    @SerializedName("shippingSchedule")
    var shippingSchedule : List<Int>,

    @SerializedName("updateUser")
    var updateUser : String
)