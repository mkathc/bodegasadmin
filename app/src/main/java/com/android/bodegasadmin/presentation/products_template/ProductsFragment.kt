package com.android.bodegasadmin.presentation.products_template

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.AutoCompleteTextView
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.bodegasadmin.R
import com.android.bodegasadmin.domain.util.Status
import com.android.bodegasadmin.presentation.myproducts.MyProductsViewModel
import com.android.bodegasadmin.presentation.products_template.adapter.ProductsAdapter
import com.android.bodegasadmin.presentation.products_template.adapter.ProductsViewHolder
import com.android.bodegasadmin.presentation.products_template.model.ProductsView
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.fragment_products.*
import kotlinx.android.synthetic.main.fragment_products.search
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class ProductsFragment : Fragment(), ProductsViewHolder.ViewHolderListener {

    companion object {
        const val TAG = "ProductsFragment"

        @JvmStatic
        fun newInstance() =
            ProductsFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }

    private var listener: OnFragmentInteractionListener? = null
    private var adapter = ProductsAdapter(this)
    private lateinit var productList: List<ProductsView>
    private val productsViewModel: ProductsViewModel by viewModel()
    private val myProductsViewModel: MyProductsViewModel by sharedViewModel()

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_products, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeListProducts()
        observerAddProducts()
        searchBehavior()
    }

    private fun searchBehavior() {
        Log.e("db","funcion search behavior: line 84")
        search.queryHint = "Buscar"
        search.findViewById<AutoCompleteTextView>(R.id.search_src_text).threshold = 1
        search.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                query?.let {
                    productsViewModel.getProductsTemplate(it)
                }
                return false
            }

            override fun onQueryTextChange(query: String?): Boolean {
                return true
            }
        })
    }

    private fun setRecyclerView() {
        val linearLayoutManagerStores = LinearLayoutManager(context)
        rvProducts.layoutManager = linearLayoutManagerStores
        rvProducts.adapter = adapter
        adapter.setProductsList(productList)
    }

    override fun onClickProducts(position: Int) {
        val productView = productList[position]
        getDialogAddProduct(
            productView.productTemplateId,
            productView.name,
            productView.unitMeasure
        )
    }

    private fun observeListProducts() {

        productsViewModel.listProducts.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    search.setIconified(true)
                    if (isOpenKeyboard()){
                        hideKeyboard()
                    }
                    listener!!.hideLoading()
                    productList = it.data
                    if (productList.isNotEmpty()) {
                        containerEmptyState.visibility = View.GONE
                        rvProducts.visibility = View.VISIBLE
                        setRecyclerView()

                    } else {
                        containerEmptyState.visibility = View.VISIBLE
                        rvProducts.visibility = View.GONE
                    }
                }

                Status.ERROR -> {
                    listener!!.hideLoading()
                    if (isOpenKeyboard()){
                        hideKeyboard()
                    }
                    Snackbar.make(
                        productView,
                        "Ha ocurrido un error al obtener los productos",
                        Snackbar.LENGTH_SHORT
                    ).show()
                }

                Status.LOADING -> {
                    listener!!.showLoading()
                }
            }
        })

        productsViewModel.getProductsTemplate("")

    }

    private fun getDialogAddProduct(
        productId: Int,
        productName: String,
        productUnitMeasure: String
    ) {
        val dialog = Dialog(context!!)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.layout_dialog_add_product)
        dialog.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        val boxPrice = dialog.findViewById(R.id.boxPrice) as TextInputLayout
        val etPrice = dialog.findViewById(R.id.etPrice) as TextInputEditText
        val btnAddProduct = dialog.findViewById(R.id.btnAddProduct) as Button
        val btnNoStock = dialog.findViewById(R.id.btnNoStock) as TextView
        val btnStock = dialog.findViewById(R.id.btnStock) as TextView
        val tvNameProduct = dialog.findViewById(R.id.tvNameProduct) as TextView

        tvNameProduct.text = String.format("%s x %s", productName, productUnitMeasure)

        btnNoStock.setOnClickListener {
            btnNoStock.setTextColor(
                ContextCompat.getColor(
                    context!!,
                    R.color.color_brand_primary_default
                )
            )
            btnStock.setTextColor(ContextCompat.getColor(context!!, R.color.color_text_grey_100))
        }

        btnStock.setOnClickListener {
            btnStock.setTextColor(
                ContextCompat.getColor(
                    context!!,
                    R.color.color_brand_primary_default
                )
            )
            btnNoStock.setTextColor(ContextCompat.getColor(context!!, R.color.color_text_grey_100))
        }

        btnAddProduct.setOnClickListener {
            if (etPrice.text!!.isNotEmpty()) {
                val price = etPrice.text.toString()
                productsViewModel.addProducts(productId, price)
                dialog.dismiss()
            } else {
                boxPrice.isErrorEnabled = true
                boxPrice.error = "No ha agregado un precio"
            }
        }

        dialog.show()
    }

    private fun observerAddProducts() {
        productsViewModel.addProducts.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    listener!!.hideLoading()
                    Snackbar.make(
                        productView,
                        "Producto agregado correctamente",
                        Snackbar.LENGTH_SHORT
                    ).show()
                    myProductsViewModel.getMyProducts("")
                }

                Status.ERROR -> {
                    listener!!.hideLoading()
                    if(it.message != ""){
                        Snackbar.make(
                            productView,
                            it.message!!,
                            Snackbar.LENGTH_LONG
                        ).show()
                    }else{
                        Snackbar.make(
                            productView,
                            "Ha ocurrido un error al agregar su producto",
                            Snackbar.LENGTH_SHORT
                        ).show()
                    }
                }

                Status.LOADING -> {
                    listener!!.showLoading()
                }
            }
        })
    }

    fun refreshList(list: List<ProductsView>) {
        adapter.setProductsList(list)
        listener!!.hideLoading()
    }

    interface OnFragmentInteractionListener {
        fun showLoading()
        fun hideLoading()
    }

    fun hideKeyboard() {
        val imm =
            activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view!!.windowToken, 0);
    }


    fun isOpenKeyboard(): Boolean {
        val imm =
            activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        return imm.isAcceptingText
    }
}