package com.android.bodegasadmin.presentation.ordersHistory.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DistanceView (
    val distanceInKms: String,
    val numberOfBlocks: Int,
    val travelTimeByCar: String,
    val travelTimeByBike: String,
    val travelTimeByWalking: String
) : Parcelable