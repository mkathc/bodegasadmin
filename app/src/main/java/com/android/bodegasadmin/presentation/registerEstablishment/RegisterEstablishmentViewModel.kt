package com.android.bodegasadmin.presentation.registerEstablishment

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.android.bodegasadmin.data.PreferencesManager
import com.android.bodegasadmin.domain.establishment.register.RegisterNewEstablishment
import com.android.bodegasadmin.domain.references.department.GetDepartments
import com.android.bodegasadmin.domain.references.district.GetDistricts
import com.android.bodegasadmin.domain.references.province.GetProvinces
import com.android.bodegasadmin.domain.util.Resource
import com.android.bodegasadmin.domain.util.Status
import com.android.bodegasadmin.presentation.establishment.mapper.EstablishmentLoginTokenViewMapper
import com.android.bodegasadmin.presentation.establishment.mapper.EstablishmentViewMapper
import com.android.bodegasadmin.presentation.establishment.mapper.RegisterEstablishmentBodyViewMapper
import com.android.bodegasadmin.presentation.establishment.model.EstablishmentLoginTokenView
import com.android.bodegasadmin.presentation.establishment.model.EstablishmentView
import com.android.bodegasadmin.presentation.establishment.model.RegisterEstablishmentBodyView
import com.android.bodegasadmin.presentation.references.DepartmentView
import com.android.bodegasadmin.presentation.references.DistrictView
import com.android.bodegasadmin.presentation.references.ProvinceView
import com.android.bodegasadmin.presentation.references.mapper.DepartmentViewMapper
import com.android.bodegasadmin.presentation.references.mapper.DistrictViewMapper
import com.android.bodegasadmin.presentation.references.mapper.ProvinceViewMapper
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class RegisterEstablishmentViewModel constructor(
    private val registerNewEstablishment: RegisterNewEstablishment,
    private val getDepartments: GetDepartments,
    private val departmentViewMapper: DepartmentViewMapper,
    private val getProvinces: GetProvinces,
    private val provincesViewMapper: ProvinceViewMapper,
    private val getDistricts: GetDistricts,
    private val districtsViewMapper: DistrictViewMapper,
    private val establishmentViewMapper: EstablishmentViewMapper,
    private val registerEstablishmentBodyViewMapper: RegisterEstablishmentBodyViewMapper,
    private val establishmentLoginViewMapper: EstablishmentLoginTokenViewMapper
) :
    ViewModel(), CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO

    private val _registerEstablishment = MutableLiveData<Resource<EstablishmentLoginTokenView>>()
    val registerEstablishment: LiveData<Resource<EstablishmentLoginTokenView>> = _registerEstablishment

    private val _departments = MutableLiveData<Resource<List<DepartmentView>>>()
    val departments: LiveData<Resource<List<DepartmentView>>> = _departments

    private val _provinces = MutableLiveData<Resource<List<ProvinceView>>>()
    val provinces: LiveData<Resource<List<ProvinceView>>> = _provinces

    private val _districts = MutableLiveData<Resource<List<DistrictView>>>()
    val districts: LiveData<Resource<List<DistrictView>>> = _districts

    fun sendRegisterEstablishment(registerBodyEstablishment: RegisterEstablishmentBodyView) {
        _registerEstablishment.value = Resource(
            Status.LOADING,
            EstablishmentLoginTokenView("",""
            ),
            ""
        )
        viewModelScope.launch {

            val registerResult = registerNewEstablishment.registerEstablishment(
                registerEstablishmentBodyViewMapper.mapToUseCase(registerBodyEstablishment)
            )

            if (registerResult.status == Status.SUCCESS) {
                val establishmentView = establishmentLoginViewMapper.mapToView(registerResult.data)
                PreferencesManager.getInstance().setEstablishmentLoginTokenSelected(establishmentView)
                _registerEstablishment.value = Resource(registerResult.status, establishmentView, "")
            } else {
                _registerEstablishment.value = Resource(
                    registerResult.status,
                    establishmentLoginViewMapper.mapToView(registerResult.data),
                    registerResult.message
                )
            }
        }
    }

    fun getDepartments() {
        viewModelScope.launch {
            val departmentResult = getDepartments.getDepartments("", "")
            _departments.value = Resource(Status.LOADING, mutableListOf(), "")
            if (departmentResult.status == Status.SUCCESS) {
                val departmentResultData = departmentResult.data
                if (departmentResultData.isNotEmpty()) {
                    val departmentView =
                        departmentResultData.map { departmentViewMapper.mapToView(it) }
                    _departments.value = Resource(Status.SUCCESS, departmentView, "")
                } else {
                    _departments.value = Resource(Status.SUCCESS, mutableListOf(), "")
                }
            } else {
                _departments.value = Resource(Status.ERROR, mutableListOf(), "")
            }
        }
    }

    fun getProvinces() {
        viewModelScope.launch {
            val provinceResult = getProvinces.getProvince("", "", "")
            _provinces.value = Resource(Status.LOADING, mutableListOf(), "")
            if (provinceResult.status == Status.SUCCESS) {
                val provinceResultData = provinceResult.data
                if (provinceResultData.isNotEmpty()) {
                    val provinceView = provinceResultData.map { provincesViewMapper.mapToView(it) }
                    _provinces.value = Resource(Status.SUCCESS, provinceView, "")
                } else {
                    _provinces.value = Resource(Status.SUCCESS, mutableListOf(), "")
                }
            } else {
                _provinces.value = Resource(Status.ERROR, mutableListOf(), "")
            }
        }
    }

    fun getDistricts() {
        viewModelScope.launch {
            val districtResult = getDistricts.getDistrict("", "", "", "")
            _districts.value = Resource(Status.LOADING, mutableListOf(), "")
            if (districtResult.status == Status.SUCCESS) {
                val districtResultData = districtResult.data
                if (districtResultData.isNotEmpty()) {
                    val districtView = districtResultData.map { districtsViewMapper.mapToView(it) }
                    _districts.value = Resource(Status.SUCCESS, districtView, "")
                } else {
                    _districts.value = Resource(Status.SUCCESS, mutableListOf(), "")
                }
            } else {
                _districts.value = Resource(Status.ERROR, mutableListOf(), "")
            }
        }
    }
}