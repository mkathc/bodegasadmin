package com.android.bodegasadmin.presentation.products_template.adapter

import android.content.Context
import android.view.View
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.android.bodegasadmin.R
import com.android.bodegasadmin.presentation.products_template.model.ProductsView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.row_products.view.*

class ProductsViewHolder(itemView: View, viewHolderListener: ViewHolderListener) :
    RecyclerView.ViewHolder(itemView) {
    val productName: TextView = itemView.tvNameProduct
    val productDescription: TextView = itemView.tvDescriptionProduct
    val imageProduct: ImageView = itemView.ivImageProduct
    val holder : ViewHolderListener = viewHolderListener

    fun bind(product: ProductsView, context: Context) {
        productName.text = product.name
        productDescription.text = product.description

        if (product.imageProduct!!.isNotEmpty()) {

            val circularProgressDrawable = CircularProgressDrawable(context)
            circularProgressDrawable.strokeWidth = 5f
            circularProgressDrawable.centerRadius = 30f
            circularProgressDrawable.start()

            Glide.with(context)  // this
                .load(product.imageProduct)
                .placeholder(circularProgressDrawable)
                .error(context.getDrawable(R.drawable.ic_image_black_24dp))
                .into(imageProduct)
        }

        itemView.setOnClickListener {
            holder.onClickProducts(
                layoutPosition
            )
        }
    }

    interface ViewHolderListener {
        fun onClickProducts(
            position: Int
        )

    }
}

