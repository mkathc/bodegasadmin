package com.android.bodegasadmin.presentation.establishment.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class EstablishmentView(
    val storeId: Int,
    val storeName: String,
    val storeAddress: String,
    val storeEmail:String,
    val latitude: Double,
    val longitude: Double,
    val storeScheduleShipping:List<ScheduleView>,
    val storeScheduleOperation:List<ScheduleView>,
    val documentNumber:String,
    var image:String?="",
    val hasDelivery: Boolean,
    val storeBusinessName: String,
    val storeDepartment: String,
    val storeProvince: String,
    val storeDistrict: String,
    val storeDni: String,
    val storeUrbanization: String,
    val name: String,
    val lastNamePaternal: String,
    val lastNameMaternal: String,
    val dni:String,
    val phone: String,
    val ruc: String,
    val establishmentType: String,
    val deliveryCharge: Boolean,
    val paymentMethod : List<PaymentMethodView>
) : Parcelable