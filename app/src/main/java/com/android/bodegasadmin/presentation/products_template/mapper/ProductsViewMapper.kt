package com.android.bodegasadmin.presentation.products_template.mapper

import com.android.bodegasadmin.domain.products_template.Products
import com.android.bodegasadmin.presentation.Mapper
import com.android.bodegasadmin.presentation.products_template.model.ProductsView

class ProductsViewMapper : Mapper<ProductsView, Products> {

    override fun mapToView(type: Products): ProductsView {
        return ProductsView(
            type.productTemplateId,
            type.code,
            type.name,
            type.description,
            type.unitMeasure,
            type.imageProduct,
            type.tag
        )
    }

}