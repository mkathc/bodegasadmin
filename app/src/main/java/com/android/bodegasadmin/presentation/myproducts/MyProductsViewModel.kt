package com.android.bodegasadmin.presentation.myproducts

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.android.bodegasadmin.domain.my_products.GetMyProducts
import com.android.bodegasadmin.domain.my_products.UpdateMyProducts
import com.android.bodegasadmin.domain.util.Resource
import com.android.bodegasadmin.domain.util.Status
import com.android.bodegasadmin.presentation.myproducts.mapper.MyProductsViewMapper
import com.android.bodegasadmin.presentation.myproducts.model.MyProductsView
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class MyProductsViewModel constructor(
    private val getProductsBySubCategory: GetMyProducts,
    private val productsViewMapper: MyProductsViewMapper,
    private val updateMyProducts: UpdateMyProducts
) : ViewModel(), CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO

    private val _listProducts = MutableLiveData<Resource<List<MyProductsView>>>()
    val listProducts: LiveData<Resource<List<MyProductsView>>> = _listProducts


    private val _updateProduct = MutableLiveData<Resource<Boolean>>()
    val updateProduct: LiveData<Resource<Boolean>> = _updateProduct

    fun getMyProducts(searchText: String) {
        viewModelScope.launch {
            _listProducts.value =
                Resource(Status.LOADING, mutableListOf(), "")

            val productsResult =
                getProductsBySubCategory.getMyProducts(searchText)

            if (productsResult.status == Status.SUCCESS) {

                val productsResultData = productsResult.data

                if (productsResultData.isNotEmpty()) {
                    val suppliersView = productsResultData.map { productsViewMapper.mapToView(it) }
                    _listProducts.value = Resource(Status.SUCCESS, suppliersView, "")

                } else {
                    _listProducts.value = Resource(Status.SUCCESS, mutableListOf(), "")
                }
            } else {
                _listProducts.value =
                    Resource(Status.ERROR, mutableListOf(), "")
            }
        }
    }

    fun updateMyProduct(storeProductId: Int, price: Double, stock: String) {
        _updateProduct.value =
            Resource(Status.LOADING, false, "")
        viewModelScope.launch {

            val updateProductsResult =
                updateMyProducts.updateMyProduct(storeProductId, price, stock)

            if (updateProductsResult.status == Status.SUCCESS) {
                _updateProduct.value = Resource(Status.SUCCESS, true, updateProductsResult.message)

            } else {
                _updateProduct.value = Resource(
                    updateProductsResult.status,
                    updateProductsResult.data,
                    updateProductsResult.message
                )
            }
        }
    }


}