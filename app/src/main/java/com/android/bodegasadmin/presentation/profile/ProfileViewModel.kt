package com.android.bodegasadmin.presentation.profile

import android.util.Log
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.android.bodegasadmin.data.PreferencesManager
import com.android.bodegasadmin.domain.establishment.Establishment
import com.android.bodegasadmin.domain.establishment.update.UpdateDataEstablishment
import com.android.bodegasadmin.domain.establishment.update.UpdatePassword
import com.android.bodegasadmin.domain.establishment.update.UpdatePhotoUser
import com.android.bodegasadmin.domain.establishment.update.UpdateSchedules
import com.android.bodegasadmin.domain.util.Resource
import com.android.bodegasadmin.domain.util.Status
import com.android.bodegasadmin.presentation.establishment.mapper.EstablishmentViewMapper
import com.android.bodegasadmin.presentation.establishment.model.EstablishmentView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class ProfileViewModel constructor(
    private val updatePhotoUser: UpdatePhotoUser,
    private val updateEstablishment: UpdateDataEstablishment,
    private val updatePasswordService: UpdatePassword,
    private val establishmentMapper: EstablishmentViewMapper,
    private val updateSchedules: UpdateSchedules
) : ViewModel(), CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO

    //de mutable a live data
    private val _updatePhoto = MutableLiveData<Resource<Boolean>>()
    val updatePhoto : LiveData<Resource<Boolean>> = _updatePhoto

    private val _updatePassword = MutableLiveData<Resource<Boolean>>()
    val updatePassword : LiveData<Resource<Boolean>> = _updatePassword

    private val _updateEstablishmentEntity = MutableLiveData<Resource<Establishment>>()
    val updateEstablishmentEntity : LiveData<Resource<Establishment>> = _updateEstablishmentEntity

    private val _getEstablishmentView = MutableLiveData<Resource<EstablishmentView>>()
    val getEstablishmentView : LiveData<Resource<EstablishmentView>> = _getEstablishmentView


    fun updatePhotoOfUser(customerId:Int, filePath:String) {
        _updatePhoto.value = Resource(Status.LOADING, false, "")
        viewModelScope.launch {

            val updateResult = updatePhotoUser.updatePhoto(customerId, filePath)
            if(updateResult.status == Status.SUCCESS){
                if(updateResult.data){
                    _updatePhoto.value = Resource(Status.SUCCESS, updateResult.data, updateResult.message)
                }
                else{
                    _updatePhoto.value = Resource(Status.ERROR, updateResult.data, "")
                }
            }
            else{
                _updatePhoto.value = Resource(Status.ERROR, updateResult.data, "")
            }

        }
    }

    fun updateDataEstablishment(customerId:Int, body:UpdateEstablishmentBody) {
        viewModelScope.launch {
            val updateResult = updateEstablishment.updateEstablishment(customerId, body)
            if(updateResult.status == Status.SUCCESS){
                PreferencesManager.getInstance().setEstablishmentSelected(establishmentMapper.mapToView(updateResult.data))
                Log.e("db","updateDataEstablishment:::: ID: "+ PreferencesManager.getInstance().getEstablishmentSelected().storeId)
                _updateEstablishmentEntity.value = Resource(Status.SUCCESS, updateResult.data, updateResult.message)
            }
            else{
                _updateEstablishmentEntity.value = Resource(Status.ERROR, updateResult.data, "")
            }
        }
    }

    fun updatePassword(body: ChangePasswordDtoBody) {
        _updatePassword.value = Resource(Status.LOADING, false, "")
        viewModelScope.launch {
            val updateResult = updatePasswordService.updatePassword(body)
            if(updateResult.status == Status.SUCCESS){
                if(updateResult.data){
                    _updatePassword.value = Resource(Status.SUCCESS, updateResult.data, updateResult.message)
                }
                else{
                    _updatePassword.value = Resource(Status.ERROR, updateResult.data, "")
                }
            }
            else{
                _updatePassword.value = Resource(Status.ERROR, updateResult.data, "")
            }

        }
    }

    fun getDataEstablishment() {
        viewModelScope.launch {
            val userView = PreferencesManager.getInstance().getEstablishmentSelected()
            _getEstablishmentView.value = Resource(Status.SUCCESS, userView, "")
        }
    }

    fun updateShifts(establishmentId:Int, body: UpdateEstablishmentScheduledDTO){
        viewModelScope.launch {
            val updateResult = updateSchedules.updateSchedule(establishmentId, body)
            if(updateResult.status == Status.SUCCESS){
                if(updateResult.data){
                    _updatePassword.value = Resource(Status.SUCCESS, updateResult.data, updateResult.message)
                }
                else{
                    _updatePassword.value = Resource(Status.ERROR, updateResult.data, "")
                }
            }
            else{
                _updatePassword.value = Resource(Status.ERROR, updateResult.data, "")
            }
        }
    }
}