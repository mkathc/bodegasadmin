package com.android.bodegasadmin.presentation.ordersHistory

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.viewpager.widget.ViewPager
import com.android.bodegasadmin.R


import com.google.android.material.tabs.TabLayout

class OrdersHistory : Fragment() {

    // TODO: Customize parameters
    private var columnCount = 1
    private lateinit var ordersHistoryCollectionPagerAdapter: OrdersHistoryCollectionPagerAdapter
    private lateinit var viewPager: ViewPager
    private var listener: OnFragmentInteractionListener? = null

    companion object {
        const val TAG = "OrdersHistory"

        @JvmStatic
        fun newInstance() =
            OrdersHistory().apply {
                arguments = Bundle().apply {

                }
            }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_orders_history_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        ordersHistoryCollectionPagerAdapter =
            OrdersHistoryCollectionPagerAdapter(childFragmentManager)
        viewPager = view.findViewById(R.id.pager)
        viewPager.adapter = ordersHistoryCollectionPagerAdapter
        viewPager.offscreenPageLimit = 0
        val tabLayout = view.findViewById(R.id.tab_layout) as TabLayout
        tabLayout.setupWithViewPager(viewPager)

        viewPager?.addOnPageChangeListener(object : ViewPager.SimpleOnPageChangeListener() {
            //override only methods you need, not all of them
            override fun onPageSelected(position: Int) {
            }
        })
    }


    interface OnFragmentInteractionListener {
        //fun refreshList(position:Int)
    }
}
