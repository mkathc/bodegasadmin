package com.android.bodegasadmin.presentation.profile

import android.os.Bundle
import com.android.bodegasadmin.R
import com.android.bodegasadmin.data.PreferencesManager
import kotlinx.android.synthetic.main.activity_update_data_establishment.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import com.android.bodegasadmin.domain.util.Status
import com.android.bodegasadmin.presentation.BaseActivity
import com.android.bodegasadmin.presentation.allPaymentMethods.GetAllPaymentMethodsViewModel
import com.android.bodegasadmin.presentation.allPaymentMethods.model.AllPaymentMethodsView
import com.android.bodegasadmin.presentation.allSchedules.GetAllSchedulesViewModel
import com.android.bodegasadmin.presentation.allSchedules.model.AllScheduleView
import com.android.bodegasadmin.presentation.establishment.model.PaymentMethodView
import com.android.bodegasadmin.presentation.establishment.model.ScheduleView
import com.android.bodegasadmin.presentation.references.DepartmentView
import com.android.bodegasadmin.presentation.references.DistrictView
import com.android.bodegasadmin.presentation.references.ProvinceView
import com.android.bodegasadmin.presentation.registerEstablishment.RegisterEstablishmentViewModel
import com.android.bodegasadmin.presentation.utils.animation.LoadingAnimationImageView
import com.android.bodegasadmin.presentation.utils.animation.LoadingDots
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.activity_register_references.*
import kotlinx.android.synthetic.main.activity_update_data_establishment.boton_set_departamento
import kotlinx.android.synthetic.main.activity_update_data_establishment.boton_set_distrito
import kotlinx.android.synthetic.main.activity_update_data_establishment.boton_set_provincia
import kotlinx.android.synthetic.main.activity_update_data_establishment.boxreferencia
import kotlinx.android.synthetic.main.activity_update_data_establishment.departamentobox
import kotlinx.android.synthetic.main.activity_update_data_establishment.departamentoinput
import kotlinx.android.synthetic.main.activity_update_data_establishment.distritobox
import kotlinx.android.synthetic.main.activity_update_data_establishment.distritoinput
import kotlinx.android.synthetic.main.activity_update_data_establishment.inputreferencia
import kotlinx.android.synthetic.main.activity_update_data_establishment.provinciabox
import kotlinx.android.synthetic.main.activity_update_data_establishment.provinciainput
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlinx.android.synthetic.main.layout_loading.*
import kotlin.collections.HashMap

const val TYPE_BODEGA="Bodega"
const val ESTABLISHMENT_TYPE_BODEGA=1
const val TYPE_NO_BODEGA ="Tienda en Mercado"
const val ESTABLISHMENT_TYPE_NO_BODEGA=2
const val MESSAGE_ESTABLISHMENT_WITHOUT_DELIVERY_SCHEDULES="No cuenta con horarios de delivery."
const val HAS_DELIVERY="Si"
const val NOT_HAS_DELIVERY="No"
const val HAS_CHARGE_FOR_DELIVERY="Si"
const val NOT_HAVE_CHARGE_FOR_DELIVERY="No"

class UpdateDataEstablishmentActivity : BaseActivity() {
    private val profileViewModel: ProfileViewModel by viewModel()
    private val getAllPaymentMethodsViewModel: GetAllPaymentMethodsViewModel by viewModel()
    var latitudeSelected: String = ""
    var longitudeSelected: String = ""
    private var operationScheduleViewList = mutableListOf<AllScheduleView>()
    private var paymentMethodsViewList = mutableListOf<AllPaymentMethodsView>()
    private val registerViewModel: RegisterEstablishmentViewModel by viewModel()
    private var stringDeliveryList = mutableListOf<String>()
    private var stringPaymentMethodsList = mutableListOf<String>()
    private var departmentViewList: MutableList<DepartmentView> = mutableListOf()
    private var stringDeparmentList = ArrayList<CharSequence>()
    private var selectedDelivery = booleanArrayOf()
    private var selectedPaymentMethods = booleanArrayOf()
    private var provinceViewList: MutableList<ProvinceView> = mutableListOf()
    private var stringProvinceList = ArrayList<CharSequence>()
    private var deliveryIdSelected = mutableListOf<Int>()
    private var paymentMethodsIdSelected = mutableListOf<Int>()
    private var districtViewList: MutableList<DistrictView> = mutableListOf()
    private var stringDistrictList = ArrayList<CharSequence>()
    private var deliveryScheduleViewList = mutableListOf<AllScheduleView>()
    private var stringOperationList = mutableListOf<String>()
    private val getAllSchedulesViewModel: GetAllSchedulesViewModel by viewModel()
    var selectedDepartmentId: String = ""
    var selectedProvinceId: String = ""
    private var operationIdSelected: Int = 0
    var currentDate: String = ""
    private var loadingAnimationImageView: LoadingAnimationImageView? = null
    private var loadingDots: LoadingDots? = null
    private var schedulesDeliveryList = mutableListOf<String>()
    private var paymentMethodsList = mutableListOf<String>()
    private var idScheduleMap : HashMap<Int, String>  = HashMap<Int, String>()
    private var idPaymentMethodsMap : HashMap<Int, String>  = HashMap<Int, String>()
    private var currentSelectedScheduleDelivery : HashMap<Int, Int> = HashMap<Int, Int> ()
    private var currentSelectedPaymentMethodsDelivery : HashMap<Int, Int> = HashMap<Int, Int> ()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update_data_establishment)
        setOnClickListener()
        getDepartments()
        observeUpdateDataEstablishment()
        chargeData()
        updateData()
        updateAddress()
        cancelaryRegresar()
        showDeliverySchedule()
        btnAddOperationSchedule.setOnClickListener {
            showOperationSchedules()
        }
        boton_set_HorarioDelivery.setOnClickListener {
            showDialogDeliveryList()
        }
        botonSetPaymentMethodsToUpdate.setOnClickListener{
            showDialogPaymentMethods()
        }
    }

    fun chargeData() {
        boton_set_HorarioDelivery.isEnabled=false
        val user = PreferencesManager.getInstance().getEstablishmentSelected()
        Log.e("db", "*************************************valor user "+ user)
        inputName.setText(user.name)
        input_ape_paterno.setText(user.lastNamePaternal)
        input_ape_materno.setText(user.lastNameMaternal)
        input_dni.setText(user.dni)
        input_phone.setText(user.phone)
        inputNombreEmpresa.setText(user.storeBusinessName)
        //get payment methods from service
        var paymentMethodsDescription=""
        for(i: PaymentMethodView in PreferencesManager.getInstance().getEstablishmentSelected().paymentMethod) {
            Log.e("db", "Recorriendo ids: "+ i.paymentMethodId)
            paymentMethodsDescription+=' '+i.description
            i.paymentMethodId?.let { paymentMethodsIdSelected.add(it) }
            i.description?.let { paymentMethodsList.add(it) }
            idPaymentMethodsMap.put(i.paymentMethodId, i.description)
        }
        inputPaymentMethodsToUpdate.setText(paymentMethodsDescription)

        Log.e("db", "*************************************valor user.deliveryCharge "+ user.deliveryCharge)
        if(user.deliveryCharge) {
            inputChargeForDeliveryToUpdate.setText(HAS_CHARGE_FOR_DELIVERY)
            Log.e("db", "*************************************linea 138 valor inputChargeForDeliveryToUpdate "+ HAS_CHARGE_FOR_DELIVERY)
        }
        else {
            inputChargeForDeliveryToUpdate.setText(NOT_HAVE_CHARGE_FOR_DELIVERY)
            Log.e("db", "*************************************linea 142 valor inputChargeForDeliveryToUpdate "+ NOT_HAVE_CHARGE_FOR_DELIVERY)
        }
        //permitirEdicionChargeForDelivery()
        if (user.hasDelivery) {
            inputDelivery.setText(HAS_DELIVERY)
            var rangos =""
            for (i: ScheduleView in PreferencesManager.getInstance().getEstablishmentSelected().storeScheduleShipping) {
                    Log.e("db", "Recorriendo ids: "+ i.shippingScheduleId)
                    rangos +=' '+i.range
                    i.shippingScheduleId?.let { deliveryIdSelected.add(it) }
                    i.range?.let{schedulesDeliveryList.add(it) }
                    idScheduleMap.put(i.shippingScheduleId, i.range)
            }
            inputDeliveryHorario.setText(rangos)
            permitirEdicionHorariosDelivery()
            //permitirEdicionChargeForDelivery()
        } else {
            inputDelivery.setText(NOT_HAS_DELIVERY)
            inputChargeForDeliveryToUpdate.setText(NOT_HAVE_CHARGE_FOR_DELIVERY)
            Log.e("db", "*************************************linea 161 valor inputChargeForDeliveryToUpdate "+ NOT_HAVE_CHARGE_FOR_DELIVERY)
            deliveryIdSelected = mutableListOf<Int>()
            schedulesDeliveryList = mutableListOf<String>()
        }
        permitirEdicionChargeForDelivery()

        direccionInput.setText(user.storeAddress)
        departamentoinput.setText(user.storeDepartment)
        provinciainput.setText(user.storeProvince)
        distritoinput.setText(user.storeDistrict)
        inputreferencia.setText(user.storeUrbanization)
        latitudeSelected = user.latitude.toString()
        longitudeSelected = user.longitude.toString()
        input_tipoEstablecimiento.setText(user.establishmentType)
        input_ruc.setText(user.ruc)
        saveDataInLocalStorage() //next update
        inputHorarioAtencion.setText(user.storeScheduleOperation[0].range.toString())
        operationIdSelected=user.storeScheduleOperation[0].shippingScheduleId
        Log.e("db", "chargeData Recorriendo deliveryIdSelected: "+deliveryIdSelected.size+ " , datos: " + deliveryIdSelected.toString())
    }

    fun updateData() {
        btnGuardarCambios.setOnClickListener {
            var date = Date()
            val formatter = SimpleDateFormat("dd-MM-yyyy")
            currentDate = formatter.format(date)
            var establishmentTypeInt = 0
            if (input_tipoEstablecimiento.text.toString().equals(TYPE_BODEGA)) establishmentTypeInt = 1
            else establishmentTypeInt = 2

            if (validacionCampos()) {
                var derivery: String = ""
                if (inputDelivery.text.toString() == HAS_DELIVERY || inputDelivery.text.toString() == "S") {
                    inputDelivery.setText("S")
                } else inputDelivery.setText("N")
                val updateBody = UpdateEstablishmentBody(
                    direccionInput.text.toString(),
                    inputNombreEmpresa.text.toString(),
                    inputDelivery.text.toString(),
                    departamentoinput.text.toString(),
                    distritoinput.text.toString(),
                    input_dni.text.toString(),
                    establishmentTypeInt,
                    input_ape_materno.text.toString(),
                    input_ape_paterno.text.toString(),
                    latitudeSelected.toDouble(),
                    longitudeSelected.toDouble(),
                    inputName.text.toString(),
                    input_phone.text.toString(),
                    provinciainput.text.toString(),
                    input_ruc.text.toString(),
                    inputreferencia.text.toString(),
                    currentDate.toString(),
                    getChargeForDelivery(inputChargeForDeliveryToUpdate.text.toString()),
                    paymentMethodsIdSelected
                )
                updateHorariosAtencionandDelivery()
                Log.e("db", "dto enviado al update: " + updateBody.toString())
                profileViewModel.updateDataEstablishment(
                    PreferencesManager.getInstance().getEstablishmentSelected().storeId, updateBody
                )
            }
        }
    }

    private fun observeUpdateDataEstablishment() {
        profileViewModel.updateEstablishmentEntity.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    Snackbar.make(
                        profileview,
                        "Datos actualizados correctamente.",
                        Snackbar.LENGTH_LONG
                    ).show()
                    PreferencesManager.getInstance().cleanUpdateEstablishment()
                    finish()
                }
                Status.ERROR -> {

                    Snackbar.make(
                        profileview,
                        "Error al actualizar, intenta nuevamente.",
                        Snackbar.LENGTH_LONG
                    ).show()
                }
                Status.LOADING -> {
                }
            }
        })
    }

    fun updateAddress() {
        boton_set_direccion.setOnClickListener {
            saveDataInLocalStorage()
            nextActivity(null, UpdateDataAddressActivity::class.java, false)
        }
    }

    override fun onResume() {
        super.onResume()
        loadDataFromLocalStorage()
    }

    private fun loadDataFromLocalStorage() {
        val registerBody = PreferencesManager.getInstance().getBodyUpdateEstablishment()
        if (!registerBody.address.equals("")) direccionInput.setText(registerBody.address)
        if (!registerBody.name.equals("")) inputName.setText(registerBody.name)
        if (!registerBody.businessName.equals("")) inputNombreEmpresa.setText(registerBody.businessName)
        if (registerBody.delivery != "") inputDelivery.setText(registerBody.delivery)
        if (registerBody.department != "") departamentoinput.setText(registerBody.department)
        if (registerBody.district != "") distritoinput.setText(registerBody.district)
        if (registerBody.dni != "") input_dni.setText(registerBody.dni)
        if (registerBody.lastNameMaternal != "") input_ape_materno.setText(registerBody.lastNameMaternal)
        if (registerBody.lastNamePaternal != "") input_ape_paterno.setText(registerBody.lastNamePaternal)
        if (registerBody.latitude.toDouble() != 0.0) latitudeSelected =
            registerBody.latitude.toString()
        if (registerBody.longitude.toDouble() != 0.0) longitudeSelected =
            registerBody.longitude.toString()
        if (registerBody.name != "") inputName.setText(registerBody.name)
        if (registerBody.phoneNumber != "") input_phone.setText(registerBody.phoneNumber)
        if (registerBody.province != "") provinciainput.setText(registerBody.province)
        if (registerBody.urbanization != "") inputreferencia.setText(registerBody.urbanization)
        if (registerBody.ruc != "") input_ruc.setText(registerBody.ruc)
        if (registerBody.establishmentType.toString() != "") {
            if (registerBody.establishmentType.toString().contains(ESTABLISHMENT_TYPE_BODEGA.toString())) input_tipoEstablecimiento.setText(TYPE_BODEGA)
            else input_tipoEstablecimiento.setText(TYPE_NO_BODEGA)
        }
    }

    fun saveDataInLocalStorage() {
        var establishmentTypeInt = 0
        if (input_tipoEstablecimiento.text.toString().contains(TYPE_BODEGA)) establishmentTypeInt= ESTABLISHMENT_TYPE_BODEGA
        else establishmentTypeInt = ESTABLISHMENT_TYPE_NO_BODEGA
        val updateBody = UpdateEstablishmentBody(
            direccionInput.text.toString(),
            inputNombreEmpresa.text.toString(),
            inputDelivery.text.toString(),
            departamentoinput.text.toString(),
            distritoinput.text.toString(),
            input_dni.text.toString(),
            establishmentTypeInt,
            input_ape_materno.text.toString(),
            input_ape_paterno.text.toString(),
            latitudeSelected.toDouble(),
            longitudeSelected.toDouble(),
            inputName.text.toString(),
            input_phone.text.toString(),
            provinciainput.text.toString(),
            input_ruc.text.toString(),
            inputreferencia.text.toString(),
            currentDate.toString(),
            getChargeForDelivery(inputChargeForDeliveryToUpdate.text.toString()),
            paymentMethodsIdSelected
        )
        PreferencesManager.getInstance().saveBodyUpdateEstablishment(updateBody)
    }

    private fun getChargeForDelivery(value: String): Boolean {
        return value == HAS_CHARGE_FOR_DELIVERY
    }

    private fun setOnClickListener() {
        boton_set_departamento.setOnClickListener {
            showDialogDepartments()
        }
        boton_set_provincia.setOnClickListener {
            showDialogProvincia()
        }
        boton_set_distrito.setOnClickListener {
            showDialogDistrito()
        }
        boton_set_Delivery.setOnClickListener {
            showDialogDeliveryOptions()
        }
        boton_set_TipoEstablecimiento.setOnClickListener {
            showDialogEstablishmentTypeOptions()
        }
        botonSetChargeForDeliveryToUpdate.setOnClickListener {
            showDialogSetChargeForDelivery()
        }
    }

    private fun validateDataToRegister(): Boolean {
        return validarAlerts(departamentoinput, departamentobox) &&
                validarAlerts(provinciainput, provinciabox) &&
                validarAlerts(distritoinput, distritobox) &&
                extensionValidation(inputreferencia, boxreferencia, 1)
    }

    private fun extensionValidation(
        campo_input: TextInputEditText,
        inputTitle: TextInputLayout,
        minimunLenght: Int
    ): Boolean {
        val campo = campo_input.text.toString()
        inputTitle.error = null
        if ((campo.isEmpty()) || campo.length < minimunLenght) {
            inputTitle.error = "Introduce una referencia más extensa."
            return false
        }
        return true
    }

    private fun validarAlerts(
        campo_input: TextInputEditText,
        title_input: TextInputLayout
    ): Boolean {
        val campo = campo_input.text.toString()
        title_input.error = null
        if (campo == "Seleccionar") {
            title_input.error = "Selecciona un item de la lista."
            return false
        }
        return true
    }


    private fun showDialogDepartments() {
        lateinit var dialog: AlertDialog
        val array: Array<CharSequence> = stringDeparmentList.map { i -> i as CharSequence }.toTypedArray()
        var decoy:Int = -1
        var selected= 0
        var previosDeparment= departamentoinput.text.toString()
        for(i in departmentViewList){
            decoy++
            if (i.name == departamentoinput.text.toString()) {
                selected = decoy
            }
        }
        val builder = AlertDialog.Builder(this)
        var selectedValue: CharSequence = ""
        builder.setTitle("Elige un departamento.")
        builder.setSingleChoiceItems(array, selected)
        { _, which ->
            selectedValue = array[which]
            departamentoinput.setText(selectedValue)
            if(departamentoinput.text.toString()!= previosDeparment){
                provinciainput.setText("Seleccionar")
                distritoinput.setText("Seleccionar")
            }
            dialog.dismiss()
            boton_set_provincia.isEnabled = true


            for (i in departmentViewList) {
                if (i.name == selectedValue) {
                    selectedDepartmentId = i.id
                }

            }
            getProvinces()
        }
        dialog = builder.create()
        dialog.show()
    }

    private fun getProvinces() {
        registerViewModel.provinces.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    provinceViewList = it.data.toMutableList()
                    getProvinceStringsArray()
                }
                Status.ERROR -> {
                    Toast.makeText(
                        applicationContext,
                        "Ocurrió un error al cargar las provincias",
                        Toast.LENGTH_LONG
                    ).show()
                }
                Status.LOADING -> {
                }
            }
        })
        registerViewModel.getProvinces()
    }

    private fun getDepartments() {
        registerViewModel.departments.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    departmentViewList = it.data.toMutableList()
                    getDepartmentStringsArray()
                }
                Status.ERROR -> {
                    Toast.makeText(
                        applicationContext,
                        "Ocurrió un error al cargar los departamentos",
                        Toast.LENGTH_LONG
                    ).show()
                }

                Status.LOADING -> {
                }
            }
        })
        registerViewModel.getDepartments()
    }

    private fun getDepartmentStringsArray() {
        stringDeparmentList.clear()
        departmentViewList.forEach {
            stringDeparmentList.add(it.name)
        }
    }

    private fun getProvinceStringsArray() {
        stringProvinceList.clear()
        provinceViewList.forEach {
            if (it.department_id == selectedDepartmentId) {
                stringProvinceList.add(it.name)
            }
        }
    }

    private fun showDialogProvincia() {
        lateinit var dialog: AlertDialog
        val array: Array<CharSequence> =
            stringProvinceList.map { i -> i as CharSequence }.toTypedArray()
        var previosProvincia= provinciainput.text.toString()
        var counter=0
        var valueselected=-1
        for(i in array){
            if(provinciainput.text.toString().contains(i))
                valueselected=counter
            counter++
        }
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Elige una provincia.")
        var selectedValue: CharSequence = ""
        builder.setSingleChoiceItems(array, valueselected) { _, which ->
            selectedValue = array[which]
            provinciainput.setText(selectedValue)
            if(provinciainput.text.toString()!= previosProvincia){
                distritoinput.setText("Seleccionar")
            }
            dialog.dismiss()
            for (i in provinceViewList) {
                if (i.name == selectedValue) {
                    selectedProvinceId = i.id
                }
            }
            obtenerDistritos()
        }
        dialog = builder.create()
        dialog.show()
    }

    private fun obtenerDistritos() {
        boton_set_distrito.isEnabled = true
        registerViewModel.districts.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    districtViewList = it.data.toMutableList()
                    getDistrictsStringsArray()
                }
                Status.ERROR -> {
                    Toast.makeText(
                        applicationContext,
                        "Ocurrió un error al cargar los distritos",
                        Toast.LENGTH_LONG
                    ).show()
                }
                Status.LOADING -> {
                }
            }
        })
        registerViewModel.getDistricts()
    }

    private fun getDistrictsStringsArray() {
        stringDistrictList.clear()
        districtViewList.forEach {
            if (it.department_id == selectedDepartmentId && it.province_id == selectedProvinceId) {
                stringDistrictList.add(it.name)
            }
        }
    }

    private fun showDialogDistrito() {
        lateinit var dialog: AlertDialog
        val array: Array<CharSequence> =
            stringDistrictList.map { i -> i as CharSequence }.toTypedArray()
        val builder = AlertDialog.Builder(this)
        var counter=0
        var valueselected=-1
        for(i in array){
            if(distritoinput.text.toString().contains(i))
                valueselected=counter
            counter++
        }
        builder.setTitle("Elige un distrito.")
        var selectedValue: CharSequence = ""
        builder.setSingleChoiceItems(array, valueselected) { _, which ->
            selectedValue = array[which]
            distritoinput.setText(selectedValue)
            dialog.dismiss()
        }
        dialog = builder.create()
        dialog.show()
    }

    private fun showDialogDeliveryOptions() {
        lateinit var dialog: AlertDialog
        var stringOptionsList = ArrayList<CharSequence>()
        stringOptionsList.add(HAS_DELIVERY)
        stringOptionsList.add(NOT_HAS_DELIVERY)
        var preselectedvalue=-1
        Log.e("db","valor hasDelivery: "+inputDelivery.text.toString())
        if(inputDelivery.text.toString().contains(HAS_DELIVERY)) preselectedvalue = 0
        else preselectedvalue = 1
        val array: Array<CharSequence> =
            stringOptionsList.map { i -> i as CharSequence }.toTypedArray()
        val builder = AlertDialog.Builder(this)
        builder.setTitle("¿Tienes delivery?")
        var selectedValue: CharSequence = ""
        builder.setSingleChoiceItems(array, preselectedvalue) { _, which ->
            selectedValue = array[which]
            inputDelivery.setText(selectedValue)
            permitirEdicionHorariosDelivery()
            permitirEdicionChargeForDelivery()
            dialog.dismiss()
        }
        dialog = builder.create()
        dialog.show()
    }

    private fun permitirEdicionHorariosDelivery() {
        Log.e("dc", "inputDelivery.text.toString()::::: " + inputDelivery.text.toString())
        if (inputDelivery.text.toString().contains(HAS_DELIVERY) || inputDelivery.text.toString()
                .equals("S")
        ) {
            boton_set_HorarioDelivery.isEnabled = true;
        } else {
            boton_set_HorarioDelivery.isEnabled = false;
            inputDeliveryHorario.setText("")
            deliveryIdSelected = mutableListOf<Int>()
            schedulesDeliveryList = mutableListOf<String>()
            Log.e("db", "permitirEdicionHorariosDelivery Recorriendo deliveryIdSelected: "+deliveryIdSelected.size+ " , datos: " + deliveryIdSelected.toString())
        }
    }

    private fun permitirEdicionChargeForDelivery() {
        if (inputDelivery.text.toString().contains(HAS_DELIVERY) || inputDelivery.text.toString() == "S") {
            botonSetChargeForDeliveryToUpdate.isEnabled = true;
        } else {
            botonSetChargeForDeliveryToUpdate.isEnabled = false;
            inputChargeForDeliveryToUpdate.setText(NOT_HAVE_CHARGE_FOR_DELIVERY)
            Log.e("db", "************************************* linea 610 valor inputChargeForDeliveryToUpdate "+ NOT_HAVE_CHARGE_FOR_DELIVERY)
        }
    }

    private fun showDialogEstablishmentTypeOptions() {
        lateinit var dialog: AlertDialog
        var stringOptionsList = ArrayList<CharSequence>()
        var preSelected=-1;
        if(input_tipoEstablecimiento.text.toString().contains(TYPE_BODEGA) ) { preSelected +=1 }
        if(input_tipoEstablecimiento.text.toString().contains(TYPE_NO_BODEGA) ){ preSelected +=2 }
        stringOptionsList.add(TYPE_BODEGA)
        stringOptionsList.add(TYPE_NO_BODEGA)
        val array: Array<CharSequence> =
            stringOptionsList.map { i -> i as CharSequence }.toTypedArray()
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Selecciona una opción")
        var selectedValue: CharSequence = ""
        Log.e("db","preSelected:::> "+ preSelected)
        builder.setSingleChoiceItems(array, preSelected) { _, which ->
            selectedValue = array[which]
            input_tipoEstablecimiento.setText(selectedValue)
            dialog.dismiss()
        }
        dialog = builder.create()
        dialog.show()
    }

    fun validarCombos(): Boolean {
        var derivery: String = ""
        if (inputDelivery.text.toString() == "Si" || inputDelivery.text.toString() == "S") {
            derivery = "S"
        } else derivery = "N"

        if (derivery == "S") {
            return validarAlerts(inputHorarioAtencion, boxHorarioAtencion) && validarAlerts(
                inputDeliveryHorario,
                boxHorario
            )
        } else {
            return validarAlerts(inputHorarioAtencion, boxHorarioAtencion)
        }
    }

    fun validacionCampos(): Boolean {
        return validarNombre() && validarApeMaterno() && validarApePaterno() && validarNumeroIdentidad() && validarNumeroTelefono() && validateDataToRegister() && validarRuc() && validarCombos() && validarHorarios() && validarDeliveryHorarios() && validarChargeForDelivery() && validarPaymentMethods()
    }

    fun validarHorarios(): Boolean {
        if (!(operationIdSelected != 0)) {
            boxHorarioAtencion.error = "Selecciona un horario de atención."
        }
        return operationIdSelected > 0
    }

    private fun validarDeliveryHorarios(): Boolean {
        var ok: Boolean = true
        Log.e("db", "inputDelivery: " + inputDelivery.text.toString())
        Log.e("db", "validarDeliveryHorario Recorriendo deliveryIdSelected: "+deliveryIdSelected.size+ " , datos: " + deliveryIdSelected.toString())
        if (inputDelivery.text.toString() == HAS_DELIVERY || inputDelivery.text.toString() == "S") {
            if ((deliveryIdSelected.size == 0)) {
                boxHorario.error = "Selecciona al menos un horario de delivery."
                ok = false
            } else ok = true
        }
        if (inputDelivery.text.toString() == NOT_HAS_DELIVERY || inputDelivery.text.toString() == "N") {
            ok = true
        }
        return ok
    }

    private fun validarChargeForDelivery(): Boolean {
        var ok: Boolean = true
        if (inputDelivery.text.toString() == HAS_DELIVERY || inputDelivery.text.toString() == "S") {
            if (inputChargeForDeliveryToUpdate.text.toString().isNullOrEmpty()) {
                boxChargeForDelivery.error = "Selecciona si existe o no cargo por delivery."
                ok = false
            } else ok = true
        }
        if (inputDelivery.text.toString() == NOT_HAS_DELIVERY || inputDelivery.text.toString() == "N") {
            ok = true
        }
        return ok
    }

    private fun validarPaymentMethods(): Boolean {
        Log.e("db", "validarPaymentMethods: " + inputDelivery.text.toString())
        Log.e("db", "validarPaymentMethods Recorriendo paymentMethodsIdSelected: "+paymentMethodsIdSelected.size+ " , datos: " + paymentMethodsIdSelected.toString())
        if ((paymentMethodsIdSelected.size == 0)) {
            boxSetPaymentMethodsToUpdate.error = "Selecciona al menos un metodo de pago."
        }
        return paymentMethodsIdSelected.size >0
    }

    private fun validarNombreInt(): Boolean {
        return (inputName.text.toString().isNotEmpty() && inputName.text.toString().length >= 2)
    }

    fun validarNombre(): Boolean {
        if (!validarNombreInt()) boxBusinessName.error = "Digita un nombre de minimo 2 caracteres."
        return validarNombreInt()
    }

    private fun validarApeMaternoInt(): Boolean {
        return (input_ape_materno.text.toString()
            .isNotEmpty() && input_ape_materno.text.toString().length >= 2)
    }

    fun validarApeMaterno(): Boolean {
        if (!validarApeMaternoInt()) box_ape_materno.error =
            "Digita un apellido materno de mínimo 2 caracteres."
        return validarApeMaternoInt()
    }

    private fun validarApePaternoInt(): Boolean {
        return (input_ape_paterno.text.toString()
            .isNotEmpty() && input_ape_paterno.text.toString().length >= 2)
    }

    fun validarApePaterno(): Boolean {
        if (!validarApePaternoInt()) box_ape_paterno.error =
            "Digita un apellido paterno de mínimo 2 caracteres."
        return validarApePaternoInt()
    }

    private fun validarNumeroIdentidadInt(): Boolean {
        return (input_dni.text.toString()
            .isNotEmpty() && input_dni.text.toString().length.equals(8))
    }

    fun validarNumeroIdentidad(): Boolean {
        if (!validarNumeroIdentidadInt()) input_dni.error = "Digita un dni de 8 caracteres."
        return validarNumeroIdentidadInt()
    }

    private fun validarNumeroTelefonoInt(): Boolean {
        return (input_phone.text.toString()
            .isNotEmpty() && input_phone.text.toString().length.equals(9))
    }

    fun validarNumeroTelefono(): Boolean {
        if (!validarNumeroTelefonoInt()) input_phone.error = "Digita un teléfono de 9 caracteres."
        return validarNumeroTelefonoInt()
    }

    private fun validarRucInt(): Boolean {
        return (input_ruc.text.toString()
            .isNotEmpty() && input_ruc.text.toString().length.equals(11))
    }

    fun validarRuc(): Boolean {
        if (!validarRucInt()) input_ruc.error = "Digita un RUC de 11 caracteres."
        return validarRucInt()
    }

    private fun cancelaryRegresar() {
        btnCancelarCambios.setOnClickListener {
            PreferencesManager.getInstance().cleanUpdateEstablishment()
            finish()
        }
    }

    private fun showDeliverySchedule() {
        /* btnAddDeliverySchedule.setOnClickListener {
             showDialogDeliveryList()
         }*/
    }

    private fun showDialogDeliveryList() {
        lateinit var alertDialogDelivery: AlertDialog
        currentSelectedScheduleDelivery.clear()
        val array: Array<CharSequence> =
            stringDeliveryList.map { i -> i as CharSequence }.toTypedArray()
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Selecciona tus horarios de delivery:")
        builder.setMultiChoiceItems(array, selectedDelivery) { alertDialog, which, isChecked ->
            selectedDelivery[which] = isChecked
            val color = array[which]
        }
        builder.setPositiveButton("ACEPTAR") { _, _ ->
            var isEmpty = true
            inputDeliveryHorario.setText("")
            for (i in array.indices) {
                val checked = selectedDelivery[i]
                if (checked) {
                    isEmpty = false
                    Log.e("db", "showDialogDeliveryList seleccionado: "+ array[i])
                    inputDeliveryHorario.setText("${inputDeliveryHorario.text} \n ${array[i]}")
                    getIdOfDelivery(array[i])
                }
            }
            deliveryIdSelected.clear()
            for(key in currentSelectedScheduleDelivery.keys){
                currentSelectedScheduleDelivery.get(key)?.let { deliveryIdSelected.add(it) }
            }
            if (isEmpty) {
                inputDeliveryHorario.setText("")
            }
            alertDialogDelivery.dismiss()
        }
        alertDialogDelivery = builder.create()
        alertDialogDelivery.show()
    }

    override fun onPause() {
        super.onPause()
        //alertDialogDelivery
    }

    private fun getIdOfDelivery(value: CharSequence) {
        deliveryScheduleViewList.forEach {
            if (value == it.range) {
                Log.e("db", "deliveryScheduleViewList value equal: "+ it.range + " : "+ it.scheduleId)
                currentSelectedScheduleDelivery.put(it.scheduleId,it.scheduleId)
                deliveryIdSelected.add(it.scheduleId)
            }
        }
        Log.e("db", "getidofdelivery Recorriendo deliveryIdSelected: "+deliveryIdSelected.size+ " , datos: " + deliveryIdSelected.toString())
    }

    private fun showOperationSchedules() {
        lateinit var dialog: AlertDialog
        val array: Array<CharSequence> =
            stringOperationList.map { i -> i as CharSequence }.toTypedArray()
        val builder = AlertDialog.Builder(this)
        var selectedValue: CharSequence = ""
        var counter=0
        var valueselected=0
        for(i in array){
            if(inputHorarioAtencion.text.toString().contains(i))
                valueselected=counter
            counter++
        }
        builder.setTitle("Selecciona un horario de atención:")
        builder.setSingleChoiceItems(array, valueselected)
        { _, which ->
            selectedValue = array[which]
            inputHorarioAtencion.setText(selectedValue)
            dialog.dismiss()
            for (i in operationScheduleViewList) {
                if (i.range == selectedValue) {
                    operationIdSelected = i.scheduleId
                }
            }
        }
        dialog = builder.create()
        dialog.show()
    }

    override fun onStart() {
        super.onStart()
        getScheduleForOperation()
        getSchedulesForDelivery()
        getMethodsForPayment()
    }

    private fun getScheduleForOperation() {
        getAllSchedulesViewModel.operationSchedule.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    operationScheduleViewList = it.data.toMutableList()
                    btnAddOperationSchedule.isEnabled = true
                    getOperationStringArray()
                    btnAddOperationSchedule.isEnabled = true
                }
                Status.ERROR -> {
                    Toast.makeText(
                        applicationContext,
                        "Ocurrió un error al cargar los horarios de atencion",
                        Toast.LENGTH_LONG
                    ).show()
                }
                Status.LOADING -> {
                }
            }
        })
        getAllSchedulesViewModel.getOperationSchedule("N")
    }

    private fun getSchedulesForDelivery() {
        getAllSchedulesViewModel.deliverySchedule.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    deliveryScheduleViewList = it.data.toMutableList()
                    getDeliveryStringsArray()
                }
                Status.ERROR -> {
                    Toast.makeText(
                        applicationContext,
                        "Ocurrió un error al cargar los horarios",
                        Toast.LENGTH_LONG
                    ).show()
                }
                Status.LOADING -> {
                }
            }
        })
        getAllSchedulesViewModel.getDeliverySchedule("S")
    }

    private fun getDeliveryStringsArray() {
        stringDeliveryList.clear()
        deliveryScheduleViewList.forEach {
            stringDeliveryList.add(it.range)
        }

        selectedDelivery = stringDeliveryList.map { i -> false }.toBooleanArray()

        stringDeliveryList.forEach {
            Log.e("db","stringDeliveryList: "+ it.toString())
        }
        deliveryScheduleViewList.forEach {
            Log.e("db","deliveryScheduleViewList: "+ it.toString())
        }

        selectedDelivery.forEach {
            Log.e("db","selectedDelivery: "+ it.toString())
        }

       // deliveryIdSelected = mutableListOf<Int>()
        var hashMap : HashMap<String, Int> = HashMap<String, Int> ()
        for((f, i) in stringDeliveryList.withIndex()){
            selectedDelivery[f]=false
            if(idScheduleMap.containsValue(i)){
                hashMap.put(i, getIdOfScheduleHavingRange(i))
               // deliveryIdSelected.add(getIdOfScheduleHavingRange(i))
                selectedDelivery[f]=true
            }
        }
        deliveryIdSelected.clear()
        Log.e("db","Limpiando array")
        deliveryIdSelected.forEach {
            Log.e("db","deliveryIdSelected: "+ it.toString())
        }

        for(key in hashMap.keys){
            hashMap.get(key)?.let { deliveryIdSelected.add(it) }
            Log.e("db", "Element at key $key : ${hashMap.get(key)}")
        }
        Log.e("db", "getDeliveryStringArray Recorriendo deliveryIdSelected: "+deliveryIdSelected.size+ " , datos: " + deliveryIdSelected.toString())
    }

    private fun getIdOfScheduleHavingRange(range:String) : Int {
        var value:Int = 0
        deliveryScheduleViewList.forEach {
            if(range.equals(it.range)) value=it.scheduleId
        }
        Log.e("db", "getIdOfScheduleHavingRange id of range "+range +"igual:"  + value)
        return value
    }

    private fun getIdOfPaymentMethodHavingDescription(description:String) : Int {
        var value:Int = 0
        paymentMethodsViewList.forEach {
            if(description.equals(it.description)) value=it.paymentMethodId
        }
        Log.e("db", "getIdOfPaymentMethodHavingRange id of description "+description +"igual:"  + value)
        return value
    }

    private fun getOperationStringArray() {
        stringOperationList.clear()
        operationScheduleViewList.forEach {
            stringOperationList.add(it.range)
        }
    }


    private fun animateLoadingText() {
        if (loadingDots == null) {
            loadingDots =
                LoadingDots(dots)
            loadingDots?.animateLoadingText()
        }
    }

    private fun animateLoadingImage() {
        if (loadingAnimationImageView == null) {
            loadingAnimationImageView =
                LoadingAnimationImageView(
                    ivShrimp
                )
            loadingAnimationImageView?.startAnimation()
        }
    }

    private fun updateHorariosAtencionandDelivery() {
        val operationList = mutableListOf<Int>()
        operationList.add(operationIdSelected)
        val userView = PreferencesManager.getInstance().getEstablishmentSelected()
        var date = Date()
        val formatter = SimpleDateFormat("dd-MM-yyyy")
        currentDate = formatter.format(date)

        if (inputDelivery.text.toString() == "S" || inputDelivery.text.toString() == "S") {
            val updateBody =
                UpdateEstablishmentScheduledDTO(operationList, deliveryIdSelected, currentDate)
            profileViewModel.updateShifts(userView.storeId, updateBody)
        } else {
            val updateBody =
                UpdateEstablishmentScheduledDTO(operationList, mutableListOf<Int>(), currentDate)
            profileViewModel.updateShifts(userView.storeId, updateBody)
        }
        Log.e("db", "updateHorariosatenciondelivery Recorriendo deliveryIdSelected: "+deliveryIdSelected.size+ " , datos: " + deliveryIdSelected.toString())
    }

    private fun getMethodsForPayment() {
        getAllPaymentMethodsViewModel.paymentMethod.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    paymentMethodsViewList = it.data.toMutableList()
                    getPaymentMethodsStringsArray()
                }
                Status.ERROR -> {
                    Toast.makeText(
                        applicationContext,
                        "Ocurrió un error al cargar los métodos de pago",
                        Toast.LENGTH_LONG
                    ).show()
                }
                Status.LOADING -> {
                }
            }
        })
        getAllPaymentMethodsViewModel.getPaymentMethods()
    }

    private fun getPaymentMethodsStringsArray() {
        stringPaymentMethodsList.clear()
        paymentMethodsViewList.forEach {
            stringPaymentMethodsList.add(it.description)
        }

        selectedPaymentMethods = stringPaymentMethodsList.map { i -> false }.toBooleanArray()

        selectedPaymentMethods.forEach {
            Log.e("db","stringPaymentMethodsList: "+ it.toString())
        }

        paymentMethodsViewList.forEach {
            Log.e("db","paymentMethodsViewList: "+ it.toString())
        }

        selectedPaymentMethods.forEach {
            Log.e("db","selectedPaymentMethods: "+ it.toString())
        }
        var hashMap : HashMap<String, Int> = HashMap<String, Int> ()
        for((f, i) in stringPaymentMethodsList.withIndex()) {
            selectedPaymentMethods[f]=false
            if(idPaymentMethodsMap.containsValue(i)){
                hashMap.put(i, getIdOfPaymentMethodHavingDescription(i))
                selectedPaymentMethods[f]=true
            }
        }
        paymentMethodsIdSelected.clear()
        Log.e("db","Limpiando array")
        paymentMethodsIdSelected.forEach {
            Log.e("db","paymentMethodIdSelected: "+ it.toString())
        }

        for(key in hashMap.keys){
            hashMap.get(key)?.let { paymentMethodsIdSelected.add(it) }
            Log.e("db", "Element at key $key : ${hashMap.get(key)}")
        }
        Log.e("db", "getDeliveryStringArray Recorriendo paymentMethodsIdSelected: "+paymentMethodsIdSelected.size+ " , datos: " + paymentMethodsIdSelected.toString())

    }

    private fun showDialogPaymentMethods() {
        lateinit var alertDialog: AlertDialog
        currentSelectedPaymentMethodsDelivery.clear()
        val array: Array<CharSequence> =
            stringPaymentMethodsList.map { i -> i as CharSequence }.toTypedArray()
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Selecciona tus métodos de pago:")
        builder.setMultiChoiceItems(array, selectedPaymentMethods) { alertDialog, which, isChecked ->
            selectedPaymentMethods[which] = isChecked
            val color = array[which]
        }

        builder.setPositiveButton("ACEPTAR") { _, _ ->
            // Do something when click positive button
            var isEmpty = true
            inputPaymentMethodsToUpdate.setText("")
            for (i in array.indices) {
                val checked = selectedPaymentMethods[i]
                if (checked) {
                    isEmpty = false
                    inputPaymentMethodsToUpdate.setText("${inputPaymentMethodsToUpdate.text} \n ${array[i]}")
                    getIdOfPaymentMethods(array[i])
                }
            }
            paymentMethodsIdSelected.clear()
            for(key in currentSelectedPaymentMethodsDelivery.keys) {
                currentSelectedPaymentMethodsDelivery.get(key)?.let { paymentMethodsIdSelected.add(it)}
            }

            if (isEmpty){
                inputPaymentMethodsToUpdate.setText("")
            }
            alertDialog.dismiss()
        }

        alertDialog = builder.create()
        alertDialog.show()
    }

    private fun getIdOfPaymentMethods(value:CharSequence){
        paymentMethodsViewList.forEach {
            if(value == it.description){
                currentSelectedPaymentMethodsDelivery.put(it.paymentMethodId, it.paymentMethodId)
                paymentMethodsIdSelected.add(it.paymentMethodId)
            }
        }
    }

    private fun showDialogSetChargeForDelivery() {
        lateinit var dialog: AlertDialog
        var stringOptionsList = ArrayList<CharSequence>()
        var preSelected=-1;
        if(inputChargeForDeliveryToUpdate.text.toString().contains(HAS_CHARGE_FOR_DELIVERY) ) { preSelected +=1 }
        if(inputChargeForDeliveryToUpdate.text.toString().contains(NOT_HAVE_CHARGE_FOR_DELIVERY) ){ preSelected +=2 }
        stringOptionsList.add(HAS_CHARGE_FOR_DELIVERY)
        stringOptionsList.add(NOT_HAVE_CHARGE_FOR_DELIVERY)
        val array: Array<CharSequence> =
            stringOptionsList.map { i -> i as CharSequence }.toTypedArray()
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Selecciona una opción")
        var selectedValue: CharSequence = ""
        Log.e("db","preSelected:::> "+ preSelected)
        builder.setSingleChoiceItems(array, preSelected) { _, which ->
            selectedValue = array[which]
            inputChargeForDeliveryToUpdate.setText(selectedValue)
            dialog.dismiss()
        }
        dialog = builder.create()
        dialog.show()
    }
}


