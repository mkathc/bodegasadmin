package com.android.bodegasadmin.presentation.products_template.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ProductsView(
    val productTemplateId: Int,
    val code: String,
    val name: String,
    val description: String,
    val unitMeasure: String,
    val imageProduct: String?="",
    val tag:String
) : Parcelable