package com.android.bodegasadmin.presentation

interface Mapper<out V, in D> {

    fun mapToView(type: D): V

}