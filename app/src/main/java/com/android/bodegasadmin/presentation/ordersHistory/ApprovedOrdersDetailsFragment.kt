package com.android.bodegasadmin.presentation.ordersHistory

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.bodegasadmin.R
import com.android.bodegasadmin.data.PreferencesManager
import com.android.bodegasadmin.domain.util.Status
import com.android.bodegasadmin.presentation.ordersHistory.adapter.OrderDetailAdapter
import com.android.bodegasadmin.presentation.ordersHistory.adapter.OrderDetailViewHolder
import com.android.bodegasadmin.presentation.ordersHistory.model.EstablishmentOrderView
import com.android.bodegasadmin.presentation.utils.DateTimeHelper
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_approved_orders_details.*
import kotlinx.android.synthetic.main.fragment_approved_orders_details.boxDelivery
import kotlinx.android.synthetic.main.fragment_approved_orders_details.boxStatus
import kotlinx.android.synthetic.main.fragment_approved_orders_details.btnAtender
import kotlinx.android.synthetic.main.fragment_approved_orders_details.btnCancel
import kotlinx.android.synthetic.main.fragment_approved_orders_details.imSend
import kotlinx.android.synthetic.main.fragment_approved_orders_details.rvCustomerOrdersDetail
import kotlinx.android.synthetic.main.fragment_approved_orders_details.titleDeliveryCharge
import kotlinx.android.synthetic.main.fragment_approved_orders_details.titleSendDate
import kotlinx.android.synthetic.main.fragment_approved_orders_details.tvAtender
import kotlinx.android.synthetic.main.fragment_approved_orders_details.tvClientName
import kotlinx.android.synthetic.main.fragment_approved_orders_details.tvDeliveryCharge
import kotlinx.android.synthetic.main.fragment_approved_orders_details.tvDirection
import kotlinx.android.synthetic.main.fragment_approved_orders_details.tvFechaUpdate
import kotlinx.android.synthetic.main.fragment_approved_orders_details.tvOrderDate
import kotlinx.android.synthetic.main.fragment_approved_orders_details.tvPM
import kotlinx.android.synthetic.main.fragment_approved_orders_details.tvPhone
import kotlinx.android.synthetic.main.fragment_approved_orders_details.tvSendDate
import kotlinx.android.synthetic.main.fragment_approved_orders_details.tvTitlePayment
import kotlinx.android.synthetic.main.fragment_approved_orders_details.tvTotalOrder
import kotlinx.android.synthetic.main.fragment_delivered_orders_details.*
import kotlinx.android.synthetic.main.fragment_informed_orders.*
import kotlinx.android.synthetic.main.fragment_informed_orders_details.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import java.math.BigDecimal

private val ORDER = "orderDetails"

class ApprovedOrdersDetailsFragment : Fragment(), OrderDetailViewHolder.ViewHolderListener {

    private lateinit var order: EstablishmentOrderView
    private var adapter = OrderDetailAdapter("A", this)
    private val approvedOrderViewModel: ApprovedOrdersViewModel by sharedViewModel()
    private val establishmentOrdersViewModel: DeliveredOrdersViewModel by sharedViewModel()
    private val DELIVERY_CHARGE_EMPTY=0.0
    private val REQUEST_CODE_STORAGE = 200
    private var listener: OnFragmentInteractionListener? = null 

    companion object {
        const val TAG = "ApprovedOrdersDetailsFragment"

        @JvmStatic
        fun newInstance(establishmentOrderView: EstablishmentOrderView) =
            ApprovedOrdersDetailsFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(ORDER, establishmentOrderView)
                }
            }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            order = it.getParcelable(ORDER)!!
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_approved_orders_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setView()
        setOnClickListener()
        observerUpdateOrder()
        //  observerApprovedOrders()
    }

    private fun setOnClickListener() {
        btnAtender.setOnClickListener {
            if (validateCheckProducts()) {
                if (order.isDelivery) {
                    if (order.status == "E") {
                        approvedOrderViewModel.updateStateOrder(
                            DELIVERY_CHARGE_EMPTY,
                            order.orderId.toInt(),
                            "T",
                            adapter.getList()
                        )
                    } else {
                        approvedOrderViewModel.updateStateOrder(
                            DELIVERY_CHARGE_EMPTY,
                            order.orderId.toInt(),
                            "E",
                            adapter.getList()
                        )
                    }
                } else {
                    approvedOrderViewModel.updateStateOrder(
                        DELIVERY_CHARGE_EMPTY,
                        order.orderId.toInt(),
                        "T",
                        adapter.getList()
                    )
                }
            } else {
                Snackbar.make(
                    approvedDetailView,
                    "No ha seleccionado ningún producto como atendido",
                    Snackbar.LENGTH_LONG
                ).show()
            }
        }

        btnCancel.setOnClickListener {
            approvedOrderViewModel.updateStateOrder(DELIVERY_CHARGE_EMPTY,order.orderId.toInt(), "R", adapter.getList())
        }
    }

    private fun validateCheckProducts(): Boolean {
        val list = adapter.getList()
        var isChecked = false
        for (ordersItemView in list) {
            if (ordersItemView.isAttended) {
                isChecked = true
            }
        }
        return isChecked
    }

    private fun setButtonPhoneIconToCall() {
        imPhoneApproved.setOnClickListener {
            if (validateCallPermissionsStorage()) {
                makeCallWhenClickOn()
            } else {
                requestCallPermissions()
            }
        }
    }

    private fun validateCallPermissionsStorage(): Boolean {
        return ActivityCompat.checkSelfPermission(
            activity!!.applicationContext,
            android.Manifest.permission.CALL_PHONE
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun requestCallPermissions() {
        val contextProvider =
            ActivityCompat.shouldShowRequestPermissionRationale(
                activity!!,
                android.Manifest.permission.CALL_PHONE
            )

        if (contextProvider) {
            Toast.makeText(
                activity!!.applicationContext,
                "Los permisos son requeridos para llamar al cliente",
                Toast.LENGTH_SHORT
            ).show()
        }
        permissionRequest()
    }

    private fun permissionRequest() {
        ActivityCompat.requestPermissions(
            activity!!,
            arrayOf(android.Manifest.permission.CALL_PHONE),
            REQUEST_CODE_STORAGE
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_CODE_STORAGE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    makeCallWhenClickOn()
                } else {
                    Toast.makeText(
                        activity!!.applicationContext,
                        "No aceptó los permisos",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }

    }

    private fun makeCallWhenClickOn() {
        val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:"+order.customerPhoneNumber))
        startActivity(intent)
    }

    private fun setView() {
        tvDirection.text = order.customerAddress + "\n" + order.customerUrbanization
        tvClientName.text = order.customerCompleteName
        tvPhone.text = order.customerPhoneNumber
        setButtonPhoneIconToCall()
        tvOrderDate.text = DateTimeHelper.parseDateOrder(order.creationDate)
        tvFechaUpdate.text = DateTimeHelper.parseDateOrder(order.updateDate)
        tvTotalOrder.text = String.format("%s %s", "s/", order.total)
        tvPM.text = order.paymentMethodDescription
        var aux:BigDecimal = order.customerAmount.toBigDecimal() - order.total.toBigDecimal()
        var aux2:BigDecimal = order.customerAmount.toBigDecimal() - order.total.toBigDecimal()

        aux = String.format("%.2f", aux).toBigDecimal()
        var inputTotalVuelto1 =String.format("%s %s", "s/ ", aux.toString())

        if(aux2.compareTo(BigDecimal.ZERO) <0) {
            inputTotalVuelto1 = inputTotalVuelto1 + " El vuelto negativo se origina por el 'Cargo por envío'"
        }

        /*var inputTotalVuelto1="El vuelvo negativo se origina por el 'Cargo por envío'"
        if(aux.compareTo(BigDecimal.ZERO)>1) {
            aux = String.format("%.2f", aux).toBigDecimal()
            inputTotalVuelto1 =String.format("%s %s", "s/ ", aux.toString())
        }*/

       // var inputTotalVuelto1 =String.format("%s %s", "s/ ",  aux.toString())
        /*if(tvPM.text.toString().contains("Efectivo", true)) {
            inputTotalVuelto1.visibility = View.VISIBLE
            titleTotalVuelo1.visibility = View.VISIBLE
        }*/

        if(order.isDelivery){
            tvSendDate.visibility = View.VISIBLE
            titleSendDate.visibility = View.VISIBLE
            imSend.visibility = View.VISIBLE
            tvSendDate.text = order.shippingDateFrom
        }else{
            tvSendDate.visibility = View.GONE
            titleSendDate.visibility = View.GONE
            imSend.visibility = View.GONE
        }

        if (order.status == "A") {
            if (order.isDelivery) {
                tvAtender.text = "Enviar"
            } else {
                tvAtender.text = "Entregar"
            }
        } else {
            if (order.isDelivery) {
                tvAtender.text = "Entregar"
            }
        }

        if (order.isDelivery){
            titleDeliveryCharge.visibility = View.VISIBLE
            tvDeliveryCharge.visibility = View.VISIBLE
            if (order.deliveryCharge != "0.00"){
                tvDeliveryCharge.text = String.format("%s %s", "s/", order.deliveryCharge )
            }
        }else{
            titleDeliveryCharge.visibility = View.GONE
            tvDeliveryCharge.visibility = View.GONE
        }


        boxDelivery.text = order.deliveryType
        boxDelivery.isEnabled = false
        boxStatus.text = String.format("%s %s", "Pedido", order.stringStatus)

        if (order.paymentMethodDescription == "Efectivo"){
            tvTitlePayment.visibility = View.VISIBLE
            var paga1 = String.format("%s %s %s", "Paga con:"," s/", order.customerAmount)
            tvTitlePayment.text = paga1 + " Vuelto: " + inputTotalVuelto1

        }else{
            tvTitlePayment.visibility = View.GONE
            tvTitlePayment.text = ""
        }
        setRecyclerView()

    }

    private fun setRecyclerView() {
        val linearLayoutManager = LinearLayoutManager(context)
        rvCustomerOrdersDetail.layoutManager = linearLayoutManager
        rvCustomerOrdersDetail.adapter = adapter
        adapter.setStoreList(order.orderDetails)
    }

    private fun observerUpdateOrder() {
        approvedOrderViewModel.updateOrder.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    if (it.data) {
                        if (order.status == "A") {
                            approvedOrderViewModel.getOrdersListByEstablishmentAndState(
                                PreferencesManager.getInstance().getEstablishmentSelected().storeId,
                                "A"
                            )
                        } else {
                            approvedOrderViewModel.getOrdersListByEstablishmentAndState(
                                PreferencesManager.getInstance().getEstablishmentSelected().storeId,
                                "E"
                            )
                        }

                        establishmentOrdersViewModel.getOrdersListByEstablishmentAndState(
                            PreferencesManager.getInstance().getEstablishmentSelected().storeId,
                            "T"
                        )

                        approvedOrderViewModel.cleanUpdateState()
                        listener!!.hideLoading()
                        listener!!.onBackPressed()
                    } else {
                        listener!!.hideLoading()
                        it.message?.let { msg ->
                            Snackbar.make(
                                approvedDetailView,
                                msg,
                                Snackbar.LENGTH_LONG
                            ).show()
                        }
                    }

                }
                Status.ERROR -> {
                    listener!!.hideLoading()
                    Snackbar.make(
                        approvedDetailView,
                        "Ocurrió un error al actualizar su pedido, intente nuevamente por favor",
                        Snackbar.LENGTH_LONG
                    ).show()
                }

                Status.LOADING -> {
                    if (it.data) {
                        listener!!.showLoading()
                    } else {
                        listener!!.hideLoading()
                    }
                }
            }
        })
    }

    override fun onClick(position: Int) {

    }

    interface OnFragmentInteractionListener {
        fun onBackPressed()
        fun showLoading()
        fun hideLoading()
    }
}