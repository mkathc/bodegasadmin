package com.android.bodegasadmin.presentation.searchOrders

import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Button
import android.widget.DatePicker
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.bodegasadmin.R
import com.android.bodegasadmin.data.PreferencesManager
import com.android.bodegasadmin.domain.util.DateTimeHelper
import com.android.bodegasadmin.domain.util.Status
import com.android.bodegasadmin.presentation.ordersHistory.model.EstablishmentOrderView
import com.android.bodegasadmin.presentation.searchOrders.adapter.SearchEstablishmentOrderAdapter
import com.android.bodegasadmin.presentation.searchOrders.adapter.SearchEstablishmentOrderViewHolder
import com.android.bodegasadmin.presentation.searchOrders.model.SearchEstablishmentOrderView
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.fragment_search_orders_list.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*


class SearchOrdersFragment : Fragment(),
    SearchEstablishmentOrderViewHolder.ViewHolderListener {

    private val VALOR_VACIO: String = ""
    private var listener: OnFragmentInteractionListener? = null
    private val searchCustomerOrdersViewModel: SearchOrdersViewModel by viewModel()
    private var searchCustomerOrdersyViewList: MutableList<SearchEstablishmentOrderView> =
        mutableListOf()
    private var adapter = SearchEstablishmentOrderAdapter(this)
    private var fechaInicioValorSeleccionado: String = ""
    private var fechaFinValorSeleccionado: String = ""
    private var dni = String()
    private var name = String()

    companion object {
        const val TAG = "SearchOrdersFragment"

        @JvmStatic
        fun newInstance() =
            SearchOrdersFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_search_orders_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        openModal.setOnClickListener {
            getModalFechas()
        }
        observerSearchOrders()

    }

    private fun getModalFechas() {
        val dialog = Dialog(context!!)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.layout_dialog_dates)
        dialog.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        val inputClientDNI = dialog.findViewById(R.id.inputClientDNI) as TextInputEditText
        val inputClientName = dialog.findViewById(R.id.inputClientName) as TextInputEditText
        val btnSetUpStartDate = dialog.findViewById(R.id.btnStartDate) as Button
        val btnSetUpEndDate = dialog.findViewById(R.id.btnEndDate) as Button
        val btnSearch = dialog.findViewById(R.id.btnSearch) as Button
        val btnCLeanFilters = dialog.findViewById(R.id.botonBorrarFiltros) as TextView
        val boxStartDate = dialog.findViewById(R.id.boxStartDate) as TextInputLayout
        val boxEndDate = dialog.findViewById(R.id.boxEndDate) as TextInputLayout
        val etStartDate = dialog.findViewById(R.id.etStartDate) as TextInputEditText
        val etEndDate = dialog.findViewById(R.id.etEndDate) as TextInputEditText


        btnSearch.setOnClickListener {
            if (ExistenParametrosDeBusqueda(
                    inputClientDNI,
                    inputClientName,
                    etStartDate,
                    etEndDate
                )
            ) {
                dni = inputClientDNI.text.toString()
                name = inputClientName.text.toString()
                searchCustomerOrdersViewModel.getOrdersListByDniOrNameWithDates(
                    PreferencesManager.getInstance().getEstablishmentSelected().storeId,
                    inputClientDNI.text.toString(),
                    inputClientName.text.toString(),
                    fechaInicioValorSeleccionado,//fechaInicioValor.text.toString(),
                    fechaFinValorSeleccionado, //fechaFinValor.text.toString(),
                    VALOR_VACIO,
                    VALOR_VACIO,
                    VALOR_VACIO,
                    "-creationDate"
                )
                dialog.dismiss()
            } else {
                Snackbar.make(
                    searchOrdersView,
                    "Completa algún campo de búsqueda",
                    Snackbar.LENGTH_LONG
                ).show()
            }
        }


        btnCLeanFilters.setOnClickListener {
            inputClientName.setText("")
            inputClientDNI.setText("")
            etStartDate.setText("")
            etEndDate.setText("")
            btnSetUpStartDate.isEnabled = true
            btnSetUpEndDate.isEnabled = false
        }

        btnSetUpStartDate.setOnClickListener {
            val cal = Calendar.getInstance()
            val y = cal.get(Calendar.YEAR)
            val m = cal.get(Calendar.MONTH)
            val d = cal.get(Calendar.DAY_OF_MONTH)
            val datepickerdialog = DatePickerDialog(
                context!!,
                DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                    val date = year.toString() + "-" + (monthOfYear + 1) + "-" + dayOfMonth
                    val parseDate = DateTimeHelper.parseDate(date)
                    fechaInicioValorSeleccionado = parseDate
                    btnSetUpEndDate.isEnabled = true
                    etStartDate.setText(parseDate)
                },
                y,
                m,
                d
            )
            //datepickerdialog.datePicker.minDate =
            datepickerdialog.show()
        }

        btnSetUpEndDate.setOnClickListener {
            val cal = Calendar.getInstance()
            val y = cal.get(Calendar.YEAR)
            val m = cal.get(Calendar.MONTH)
            val d = cal.get(Calendar.DAY_OF_MONTH)
            val datepickerdialog = DatePickerDialog(
                context!!,
                DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                    val date = year.toString() + "-" + (monthOfYear + 1) + "-" + dayOfMonth
                    val parseDate = DateTimeHelper.parseDate(date)
                    etEndDate.setText(parseDate)
                    fechaFinValorSeleccionado = parseDate
                },
                y,
                m,
                d
            )
            datepickerdialog.show()
        }

        dialog.show()
    }

    private fun ExistenParametrosDeBusqueda(
        inputClientDNI: TextInputEditText,
        inputClientName: TextInputEditText,
        etStartDate: TextInputEditText,
        etEndDate: TextInputEditText
    ): Boolean {
        return inputClientDNI.text.toString().isNotEmpty() || inputClientName.text.toString()
            .isNotEmpty() || etStartDate.text.toString().isNotEmpty() || etEndDate.text.toString()
            .isNotEmpty()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnListFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }


    private fun observerSearchOrders() {
        searchCustomerOrdersViewModel.establishmentCustomerOrders.observe(
            viewLifecycleOwner,
            Observer {
                when (it.status) {
                    Status.SUCCESS -> {
                        listener?.hideLoading()
                        searchCustomerOrdersyViewList = it.data.toMutableList()
                        if (searchCustomerOrdersyViewList.isEmpty()) {
                            rvListaSearch.visibility = View.GONE;
                            containerEmptyState.visibility = View.VISIBLE
                            tvTitleEmptyPools.setText("No se encontraron pedidos")
                        } else {
                            rvListaSearch.visibility = View.VISIBLE;
                            containerEmptyState.visibility = View.GONE
                            setRecyclerView()
                        }
                    }
                    Status.ERROR -> {
                        listener?.hideLoading()
                        Snackbar.make(
                            searchOrdersView,
                            getString(R.string.generic_error),
                            Snackbar.LENGTH_LONG
                        ).show()
                    }
                    Status.LOADING -> {
                        listener?.showLoading()
                    }
                }
            })

    }

    private fun setRecyclerView() {
        val linearLayoutManager = LinearLayoutManager(context)
        rvListaSearch.layoutManager = linearLayoutManager
        rvListaSearch.adapter = adapter
        adapter.setStoreList(searchCustomerOrdersyViewList)
    }

    fun refreshList(){
        if(dni.isNotEmpty() || name.isNotEmpty() || fechaFinValorSeleccionado.isNotEmpty() || fechaInicioValorSeleccionado.isNotEmpty()){
            searchCustomerOrdersViewModel.getOrdersListByDniOrNameWithDates(
                PreferencesManager.getInstance().getEstablishmentSelected().storeId,
                dni,
                name,
                fechaInicioValorSeleccionado,//fechaInicioValor.text.toString(),
                fechaFinValorSeleccionado, //fechaFinValor.text.toString(),
                VALOR_VACIO,
                VALOR_VACIO,
                VALOR_VACIO,
                "-creationDate"
            )
        }
    }

    interface OnFragmentInteractionListener {
        fun replaceByInformedOrdersDetailsFragment(
            establishmentOrderView: EstablishmentOrderView,
            isFromOrders: Boolean
        )

        fun replaceByApprovedOrdersDetailsFragment(
            establishmentOrderView: EstablishmentOrderView,
            isFromOrders: Boolean
        )

        fun replaceByDeliveredOrdersDetailsFragment(
            establishmentOrderView: EstablishmentOrderView,
            isFromOrders: Boolean
        )

        fun replaceByCanceledDetailsOrderFragment(
            establishmentOrderView: EstablishmentOrderView,
            isFromOrders: Boolean
        )

        fun showLoading()
        fun hideLoading()
    }

    override fun onClick(position: Int) {
        val searchEstablishmentOrderView = searchCustomerOrdersyViewList[position]
        val establishmentOrderView = EstablishmentOrderView(
            searchEstablishmentOrderView.creationDate,
            searchEstablishmentOrderView.orderId,
            searchEstablishmentOrderView.deliveryType,
            searchEstablishmentOrderView.isDelivery,
            searchEstablishmentOrderView.shippingDateFrom,
            searchEstablishmentOrderView.status,
            searchEstablishmentOrderView.stringStatus,
            searchEstablishmentOrderView.establishmentEmail,
            searchEstablishmentOrderView.establishmentName,
            searchEstablishmentOrderView.total,
            searchEstablishmentOrderView.customerPhoneNumber,
            searchEstablishmentOrderView.customerCompleteName,
            searchEstablishmentOrderView.customerAddress,
            searchEstablishmentOrderView.customerLatitude,
            searchEstablishmentOrderView.customerLongitude,
            searchEstablishmentOrderView.orderDetails,
            searchEstablishmentOrderView.updateDate,
            searchEstablishmentOrderView.paymentMethodCustomerMessage,
            searchEstablishmentOrderView.paymentMethodDescription,
            searchEstablishmentOrderView.paymentMethodEstablishmentMessage,
            searchEstablishmentOrderView.paymentMethodPaymentMethodId,
            searchEstablishmentOrderView.paymentMethodRequestAmount,
            searchEstablishmentOrderView.customerAmount,
            searchEstablishmentOrderView.deliveryCharge,
            searchEstablishmentOrderView.customerUrbanization
        )
        when (establishmentOrderView.status) {
            "I" -> listener!!.replaceByInformedOrdersDetailsFragment(
                establishmentOrderView,
                false
            )
            "A", "E" -> listener!!.replaceByApprovedOrdersDetailsFragment(
                establishmentOrderView, false
            )
            "T" -> listener!!.replaceByDeliveredOrdersDetailsFragment(
                establishmentOrderView,
                false
            )
            "R", "N" -> listener!!.replaceByCanceledDetailsOrderFragment(
                establishmentOrderView, false
            )
            else -> ""
        }
    }
}
