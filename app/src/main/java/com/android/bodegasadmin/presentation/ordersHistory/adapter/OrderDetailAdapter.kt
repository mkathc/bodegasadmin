package com.android.bodegasadmin.presentation.ordersHistory.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.android.bodegasadmin.R
import com.android.bodegasadmin.presentation.ordersHistory.model.OrdersItemView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_profile.*

class OrderDetailAdapter(
    private val state: String,
    private val viewHolderListener: OrderDetailViewHolder.ViewHolderListener
) :
    RecyclerView.Adapter<OrderDetailViewHolder>() {

    private val detailedOrdersList = mutableListOf<OrdersItemView>()
    private lateinit var context: Context
    private var stateOrder: String = ""

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderDetailViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_customer_order_detail, parent, false)
        context = parent.context
        stateOrder = state
        return OrderDetailViewHolder(view, viewHolderListener)
    }

    override fun getItemCount(): Int {
        return detailedOrdersList.size
    }

    override fun onBindViewHolder(holder: OrderDetailViewHolder, position: Int) {
        val orderItem = detailedOrdersList[position]
        holder.tvProductName.text = orderItem.productTemplateName
        holder.tvUnitPrice.text = String.format("%s %s %s", "Precio unitario", "s/", orderItem.orderDetailPrice)
        holder.tvSubTotalProduct.text = String.format("%s %s", "s/", orderItem.orderDetailSubTotal)
        holder.checkBox.isChecked = orderItem.isAttended
        holder.tvIsAttended.text = orderItem.orderDetailStatus

        if(orderItem.orderDetailStatus == "Atendido"){
            holder.tvIsAttended.background = ContextCompat.getDrawable(context, R.drawable.button_rounded_green_8_background)
        }else{
            holder.tvIsAttended.background = ContextCompat.getDrawable(context, R.drawable.button_rounded_red_8_background)
        }

        holder.description.text = String.format(
            "%s %s %s %s",
            orderItem.productTemplateDescription,
            "x",
            orderItem.orderDetailQuantity,
            orderItem.productTemplateUnitMeasure
        )

        if (stateOrder != "I") {
            holder.checkBox.visibility = View.GONE
            holder.tvIsAttended.visibility = View.VISIBLE
        } else {
            holder.checkBox.visibility = View.VISIBLE
            holder.tvIsAttended.visibility = View.GONE
        }

        holder.checkBox.setOnCheckedChangeListener { buttonView, isChecked ->
            orderItem.isAttended = isChecked
        }

        if (orderItem.productTemplatePathImage != "") {
            val circularProgressDrawable = CircularProgressDrawable(context)
            circularProgressDrawable.strokeWidth = 5f
            circularProgressDrawable.centerRadius = 30f
            circularProgressDrawable.start()

            Glide.with(context)  // this
                .load(orderItem.productTemplatePathImage)
                .placeholder(circularProgressDrawable)
                .error(context.getDrawable(R.drawable.ic_image_black_24dp))
                .into(holder.imageProduct)
            holder.imageProduct.setPadding(0, 0, 0, 0)
        }

    }

    fun setStoreList(poolList: List<OrdersItemView>) {
        this.detailedOrdersList.clear()
        this.detailedOrdersList.addAll(poolList)
    }

    fun getList(): List<OrdersItemView> {
        return this.detailedOrdersList
    }
}