package com.android.bodegasadmin.data.db.establishment.dao

object EstablishmentConstants {

    const val TABLE_NAME = "establishment"

    const val QUERY_USER = "SELECT * FROM" + " " + TABLE_NAME

    const val DELETE_USER = "DELETE FROM" + " " + TABLE_NAME
}