package com.android.bodegasadmin.data.repository.references.departments.mapper

import com.android.bodegasadmin.data.repository.Mapper
import com.android.bodegasadmin.data.repository.references.departments.model.DepartmentEntity
import com.android.bodegasadmin.domain.references.department.Department

class DepartmentMapper: Mapper<DepartmentEntity, Department> {
    override fun mapFromEntity(type: DepartmentEntity): Department {
        return Department(type.id,type.name)
    }

    override fun mapToEntity(type: Department): DepartmentEntity {
        return DepartmentEntity(type.id,type.name)
    }
}
