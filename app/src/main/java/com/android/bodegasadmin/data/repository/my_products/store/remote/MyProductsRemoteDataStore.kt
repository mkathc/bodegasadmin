package com.android.bodegasadmin.data.repository.my_products.store.remote


import com.android.bodegasadmin.data.repository.my_products.model.MyProductsEntity
import com.android.bodegasadmin.data.repository.my_products.store.MyProductsDataStore
import com.android.bodegasadmin.domain.util.Resource


class MyProductsRemoteDataStore constructor(private val productsRemote: MyProductsRemote) :
    MyProductsDataStore {

    override suspend fun getMyProducts(searchText: String): Resource<List<MyProductsEntity>> {
        return productsRemote.getMyProducts(searchText)
    }

    override suspend fun updateMyProduct(
        storeProductId: Int,
        price: Double,
        stock: String
    ): Resource<Boolean> {
        return  productsRemote.updateMyProduct(storeProductId, price, stock)
    }

}