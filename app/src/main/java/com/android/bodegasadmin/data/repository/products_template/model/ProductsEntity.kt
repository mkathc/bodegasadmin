package com.android.bodegasadmin.data.repository.products_template.model

data class ProductsEntity(
    val productTemplateId: Int,
    val code: String,
    val name: String,
    val description: String,
    val unitMeasure: String,
    val imageProduct: String ? = "",
    val tag:String
)
