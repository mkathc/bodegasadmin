package com.android.bodegasadmin.data.repository.establishment.model

data class ScheduleEntity(
    val range: String,
    val startTime: String ? = "",
    val endTime: String? = "",
    val shippingScheduleId:Int
)