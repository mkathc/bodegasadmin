package com.android.bodegasadmin.data.repository.distance.store.remote

import com.android.bodegasadmin.data.repository.allschedules.model.AllScheduleEntity
import com.android.bodegasadmin.data.repository.allschedules.store.AllSchedulesDataStore
import com.android.bodegasadmin.data.repository.allschedules.store.remote.AllSchedulesRemote
import com.android.bodegasadmin.data.repository.distance.model.DistanceEntity
import com.android.bodegasadmin.data.repository.distance.store.DistanceDataStore
import com.android.bodegasadmin.domain.util.Resource

class DistanceRemoteDataStore constructor(
    private val distanceRemote: DistanceRemote
) : DistanceDataStore {

    override suspend fun getDistance(eLat: Double, eLong: Double, cLat :Double, cLong: Double): Resource<DistanceEntity> {
        return  distanceRemote.getDistance(eLat, eLong, cLat, cLong)
    }


}