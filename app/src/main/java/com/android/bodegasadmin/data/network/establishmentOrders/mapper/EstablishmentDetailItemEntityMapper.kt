package com.android.bodegasadmin.data.network.establishmentOrders.mapper

import com.android.bodegasadmin.data.network.establishmentOrders.model.OrderDetailItem
import com.android.bodegasadmin.data.network.Mapper
import com.android.bodegasadmin.data.repository.establishmentOrders.model.OrderDetailItemEntity

class EstablishmentDetailItemEntityMapper : Mapper<OrderDetailItem, OrderDetailItemEntity> {

    override fun mapFromRemote(type: OrderDetailItem): OrderDetailItemEntity {
        return OrderDetailItemEntity(
            type.orderDetailId.toString(),
            type.unitMeasure,
            type.quantity.toString(),
            type.price,
            type.status,
            type.observation,
            type.subTotal,
            type.storeProduct.storeProductId.toString(),
            type.storeProduct.price.toString(),
            type.storeProduct.status,
            type.storeProduct.productTemplate.productTemplateId.toString(),
            type.storeProduct.productTemplate.code,
            type.storeProduct.productTemplate.name,
            type.storeProduct.productTemplate.description,
            type.storeProduct.productTemplate.unitMeasure,
            type.storeProduct.productTemplate.status,
            type.storeProduct.productTemplate.pathImage
        )
    }
}