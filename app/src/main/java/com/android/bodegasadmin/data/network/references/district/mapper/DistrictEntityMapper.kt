package com.android.bodegasadmin.data.network.references.district.mapper

import com.android.bodegasadmin.data.network.Mapper
import com.android.bodegasadmin.data.network.references.district.model.DistrictResponse
import com.android.bodegasadmin.data.repository.references.districts.model.DistrictEntity

class DistrictEntityMapper : Mapper<DistrictResponse, DistrictEntity> {
    override fun mapFromRemote(type: DistrictResponse): DistrictEntity {
        return DistrictEntity(
            type.id,
            type.name,
            type.province_id,
            type.department_id
        )
    }
}