package com.android.bodegasadmin.data.network.establishment.mapper

import com.android.bodegasadmin.data.network.Mapper
import com.android.bodegasadmin.data.network.establishment.model.ScheduleResponse
import com.android.bodegasadmin.data.repository.establishment.model.ScheduleEntity

class ScheduleEntityMapper : Mapper<ScheduleResponse, ScheduleEntity> {

    override fun mapFromRemote(type: ScheduleResponse): ScheduleEntity {
        return ScheduleEntity(
            type.range,
            type.startTime,
            type.endTime,
            type.shippingScheduleId
        )
    }

}