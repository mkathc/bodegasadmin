package com.android.bodegasadmin.data.repository.establishment

import com.android.bodegasadmin.data.repository.establishment.mapper.EstablishmentLoginMapper
import com.android.bodegasadmin.data.repository.establishment.mapper.EstablishmentMapper
import com.android.bodegasadmin.data.repository.establishment.mapper.RegisterEstablishmentMapper
import com.android.bodegasadmin.data.repository.establishment.model.EstablishmentEntity
import com.android.bodegasadmin.data.repository.establishment.store.db.EstablishmentDbDataStore
import com.android.bodegasadmin.data.repository.establishment.store.remote.EstablishmentRemoteDataStore
import com.android.bodegasadmin.domain.establishment.Establishment
import com.android.bodegasadmin.domain.establishment.EstablishmentLoginToken
import com.android.bodegasadmin.domain.establishment.EstablishmentRepository
import com.android.bodegasadmin.domain.establishment.register.RegisterEstablishmentBody
import com.android.bodegasadmin.domain.util.Resource
import com.android.bodegasadmin.presentation.login.ForgetPassDTO
import com.android.bodegasadmin.presentation.profile.ChangePasswordDtoBody
import com.android.bodegasadmin.presentation.profile.UpdateEstablishmentBody
import com.android.bodegasadmin.presentation.profile.UpdateEstablishmentScheduledDTO
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.coroutineScope
import kotlin.coroutines.CoroutineContext

class EstablishmentDataRepository constructor(
    private val establishmentDbDataStore: EstablishmentDbDataStore,
    private val establishmentRemoteDataStore: EstablishmentRemoteDataStore,
    private val establishmentMapper: EstablishmentMapper,
    private val establishmentLoginMapper: EstablishmentLoginMapper,
    private val registerEstablishmentMapper: RegisterEstablishmentMapper
) : EstablishmentRepository, CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.IO + job
    override suspend fun loginEstablishmentToken(
        email: String,
        password: String
    ): Resource<EstablishmentLoginToken> {
        return coroutineScope {
            val loginEstablishmentResponse =
                establishmentRemoteDataStore.loginEstablishmentToken(email, password)
            val establishment = establishmentLoginMapper.mapFromEntity(loginEstablishmentResponse.data)
           // establishmentDbDataStore.saveEstablishment(establishmentMapper.mapToEntity(establishment))
            return@coroutineScope Resource(
                loginEstablishmentResponse.status,
                establishment,
                loginEstablishmentResponse.message
            )
        }
    }

    override suspend fun loginEstablishment(
        establishmentId: Int
    ): Resource<Establishment> {
        return coroutineScope {
            val loginEstablishmentResponse =
                establishmentRemoteDataStore.loginEstablishment(establishmentId)
            val establishment = establishmentMapper.mapFromEntity(loginEstablishmentResponse.data)
            establishmentDbDataStore.saveEstablishment(establishmentMapper.mapToEntity(establishment))
            return@coroutineScope Resource(
                loginEstablishmentResponse.status,
                establishment,
                loginEstablishmentResponse.message
            )
        }
    }

    override suspend fun cleanEstablishment() {
        return coroutineScope {
            establishmentDbDataStore.clearEstablishment()
        }
    }

    override suspend fun registerEstablishment(registerEstablishmentBody: RegisterEstablishmentBody): Resource<EstablishmentLoginToken> {
        return coroutineScope {
            val registerEstablishmentResponse = establishmentRemoteDataStore.registerEstablishment(
                registerEstablishmentMapper.mapToEntity(registerEstablishmentBody)
            )
            val establishment = establishmentLoginMapper.mapFromEntity(registerEstablishmentResponse.data)
            return@coroutineScope Resource(
                registerEstablishmentResponse.status,
                establishment,
                registerEstablishmentResponse.message
            )
        }
    }

    override suspend fun getEstablishment(): Resource<Establishment> {
        return coroutineScope {
            val establishmentEntity = establishmentDbDataStore.getEstablishment()
            return@coroutineScope Resource(
                establishmentEntity.status,
                establishmentMapper.mapFromEntity(establishmentEntity.data),
                establishmentEntity.message
            )
        }
    }

    override suspend fun updatePhotoUser(customerId: Int, filePath: String): Resource<Boolean> {
        return coroutineScope {
            val updateUserResponse = establishmentRemoteDataStore.updatePhotoUser(customerId, filePath)
            return@coroutineScope Resource(
                updateUserResponse.status,
                updateUserResponse.data,
                updateUserResponse.message
            )
        }

    }

    override suspend fun updateEstablishment(customerId: Int, body: UpdateEstablishmentBody): Resource<Establishment> {
        return coroutineScope {
            val updateUserResponse = establishmentRemoteDataStore.updateEstablishment(customerId, body)
            val establishment = establishmentMapper.mapFromEntity(updateUserResponse.data)
            return@coroutineScope Resource(
                updateUserResponse.status,
                establishment,
                updateUserResponse.message
            )
        }

    }

    override suspend fun updatePassword(body: ChangePasswordDtoBody): Resource<Boolean> {
        return coroutineScope {
            val updateUserResponse = establishmentRemoteDataStore.updatePassword(body)
            return@coroutineScope Resource(
                updateUserResponse.status,
                updateUserResponse.data,
                updateUserResponse.message
            )
        }
    }

    override suspend fun recoveryPassword(body: ForgetPassDTO): Resource<Boolean> {
        return coroutineScope {
            val updateUserResponse = establishmentRemoteDataStore.recoveryPassword(body)
            return@coroutineScope Resource(
                updateUserResponse.status,
                updateUserResponse.data,
                updateUserResponse.message
            )
        }
    }

    override suspend fun updateSchedule(establishmentId:Int, body: UpdateEstablishmentScheduledDTO): Resource<Boolean> {
        return coroutineScope {
            val updateUserResponse = establishmentRemoteDataStore.updateSchedule(establishmentId,body)
            return@coroutineScope Resource(
                updateUserResponse.status,
                updateUserResponse.data,
                updateUserResponse.message
            )
        }
    }

    /* override suspend fun registerUser(
         registerUserBody: RegisterUserBody
     ): Resource<User> {
         return coroutineScope {
             val createUserResponse = userRemoteDataStore.registerUser(
                registerUserMapper.mapToEntity(registerUserBody)
             )
             val user = userMapper.mapFromEntity(createUserResponse.data)
             return@coroutineScope Resource(
                 createUserResponse.status,
                 user,
                 createUserResponse.message
             )
         }
     }*/

}