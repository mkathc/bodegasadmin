package com.android.bodegasadmin.data.network.distance.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class DistanceResponse(
    @SerializedName("distanceInKms")
    @Expose
    val distanceInKms: String,
    @SerializedName("numberOfBlocks")
    @Expose
    val numberOfBlocks: Int,
    @SerializedName("travelTimeByCar")
    @Expose
    val travelTimeByCar: String,
    @SerializedName("travelTimeByBike")
    @Expose
    val travelTimeByBike: String,
    @SerializedName("travelTimeByWalking")
    @Expose
    val travelTimeByWalking: String
)