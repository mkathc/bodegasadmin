package com.android.bodegasadmin.data.repository.products_template.store

import com.android.bodegasadmin.data.repository.products_template.mapper.ProductsMapper
import com.android.bodegasadmin.data.repository.products_template.model.ProductsEntity
import com.android.bodegasadmin.data.repository.products_template.store.remote.ProductsRemoteDataStore
import com.android.bodegasadmin.domain.products_template.Products
import com.android.bodegasadmin.domain.products_template.ProductsRepository
import com.android.bodegasadmin.domain.util.Resource
import com.android.bodegasadmin.domain.util.Status
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.coroutineScope
import kotlin.coroutines.CoroutineContext

class ProductsDataRepository(
    private val productsRemoteDataStore: ProductsRemoteDataStore,
    private val productsMapper: ProductsMapper
) : ProductsRepository, CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.IO + job

    override suspend fun getProducts(text:String): Resource<List<Products>> {
        return coroutineScope {
            val productsList =
                productsRemoteDataStore.getProducts(text)

            var resource: Resource<List<Products>> =
                Resource(productsList.status, mutableListOf(), productsList.message)

            if (productsList.status == Status.SUCCESS) {
                val mutableListFarms = mapSupplierList(productsList.data)
                resource = Resource(Status.SUCCESS, mutableListFarms, productsList.message)
            } else {
                if (productsList.status == Status.ERROR) {
                    resource = Resource(productsList.status, mutableListOf(), productsList.message)
                }
            }

            return@coroutineScope resource
        }
    }

    override suspend fun addProducts(productId: Int, productPrice: String): Resource<Boolean> {
        return coroutineScope {
            val addResponse =
                productsRemoteDataStore.addProducts(productId, productPrice)
            return@coroutineScope  Resource(addResponse.status, addResponse.data, addResponse.message)
        }
    }

    private fun mapSupplierList(productsEntityList: List<ProductsEntity>): List<Products> {
        val productsList = mutableListOf<Products>()
        productsEntityList.forEach {
            productsList.add(productsMapper.mapFromEntity(it))
        }
        return productsList
    }
}