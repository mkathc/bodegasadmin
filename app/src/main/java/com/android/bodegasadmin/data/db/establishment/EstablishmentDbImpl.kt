package com.android.bodegasadmin.data.db.establishment

import com.android.bodegasadmin.data.db.AppDatabase
import com.android.bodegasadmin.data.db.establishment.mapper.EstablishmentDbMapper
import com.android.bodegasadmin.data.repository.establishment.model.EstablishmentEntity
import com.android.bodegasadmin.data.repository.establishment.store.db.EstablishmentDb
import com.android.bodegasadmin.domain.util.Resource
import com.android.bodegasadmin.domain.util.Status
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.coroutineScope
import kotlin.coroutines.CoroutineContext

class EstablishmentDbImpl constructor(
    private val database: AppDatabase,
    private val establishmentDbMapper: EstablishmentDbMapper
) : EstablishmentDb, CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.IO + job

    override suspend fun getEstablishment(): Resource<EstablishmentEntity> {
        return coroutineScope {
            val establishmentDb = database.establishmentDao().getEstablishment()
            val establishmentEntity = establishmentDbMapper.mapFromDb(establishmentDb)
            return@coroutineScope Resource(
                Status.SUCCESS,
                establishmentEntity,
                ""
            )
        }
    }

    override suspend fun saveEstablishment(establishmentEntity: EstablishmentEntity) {
        coroutineScope {
            database.establishmentDao()
                .insertEstablishment(establishmentDbMapper.mapToDb(establishmentEntity))
        }
    }

    override suspend fun clearAllTables() {
        return coroutineScope {
            database.establishmentDao().clearEstablishment()
        }
    }

}