package com.android.bodegasadmin.data.network.products_template.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class ProductsResponse(

    @SerializedName("productTemplateId")
    @Expose
    val productTemplateId: Int,
    @SerializedName("code")
    @Expose
    val code: String,
    @SerializedName("description")
    @Expose
    val description: String,
    @SerializedName("name")
    @Expose
    val name: String,
    @SerializedName("unitMeasure")
    @Expose
    val unitMeasure: String,
    @SerializedName("pathImage")
    @Expose
    val pathImage: String ? = "",
    @SerializedName("tag")
    @Expose
    val tag: String
)