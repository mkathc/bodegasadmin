package com.android.bodegasadmin.data.network.establishmentOrders

import com.android.bodegasadmin.data.network.ResponseBodySuccess
import com.android.bodegasadmin.data.network.establishmentOrders.model.EstablishmentListOrdersBody
import com.android.bodegasadmin.data.network.establishmentOrders.model.OrderBodyToUpdate
import com.android.bodegasadmin.data.network.establishmentOrders.model.OrderBodyToUpdateWithDeliveryCharge
import retrofit2.Response
import retrofit2.http.*

interface EstablishmentOrdersService {

    @GET ("/establishments/{establishment-id}/orders")
    suspend fun getOrdersListByEstablishmentAndState(
        @Path("establishment-id") establishmentid: Int?,
        @Query("sortBy") sortBy: String,
        @Query("order-state") orderState: String?
    ): Response<EstablishmentListOrdersBody>

    @PUT("/establishments/{establishment-id}/order/{order-id}")
    suspend fun updateStateOrder(
        @Path("establishment-id") customerId: Int,
        @Path("order-id") orderId: Int,
        @Body orderBodyToUpdate: OrderBodyToUpdate
    ): Response<ResponseBodySuccess>

    @PUT("/establishments/{establishment-id}/order/{order-id}")
    suspend fun updateStateOrderWithDeliveryCharge(
        @Path("establishment-id") customerId: Int,
        @Path("order-id") orderId: Int,
        @Body orderBodyToUpdate: OrderBodyToUpdateWithDeliveryCharge
    ): Response<ResponseBodySuccess>

}
