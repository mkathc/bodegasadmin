package com.android.bodegasadmin.data.repository.searchOrders.store

import com.android.bodegasadmin.data.repository.searchOrders.model.SearchOrdersEntity
import com.android.bodegasadmin.domain.util.Resource

interface SearchOrdersDataStore {
    suspend fun getOrdersListByDni(
        establishmentId: Int,
        clientDni: String,
        clientName: String,
        orderStartDate: String,
        orderEndDate: String,
        orderState: String,
        page: String,
        size: String,
        sortBy: String
    ): Resource<List<SearchOrdersEntity>>
}