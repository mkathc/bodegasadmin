package com.android.bodegasadmin.data.repository.establishment.model

data class EstablishmentLoginEntity(
    val id_entity: String,
    val token: String
)