package com.android.bodegasadmin.data.repository.establishment.store.remote

import com.android.bodegasadmin.data.repository.establishment.model.EstablishmentEntity
import com.android.bodegasadmin.data.repository.establishment.model.EstablishmentLoginEntity
import com.android.bodegasadmin.data.repository.establishment.model.RegisterEstablishmentBodyEntity
import com.android.bodegasadmin.data.repository.establishment.store.EstablishmentDataStore
import com.android.bodegasadmin.domain.util.Resource
import com.android.bodegasadmin.presentation.login.ForgetPassDTO
import com.android.bodegasadmin.presentation.profile.ChangePasswordDtoBody
import com.android.bodegasadmin.presentation.profile.UpdateEstablishmentBody
import com.android.bodegasadmin.presentation.profile.UpdateEstablishmentScheduledDTO


class EstablishmentRemoteDataStore constructor(private val establishmentRemote: EstablishmentRemote) :
    EstablishmentDataStore {
    override suspend fun loginEstablishmentToken(
        email: String,
        password: String
    ): Resource<EstablishmentLoginEntity> {
        return establishmentRemote.loginEstablishmentToken(email, password)
    }

    override suspend fun loginEstablishment(
        establishmentId: Int
    ): Resource<EstablishmentEntity> {
        return establishmentRemote.loginEstablishment(establishmentId)
    }

    override suspend fun saveEstablishment(establishmentEntity: EstablishmentEntity) {
        throw UnsupportedOperationException()
    }

    override suspend fun clearEstablishment() {
        throw UnsupportedOperationException()
    }

    override suspend fun getEstablishment(): Resource<EstablishmentEntity> {
        throw UnsupportedOperationException()
    }

    override suspend fun registerEstablishment(registerEstablishmentBodyEntity: RegisterEstablishmentBodyEntity): Resource<EstablishmentLoginEntity> {
        return establishmentRemote.registerEstablishment(registerEstablishmentBodyEntity)
    }

    override suspend fun updatePhotoUser(customerId: Int, filePath: String): Resource<Boolean> {
        return establishmentRemote.updatePhotoUser(customerId, filePath)
    }

    override suspend fun updateEstablishment(customerId: Int, body: UpdateEstablishmentBody): Resource<EstablishmentEntity> {
        return establishmentRemote.updateEstablishment(customerId,body)
    }

    override suspend fun updatePassword( body: ChangePasswordDtoBody): Resource<Boolean> {
        return establishmentRemote.updatePassword(body)
    }

    override suspend fun recoveryPassword( body: ForgetPassDTO): Resource<Boolean> {
        return establishmentRemote.recoveryPassword(body)
    }

    override suspend fun updateSchedule(establishmentId:Int,  body: UpdateEstablishmentScheduledDTO): Resource<Boolean> {
        return establishmentRemote.updateSchedule(establishmentId, body)
    }
}