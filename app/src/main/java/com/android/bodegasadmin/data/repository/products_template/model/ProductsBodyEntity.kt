package com.android.bodegasadmin.data.repository.products_template.model

data class ProductsBodyEntity(
    val productTemplateId: Int,
    val price: String
)
