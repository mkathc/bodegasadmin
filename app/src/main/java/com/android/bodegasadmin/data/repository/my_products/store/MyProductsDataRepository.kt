package com.android.bodegasadmin.data.repository.my_products.store

import com.android.bodegasadmin.data.repository.my_products.mapper.MyProductsMapper
import com.android.bodegasadmin.data.repository.my_products.model.MyProductsEntity
import com.android.bodegasadmin.data.repository.my_products.store.remote.MyProductsRemoteDataStore
import com.android.bodegasadmin.domain.my_products.MyProducts
import com.android.bodegasadmin.domain.my_products.MyProductsRepository
import com.android.bodegasadmin.domain.util.Resource
import com.android.bodegasadmin.domain.util.Status
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.coroutineScope
import kotlin.coroutines.CoroutineContext

class MyProductsDataRepository(
    private val productsRemoteDataStore: MyProductsRemoteDataStore,
    private val productsMapper: MyProductsMapper
) : MyProductsRepository, CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.IO + job

    override suspend fun getMyProducts(searchText: String): Resource<List<MyProducts>> {
        return coroutineScope {
            val productsList =
                productsRemoteDataStore.getMyProducts(searchText)

            var resource: Resource<List<MyProducts>> =
                Resource(productsList.status, mutableListOf(), productsList.message)

            if (productsList.status == Status.SUCCESS) {
                val mutableListFarms = mapSupplierList(productsList.data)
                resource = Resource(Status.SUCCESS, mutableListFarms, productsList.message)
            } else {
                if (productsList.status == Status.ERROR) {
                    resource = Resource(productsList.status, mutableListOf(), productsList.message)
                }
            }

            return@coroutineScope resource
        }
    }

    override suspend fun updateMyProduct(
        storeProductId: Int,
        price: Double,
        stock: String
    ): Resource<Boolean> {
        return coroutineScope {
            val response = productsRemoteDataStore.updateMyProduct(storeProductId, price, stock)
            return@coroutineScope Resource(response.status, response.data, response.message)
        }
    }

    private fun mapSupplierList(productsEntityList: List<MyProductsEntity>): List<MyProducts> {
        val productsList = mutableListOf<MyProducts>()
        productsEntityList.forEach {
            productsList.add(productsMapper.mapFromEntity(it))
        }
        return productsList
    }
}