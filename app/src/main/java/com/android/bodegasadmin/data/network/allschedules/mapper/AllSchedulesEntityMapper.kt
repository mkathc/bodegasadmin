package com.android.bodegasadmin.data.network.allschedules.mapper

import com.android.bodegasadmin.data.network.Mapper
import com.android.bodegasadmin.data.network.allschedules.model.AllSchedulesResponse
import com.android.bodegasadmin.data.network.establishment.model.ScheduleResponse
import com.android.bodegasadmin.data.repository.allschedules.model.AllScheduleEntity
import com.android.bodegasadmin.data.repository.establishment.model.ScheduleEntity

class AllSchedulesEntityMapper : Mapper<AllSchedulesResponse, AllScheduleEntity> {

    override fun mapFromRemote(type: AllSchedulesResponse): AllScheduleEntity {
        return AllScheduleEntity(
            type.shippingScheduleId,
            type.range,
            type.type,
            type.startTime,
            type.endTime
        )
    }

}