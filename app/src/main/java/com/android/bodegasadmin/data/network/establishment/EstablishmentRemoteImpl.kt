package com.android.bodegasadmin.data.network.establishment

import com.android.bodegasadmin.data.network.ResponseBodyError
import com.android.bodegasadmin.data.network.establishment.mapper.EstablishmentEntityMapper
import com.android.bodegasadmin.data.network.establishment.mapper.EstablishmentLoginEntityMapper
import com.android.bodegasadmin.data.network.establishment.mapper.RegisterEstablishmentEntityMapper
import com.android.bodegasadmin.data.network.establishment.model.UserBody
import com.android.bodegasadmin.data.repository.establishment.model.EstablishmentEntity
import com.android.bodegasadmin.data.repository.establishment.model.EstablishmentLoginEntity
import com.android.bodegasadmin.data.repository.establishment.model.RegisterEstablishmentBodyEntity
import com.android.bodegasadmin.data.repository.establishment.store.remote.EstablishmentRemote
import com.android.bodegasadmin.domain.util.Resource
import com.android.bodegasadmin.domain.util.Status
import com.android.bodegasadmin.presentation.login.ForgetPassDTO
import com.android.bodegasadmin.presentation.profile.ChangePasswordDtoBody
import com.android.bodegasadmin.presentation.profile.UpdateEstablishmentBody
import com.android.bodegasadmin.presentation.profile.UpdateEstablishmentScheduledDTO
import com.google.gson.GsonBuilder
import kotlinx.coroutines.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import java.io.IOException
import java.net.UnknownHostException
import kotlin.coroutines.CoroutineContext


class EstablishmentRemoteImpl constructor(
    private val establishmentServices: EstablishmentServices,
    private val establishmentEntityMapper: EstablishmentEntityMapper,
    private val establishmentLoginEntityMapper: EstablishmentLoginEntityMapper,
    private val registerEstablishmentEntityMapper: RegisterEstablishmentEntityMapper
) : EstablishmentRemote, CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.IO + job

    override suspend fun loginEstablishmentToken(
        email: String,
        password: String
    ): Resource<EstablishmentLoginEntity> {
        return coroutineScope {
            val userBodySend = UserBody(email, password)
            try {
                val response = establishmentServices.loginEstablishmentToken(userBodySend)
                if (response.isSuccessful) {
                    if(response.body()!!.success){
                        val establishmentResponse = response.body()!!.data!!.login
                        val establishmentEntity =
                            establishmentLoginEntityMapper.mapFromRemote(establishmentResponse)
                        return@coroutineScope Resource(
                            Status.SUCCESS,
                            establishmentEntity,
                            response.message()
                        )
                    }else{
                        val emptyEstablishment = EstablishmentLoginEntity(
                            "",
                            ""
                        )
                        val message = response.body()!!.message
                        return@coroutineScope Resource(
                            Status.ERROR,
                            emptyEstablishment,
                            message
                        )
                    }
                }
                else {
                    val emptyEstablishment = EstablishmentLoginEntity(
                        "",
                        ""
                    )
                    return@coroutineScope Resource(
                        Status.ERROR,
                        emptyEstablishment,
                        response.message()
                    )
                }
            } catch
                (e: UnknownHostException) {
                val emptyEstablishment = EstablishmentLoginEntity(
                    "",
                    ""
                )
                return@coroutineScope Resource(
                    Status.ERROR,
                    emptyEstablishment,
                    "500"
                )
            }
            catch (e: Throwable) {
                val emptyEstablishment =
                    EstablishmentLoginEntity(
                        "",
                        ""
                    )
                return@coroutineScope Resource(
                    Status.ERROR,
                    emptyEstablishment,
                    e.message
                )
            }
        }
    }

    override suspend fun loginEstablishment(
        establishmentId: Int
    ): Resource<EstablishmentEntity> {
        return coroutineScope {
            //val userBodySend = UserBody(email, password)
            try {
                val response = establishmentServices.loginUser(establishmentId)
                if (response.isSuccessful) {
                    if(response.body()!!.success){
                        val establishmentResponse = response.body()!!.establishmentItem!!.establishment
                        val establishmentEntity =
                            establishmentEntityMapper.mapFromRemote(establishmentResponse)
                        return@coroutineScope Resource(
                            Status.SUCCESS,
                            establishmentEntity,
                            response.message()
                        )
                    }else{
                        val emptyEstablishment = EstablishmentEntity(
                            0,
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            0.0,
                            0.0,
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            mutableListOf(),
                            mutableListOf(),
                            "",
                            false,
                            mutableListOf()
                        )
                        val message = response.body()!!.message
                        return@coroutineScope Resource(
                            Status.ERROR,
                            emptyEstablishment,
                            message
                        )
                    }
                }
                else {
                    val emptyEstablishment = EstablishmentEntity(
                        0,
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        0.0,
                        0.0,
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        mutableListOf(),
                        mutableListOf(),
                        "",
                        false,
                        mutableListOf()
                    )
                    return@coroutineScope Resource(
                        Status.ERROR,
                        emptyEstablishment,
                        response.message()
                    )
                }
            } catch
                (e: UnknownHostException) {
                val emptyEstablishment = EstablishmentEntity(
                    0,
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    0.0,
                    0.0,
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    mutableListOf(),
                    mutableListOf(),
                    "",
                    false,
                    mutableListOf()
                )
                return@coroutineScope Resource(
                    Status.ERROR,
                    emptyEstablishment,
                    "500"
                )
            }
            catch (e: Throwable) {
                val emptyEstablishment =
                    EstablishmentEntity(
                        0,
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        0.0,
                        0.0,
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        mutableListOf(),
                        mutableListOf(),
                        "",
                        false,
                        mutableListOf()
                    )
                return@coroutineScope Resource(
                    Status.ERROR,
                    emptyEstablishment,
                    e.message
                )
            }
        }
    }

    override suspend fun registerEstablishment(registerEstablishmentBodyEntity: RegisterEstablishmentBodyEntity): Resource<EstablishmentLoginEntity> {
        return coroutineScope {
            var emptyEstablishment = EstablishmentLoginEntity( "", "")
            var createUserBody = registerEstablishmentEntityMapper.mapFromRemote(registerEstablishmentBodyEntity)
            if(createUserBody.delivery == "") createUserBody.delivery= null
            try {
                val response = establishmentServices.registerEstablishment(createUserBody)
                if (response.isSuccessful) {
                    if(response.body()!!.success){
                        val body = response.body()!!.data!!.login
                        val createUserEntity = establishmentLoginEntityMapper.mapFromRemote(body)
                        return@coroutineScope Resource(
                            Status.SUCCESS,
                            createUserEntity,
                            response.message() )
                    }else{
                        return@coroutineScope Resource(
                            Status.SUCCESS,
                            emptyEstablishment,
                            response.body()!!.message)
                    }
                }else {
                    val gson = GsonBuilder().create()
                    var mError = ResponseBodyError()
                    try {
                        mError = gson.fromJson(
                            response.errorBody()!!.string(),
                            ResponseBodyError::class.java
                        )

                        val error = mError.message + ": " + mError.fields[0].fieldName + " - " + mError.fields[0].message
                        return@coroutineScope Resource(
                            Status.ERROR,
                            emptyEstablishment,
                            error
                        )

                    } catch (e: IOException) {
                        return@coroutineScope Resource(
                            Status.ERROR,
                            emptyEstablishment,
                            "Ha ocurrido un error, intente nuevamente por favor"
                        )
                    }
                }
            } catch (e: UnknownHostException) {
                return@coroutineScope Resource(
                    Status.ERROR,
                    emptyEstablishment,
                    "Ha ocurrido un error, intente nuevamente por favor"
                )
            } catch (e: Throwable) {
                return@coroutineScope Resource(
                    Status.ERROR,
                    emptyEstablishment,
                    "Ha ocurrido un error, intente nuevamente por favor"
                )
            }
        }
    }

    override suspend fun updatePhotoUser(customerId: Int, filePath: String): Resource<Boolean> {
        return coroutineScope {
            val body = MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart("file", filePath, RequestBody.create(
                    MediaType.parse("application/octet-stream"),
                    File(filePath)
                ))
                .addFormDataPart("table", "Establishment").build() as RequestBody
            try {
                val response = establishmentServices.updatePhotoUser(customerId, body)
                if(response.isSuccessful){
                    if(response.body()!!.success){
                        val body = response.body()!!.fileResponse!!.file.fileDownloadUri
                        return@coroutineScope Resource(
                            Status.SUCCESS,
                            true,
                            body
                        )
                    }
                    else{
                        return@coroutineScope Resource(
                            Status.ERROR,
                            false,
                            "Ha ocurrido un error al subir su imagen, intente nuevamente por favor"
                        )
                    }
                }else{
                    return@coroutineScope Resource(
                        Status.ERROR,
                        false,
                        "Ha ocurrido un error al subir su imagen, intente nuevamente por favor"
                    )
                }
            }catch (e:IOException){
                return@coroutineScope Resource(
                    Status.ERROR,
                    false,
                    "Ha ocurrido un error, intente nuevamente por favor"
                )
            }
        }
    }

    override suspend fun updateEstablishment(customerId: Int, body_: UpdateEstablishmentBody): Resource<EstablishmentEntity> {
        return coroutineScope {
            var emptyEstablishment = EstablishmentEntity(0, "", "", "", "",
                "", "", "", "", "", "", 0.0, 0.0,
                "", "", "", "", "", "", "", "", mutableListOf(), mutableListOf(), "", false,
                mutableListOf()
            )
            try {
                val response = establishmentServices.updateEstablishment(customerId, body_)
                if(response.isSuccessful){
                    if(response.body()!!.success){
                        val establishmentResponse = response.body()!!.establishmentItem!!.establishment
                        val establishmentEntity = establishmentEntityMapper.mapFromRemote(establishmentResponse)
                        return@coroutineScope Resource(
                            Status.SUCCESS,
                            establishmentEntity,
                            response.message()
                        )
                    }
                    else{
                        return@coroutineScope Resource(
                            Status.ERROR,
                            emptyEstablishment,
                            "Ha ocurrido un error al actualizar los datos, intente nuevamente por favor"
                        )
                    }
                }else{
                    return@coroutineScope Resource(
                        Status.ERROR,
                        emptyEstablishment,
                        "Ha ocurrido un error al actualizar los datos, intente nuevamente por favor"
                    )
                }
            }catch (e:IOException){
                return@coroutineScope Resource(
                    Status.ERROR,
                    emptyEstablishment,
                    "Ha ocurrido un error, intente nuevamente por favor"
                )
            }
        }
    }

    override suspend fun updatePassword(body: ChangePasswordDtoBody): Resource<Boolean> {
        return coroutineScope {
            try {
                val response = establishmentServices.updatePassword(body)
                if(response.isSuccessful){
                    if(response.body()!!.success){
                        val body = response.body()!!
                        return@coroutineScope Resource(
                            Status.SUCCESS,
                            true,
                            body.message
                        )
                    }
                    else{
                        return@coroutineScope Resource(
                            Status.ERROR,
                            false,
                            "Ha ocurrido un error al actualizar la contraseña, intente nuevamente por favor"
                        )
                    }
                }else{
                    return@coroutineScope Resource(
                        Status.ERROR,
                        false,
                        "Ha ocurrido un error al actualizar la contraseña, intente nuevamente por favor"
                    )
                }
            }catch (e:IOException){
                return@coroutineScope Resource(
                    Status.ERROR,
                    false,
                    "Ha ocurrido un error, intente nuevamente por favor"
                )
            }
        }
    }

    override suspend fun recoveryPassword(body: ForgetPassDTO): Resource<Boolean> {
        return coroutineScope {
            try {
                val response = establishmentServices.recoveryPassword(body)
                if(response.isSuccessful){
                    if(response.body()!!.success){
                        val body = response.body()!!
                        return@coroutineScope Resource(
                            Status.SUCCESS,
                            true,
                            body.message
                        )
                    }
                    else{
                        return@coroutineScope Resource(
                            Status.ERROR,
                            false,
                            "Ha ocurrido un error al enviar mail, intente nuevamente por favor"
                        )
                    }
                }else{
                    return@coroutineScope Resource(
                        Status.ERROR,
                        false,
                        "Ha ocurrido un error al enviar mail, intente nuevamente por favor"
                    )
                }
            }catch (e:IOException){
                return@coroutineScope Resource(
                    Status.ERROR,
                    false,
                    "Ha ocurrido un error, intente nuevamente por favor"
                )
            }
        }
    }

    override suspend fun updateSchedule(establishmentId:Int, body: UpdateEstablishmentScheduledDTO): Resource<Boolean> {
        return coroutineScope {
            try {
                val response = establishmentServices.updateScheduled(establishmentId, body)
                if(response.isSuccessful){
                    if(response.body()!!.success){
                        val body = response.body()!!
                        return@coroutineScope Resource(
                            Status.SUCCESS,
                            true,
                            body.message
                        )
                    }
                    else{
                        return@coroutineScope Resource(
                            Status.ERROR,
                            false,
                            "Ha ocurrido un error al actualizar el schedule, intente nuevamente por favor"
                        )
                    }
                }else{
                    return@coroutineScope Resource(
                        Status.ERROR,
                        false,
                        "Ha ocurrido un error al actualizar el schedule, intente nuevamente por favor"
                    )
                }
            }catch (e:IOException){
                return@coroutineScope Resource(
                    Status.ERROR,
                    false,
                    "Ha ocurrido un error, intente nuevamente por favor"
                )
            }
        }
    }
}