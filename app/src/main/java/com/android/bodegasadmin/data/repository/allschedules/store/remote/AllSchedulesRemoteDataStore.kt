package com.android.bodegasadmin.data.repository.allschedules.store.remote

import com.android.bodegasadmin.data.repository.allschedules.model.AllScheduleEntity
import com.android.bodegasadmin.data.repository.allschedules.store.AllSchedulesDataStore
import com.android.bodegasadmin.data.repository.establishment.model.EstablishmentEntity
import com.android.bodegasadmin.data.repository.establishment.model.RegisterEstablishmentBodyEntity
import com.android.bodegasadmin.data.repository.establishment.store.EstablishmentDataStore
import com.android.bodegasadmin.domain.util.Resource


class AllSchedulesRemoteDataStore constructor(
    private val allSchedulesRemote: AllSchedulesRemote
) : AllSchedulesDataStore {

    override suspend fun getAllSchedules(type: String): Resource<List<AllScheduleEntity>> {
        return  allSchedulesRemote.getAllSchedules(type)
    }


}