package com.android.bodegasadmin.data.network.my_products.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class MyProductsDetailsResponse(
    @SerializedName("code")
    @Expose
    val code: String,
    @SerializedName("description")
    @Expose
    val description: String,
    @SerializedName("name")
    @Expose
    val name: String,
    @SerializedName("unitMeasure")
    @Expose
    val unitMeasure: String,
    @SerializedName("pathImage")
    @Expose
    val pathImage: String ? = ""
)