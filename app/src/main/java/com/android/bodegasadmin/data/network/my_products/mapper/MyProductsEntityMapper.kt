package com.android.bodegasadmin.data.network.my_products.mapper

import com.android.bodegasadmin.data.network.Mapper
import com.android.bodegasadmin.data.network.my_products.model.MyProductsResponse
import com.android.bodegasadmin.data.repository.my_products.model.MyProductsEntity

class MyProductsEntityMapper : Mapper<MyProductsResponse, MyProductsEntity> {

    override fun mapFromRemote(type: MyProductsResponse): MyProductsEntity {
        return MyProductsEntity(
            type.storeProductId,
            type.price,
            type.status,
            type.productDetails.code,
            type.productDetails.name,
            type.productDetails.description,
            type.productDetails.unitMeasure,
            type.stock,
            type.productDetails.pathImage
        )
    }

}