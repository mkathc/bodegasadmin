package com.android.bodegasadmin.data.repository.references.departments.model

class DepartmentEntity (
    val id: String,
    val name: String
)