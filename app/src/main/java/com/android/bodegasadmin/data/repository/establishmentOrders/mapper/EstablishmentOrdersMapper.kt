package com.android.bodegasadmin.data.repository.establishmentOrders.mapper

import com.android.bodegasadmin.data.repository.establishmentOrders.model.EstablishmentOrdersEntity
import com.android.bodegasadmin.data.repository.Mapper
import com.android.bodegasadmin.data.repository.establishmentOrders.model.OrderDetailItemEntity
import com.android.bodegasadmin.domain.establishmentOrders.EstablishmentOrders
import com.android.bodegasadmin.domain.establishmentOrders.OrderDetails


class EstablishmentOrdersMapper (
    private val establishmentItemMapper: EstablishmentDetailItemMapper
): Mapper<EstablishmentOrdersEntity, EstablishmentOrders> {
    override fun mapFromEntity(type: EstablishmentOrdersEntity): EstablishmentOrders {
        return EstablishmentOrders(
            type.creationDate,
            type.orderId,
            type.deliveryType,
            type.shippingDateFrom,
            type.shippingDateUntil,
            type.status,
            type.establishmentId,
            type.establishmentEmail,
            type.establishmentName,
            type.establishmentPhoneNumber,
            type.total,
            type.customerPhoneNumber,
            type.customerName,
            type.customerLastNamePaternal,
            type.customerLastNameMaternal,
            type.customerAddress,
            type.customerLatitude,
            type.customerLongitude,
            getOrdersDetail(type.orderDetails),
            type.updateDate,
            type.paymentMethodCustomerMessage,
            type.paymentMethodDescription,
            type.paymentMethodEstablishmentMessage,
            type.paymentMethodPaymentMethodId,
            type.paymentMethodRequestAmount,
            type.customerAmount,
            type.deliveryCharge,
            type.customerUrbanization
        )
    }

    override fun mapToEntity(type: EstablishmentOrders): EstablishmentOrdersEntity {
        return EstablishmentOrdersEntity(
            type.creationDate,
            type.orderId,
            type.deliveryType,
            type.shippingDateFrom,
            type.shippingDateUntil,
            type.status,
            type.establishmentId,
            type.establishmentEmail,
            type.establishmentName,
            type.establishmentPhoneNumber,
            type.total,
            type.customerPhoneNumber,
            type.customerName,
            type.customerLastNamePaternal,
            type.customerLastNameMaternal,
            type.customerAddress,
            type.customerLatitude,
            type.customerLongitude,
            getOrdersDetailFromUsesCase(type.orderDetails),
            type.updateDate,
            type.paymentMethodCustomerMessage,
            type.paymentMethodDescription,
            type.paymentMethodEstablishmentMessage,
            type.paymentMethodPaymentMethodId,
            type.paymentMethodRequestAmount,
            type.customerAmount,
            type.deliveryCharge,
            type.customerUrbanization
        )
    }

    private fun getOrdersDetail(list: List<OrderDetailItemEntity>): List<OrderDetails>{
        val listSchedule = mutableListOf<OrderDetails>()
        list.forEach {
            listSchedule.add(establishmentItemMapper.mapFromEntity(it))
        }
        return listSchedule
    }

    private fun getOrdersDetailFromUsesCase(list: List<OrderDetails>): List<OrderDetailItemEntity>{
        val listSchedule = mutableListOf<OrderDetailItemEntity>()
        list.forEach {
            listSchedule.add(establishmentItemMapper.mapToEntity(it))
        }
        return listSchedule
    }
}
