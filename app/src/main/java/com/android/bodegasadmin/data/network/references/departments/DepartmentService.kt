package com.android.bodegasadmin.data.network.references.departments

import com.android.bodegasadmin.data.network.references.departments.model.DepartmentListBody
import retrofit2.Response
import retrofit2.http.GET

interface DepartmentService {
        @GET("/departments")
        suspend fun getDeparments() : Response<DepartmentListBody>
    }