package com.android.bodegasadmin.data.network.products_template.mapper

import com.android.bodegasadmin.data.network.Mapper
import com.android.bodegasadmin.data.network.products_template.model.ProductsResponse
import com.android.bodegasadmin.data.repository.products_template.model.ProductsEntity

class ProductsEntityMapper : Mapper<ProductsResponse, ProductsEntity> {

    override fun mapFromRemote(type: ProductsResponse): ProductsEntity {
        return ProductsEntity(
            type.productTemplateId,
            type.code,
            type.name,
            type.description,
            type.unitMeasure,
            type.pathImage,
            type.tag
        )
    }

}