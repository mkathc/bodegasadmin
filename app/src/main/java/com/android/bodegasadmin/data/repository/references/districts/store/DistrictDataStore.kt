package com.android.bodegasadmin.data.repository.references.districts.store

import com.android.bodegasadmin.data.repository.references.districts.model.DistrictEntity
import com.android.bodegasadmin.domain.util.Resource

interface DistrictDataStore {
    suspend fun getDistricts(
        id: String,
        name: String,
        province_id: String,
        department_id: String
    ): Resource<List<DistrictEntity>>
}