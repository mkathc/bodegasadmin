package com.android.bodegasadmin.data.network.my_products

import android.content.Context
import android.icu.lang.UProperty
import com.android.bodegasadmin.data.PreferencesManager
import com.android.bodegasadmin.data.network.my_products.mapper.MyProductsEntityMapper
import com.android.bodegasadmin.data.network.my_products.model.MyProductsItem
import com.android.bodegasadmin.data.network.my_products.model.UpdateProductBody
import com.android.bodegasadmin.data.repository.my_products.model.MyProductsEntity
import com.android.bodegasadmin.data.repository.my_products.store.remote.MyProductsRemote
import com.android.bodegasadmin.domain.util.Resource
import com.android.bodegasadmin.domain.util.Status
import com.google.gson.Gson
import kotlinx.coroutines.*
import java.io.InputStream
import java.io.InputStreamReader
import java.net.UnknownHostException
import java.nio.charset.StandardCharsets
import kotlin.coroutines.CoroutineContext


class MyProductsRemoteImpl constructor(
    private val productsServices: MyProductsServices,
    private val productsEntityMapper: MyProductsEntityMapper,
    private val context: Context
) : MyProductsRemote, CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.IO + job

    override suspend fun getMyProducts(searchText: String): Resource<List<MyProductsEntity>> {
        return coroutineScope {
            try {
                val result = productsServices.getMyProducts(
                    PreferencesManager.getInstance().getEstablishmentSelected().storeId,
                    searchText,
                    "productTemplate.description",
                    0,
                    1000
                )

                if (result.isSuccessful) {
                    if(result.code() == 200){
                        result.body()?.let {
                            val productsEntityList = mapList(result.body()!!.productsListBody)
                            productsEntityList?.let {
                                return@coroutineScope Resource(Status.SUCCESS, productsEntityList, "")
                            }
                        }
                    }else{
                        if(result.code() == 204){
                            return@coroutineScope Resource(Status.SUCCESS,  listOf<MyProductsEntity>(), "")
                        }
                    }
                }
                return@coroutineScope Resource(Status.ERROR, listOf<MyProductsEntity>(), "")

            } catch (e: UnknownHostException) {
                return@coroutineScope Resource(
                    Status.ERROR,
                    listOf<MyProductsEntity>(),
                    "Not Connection"
                )
            } catch (e: Throwable) {
                e.printStackTrace()
                return@coroutineScope Resource(Status.ERROR, listOf<MyProductsEntity>(), "")
            }
        }
    }

    override suspend fun updateMyProduct(
        storeProductId: Int,
        price: Double,
        stock: String
    ): Resource<Boolean> {
        return coroutineScope {
            val updateProductBody = UpdateProductBody(price,stock)
            val result = productsServices.updateMyProducts(
                PreferencesManager.getInstance().getEstablishmentSelected().storeId,
                storeProductId,
                updateProductBody
            )

            if(result.isSuccessful){
                if(result.code() == 200){
                    return@coroutineScope Resource(Status.SUCCESS, true, result.body()!!.message)
                }else{
                    return@coroutineScope Resource(Status.SUCCESS, false, result.body()!!.message)
                }
            }else{
                return@coroutineScope Resource(Status.ERROR, false, "")
            }
        }
    }

    private fun mapList(productsListBody: MyProductsItem?): List<MyProductsEntity>? {
        val productsList = ArrayList<MyProductsEntity>()
        productsListBody?.storeProducts?.forEach {
            productsList.add(productsEntityMapper.mapFromRemote(it))
        }
        return productsList
    }
}