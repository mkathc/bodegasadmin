package com.android.bodegasadmin.data.network.searchOrders

import android.content.Context
import android.util.Log
import com.android.bodegasadmin.data.network.searchOrders.model.DataItem
import com.android.bodegasadmin.data.network.searchOrders.mapper.SearchOrdersEntityMapper
import com.android.bodegasadmin.data.network.searchOrders.model.SearchListOrdersBody
import com.android.bodegasadmin.data.repository.establishmentOrders.model.EstablishmentOrdersEntity
import com.android.bodegasadmin.data.repository.searchOrders.model.SearchOrdersEntity
import com.android.bodegasadmin.data.repository.searchOrders.store.remote.SearchOrdersRemote
import com.android.bodegasadmin.domain.util.DateTimeHelper
import com.android.bodegasadmin.domain.util.Resource
import com.android.bodegasadmin.domain.util.Status
import com.google.gson.Gson
import kotlinx.coroutines.*
import java.io.InputStream
import java.io.InputStreamReader
import java.net.UnknownHostException
import java.nio.charset.StandardCharsets
import kotlin.coroutines.CoroutineContext

class SearchOrdersRemoteImpl constructor(
    private val establishmentOrdersServices: SearchOrdersService,
    private val establishmentOrdersEntityMapper: SearchOrdersEntityMapper,
    private val context: Context
) : SearchOrdersRemote, CoroutineScope {
    val CODE_NO_CONTENT:Int= 204
    private val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.IO + job

    override suspend fun getOrdersListByDni(
        establishmentId: Int,
        clientDni: String,
        clientName: String,
        orderStartDate: String,
        orderEndDate: String,
        orderState: String,
        page: String,
        size: String,
        sortBy: String
    ): Resource<List<SearchOrdersEntity>> {
        return coroutineScope {
            try {

                var emptyClientDni:String? = ""
                var emptyClientName:String? = ""
                var emptyOrderStartDate:String? = ""
                var emptyOrderEndDate:String? = ""
                var emptyOrderState:String? = ""
                var emptyPage:String? = ""
                var emptySize:String? = ""
                var emptySortBy:String? = ""

                emptyClientDni = if(clientDni.isEmpty()) null
                else clientDni

                emptyClientName = if(clientName.isEmpty()) null
                else clientName

                emptyOrderStartDate = if(orderStartDate.isEmpty()) null
                else DateTimeHelper.parseFinalDate(orderStartDate)

                emptyOrderEndDate = if(orderEndDate.isEmpty()) null
                else DateTimeHelper.parseFinalDate(orderEndDate)

                emptyOrderState = if(orderState.isEmpty()) null
                else orderState

                emptyPage = if(page.isEmpty()) null
                else page

                emptySize = if(size.isEmpty()) null
                else size

                emptySortBy = if(sortBy.isEmpty()) null
                else sortBy

                val result = establishmentOrdersServices.getOrdersListByDni(establishmentId,
                    emptyClientDni,
                    emptyClientName,
                    emptyOrderStartDate,
                    emptyOrderEndDate,
                    emptyOrderState,
                    emptyPage,
                    emptySize,
                    emptySortBy)

                if (result.isSuccessful) {
                    if(result.code() == CODE_NO_CONTENT) {
                        return@coroutineScope Resource(Status.SUCCESS,  listOf<SearchOrdersEntity>(), "") }
                    else{
                        result.body()?.let {
                            val customerOrdersEntityList = mapList(result.body()!!.data)
                            customerOrdersEntityList?.let {
                            return@coroutineScope Resource(Status.SUCCESS, customerOrdersEntityList, "")
                            }
                        }
                    }
                }
                return@coroutineScope Resource(Status.ERROR, listOf<SearchOrdersEntity>(), "")

              //  return@coroutineScope getMockedProducts()
            } catch (e: UnknownHostException) {
                return@coroutineScope Resource(
                    Status.ERROR,
                    listOf<SearchOrdersEntity>(),
                    "Not Connection"
                )
            } catch (e: Throwable) {
                e.printStackTrace()
                return@coroutineScope Resource(Status.ERROR, listOf<SearchOrdersEntity>(), "")
            }
        }
    }

    private fun mapList(customerOrdersListBody: DataItem?): List<SearchOrdersEntity>? {
        val customerOrdersList = ArrayList<SearchOrdersEntity>()
        customerOrdersListBody?.orders?.forEach {
            customerOrdersList.add(establishmentOrdersEntityMapper.mapFromRemote(it))
        }
        return customerOrdersList
    }

    private suspend fun getMockedProducts(): Resource<List<SearchOrdersEntity>> {
        return coroutineScope {
            delay(3000)
            try {
                val gson = Gson()
                val inputStream: InputStream = context.assets.open("mocked_establishment_orders_by_dni")
                val inputStreamReader = InputStreamReader(inputStream, StandardCharsets.UTF_8)
                val establishmentList: SearchListOrdersBody =
                    gson.fromJson(inputStreamReader, SearchListOrdersBody::class.java)
                val establishmentEntityList = mutableListOf<SearchOrdersEntity>()
                    establishmentList.data?.orders?.forEach { it ->
                    establishmentEntityList.add(establishmentOrdersEntityMapper.mapFromRemote(it))
                }
                return@coroutineScope Resource<List<SearchOrdersEntity>>(
                    Status.SUCCESS,
                    establishmentEntityList,
                    "mocked_establishment_orders_by_dni"
                )
            } catch (e: Throwable) {
                e.printStackTrace()
                return@coroutineScope Resource(Status.ERROR, listOf<SearchOrdersEntity>(), "")
            }
        }
    }
}