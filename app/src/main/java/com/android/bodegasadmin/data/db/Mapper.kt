package com.android.bodegasadmin.data.db

interface Mapper<T, V> {

    fun mapFromDb(type: T): V

    fun mapToDb(type: V): T

}