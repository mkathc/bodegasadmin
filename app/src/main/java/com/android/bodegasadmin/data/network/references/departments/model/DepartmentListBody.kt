package com.android.bodegasadmin.data.network.references.departments.model

import com.android.bodegasadmin.data.network.ResponseBodySuccess
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class DepartmentListBody : ResponseBodySuccess() {
    @SerializedName("data")
    @Expose
    var departmentListBody: DepartmentItem? = null
}

class DepartmentItem (
    @SerializedName("departamentosTotal")
    val departamentosTotal: Int,
    @SerializedName("departments")
    val departments: List<DepartmentResponse>
)