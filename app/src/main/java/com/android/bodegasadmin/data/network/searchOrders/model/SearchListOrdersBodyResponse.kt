package com.android.bodegasadmin.data.network.searchOrders.model

import com.android.bodegasadmin.data.network.establishmentOrders.model.OrderDetailItem
import com.google.gson.annotations.SerializedName

class SearchListOrdersBodyResponse (
    @SerializedName("creationDate")
    val creationDate: String,
    @SerializedName("creationUser")
    val creationUser: Int,
    @SerializedName("updateDate")
    val updateDate: String,
    @SerializedName("updateUser")
    val updateUser: String,
    @SerializedName("orderId")
    val orderId: Int,
    @SerializedName("establishment")
    val establishment: EstablishmentItem,
    @SerializedName("customer")
    val customer: CustomerItem,
    @SerializedName("orderDetail")
    val orderDetail:List<OrderDetailItem>,
    @SerializedName("deliveryType")
    val deliveryType: String,
    @SerializedName("shippingDateFrom")
    val shippingDateFrom: String? ="",
    @SerializedName("shippingDateUntil")
    val shippingDateUntil: String? ="",
    @SerializedName("status")
    val status: String,
    @SerializedName("total")
    val total: String,
    @SerializedName("customerAmount")
    val customerAmount: String? ="0.00",
    @SerializedName("deliveryCharge")
    val deliveryCharge: String? = "0.00",
    @SerializedName("paymentMethod")
    val paymentMethod: PaymentMethodItem
)

class EstablishmentItem (
    @SerializedName("creationDate")
    val creationDate: String,
    @SerializedName("creationUser")
    val creationUser: String,
    @SerializedName("updateDate")
    val updateDate: String,
    @SerializedName("updateUser")
    val updateUser: String,
    @SerializedName("establishmentId")
    val establishmentId: Int,
    @SerializedName("dni")
    val dni: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("lastNamePaternal")
    val lastNamePaternal: String,
    @SerializedName("lastNameMaternal")
    val lastNameMaternal: String,
    @SerializedName("phoneNumber")
    val phoneNumber: String
)


class CustomerItem (
    @SerializedName("creationDate")
    val creationDate: String,
    @SerializedName("creationUser")
    val creationUser: String,
    @SerializedName("updateDate")
    val updateDate: String,
    @SerializedName("updateUser")
    val updateUser: String,
    @SerializedName("customerId")
    val customerId: Int,
    @SerializedName("dni")
    val dni: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("lastNamePaternal")
    val lastNamePaternal: String,
    @SerializedName("lastNameMaternal")
    val lastNameMaternal: String,
    @SerializedName("phoneNumber")
    val phoneNumber: String,
    @SerializedName("address")
    val address: String,
    @SerializedName("latitude")
    val latitude: Double,
    @SerializedName("longitude")
    val longitude: Double,
    @SerializedName("urbanization")
    val urbanization: String
)

class StoreProductItem (
    @SerializedName("storeProductId")
    val storeProductId: Int,
    @SerializedName("productTemplate")
    val productTemplate: ProductTemplateItem,
    @SerializedName("price")
    val price: Double,
    @SerializedName("status")
    val status: String,
    @SerializedName("stock")
    val stock: String
)

class ProductTemplateItem (
    @SerializedName("productTemplateId")
    val productTemplateId: Int,
    @SerializedName("code")
    val code: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("unitMeasure")
    val unitMeasure: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("pathImage")
    val pathImage: String,
    @SerializedName("category")
    val category: CategoryItem,
    @SerializedName("class")
    val class_: ClassItem,
    @SerializedName("subCategory")
    val subCategory: SubCategoryItem,
    @SerializedName("subSubCategory")
    val subSubCategory: SubSubCategoryItem
)

class CategoryItem (
    @SerializedName("categoryId")
    val categoryId: Int,
    @SerializedName("description")
    val description: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("pathImage")
    val pathImage: String
)

class ClassItem (
    @SerializedName("classId")
    val classId: Int,
    @SerializedName("description")
    val description: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("pathImage")
    val pathImage: String
)

class SubCategoryItem (
    @SerializedName("subCategoryId")
    val subCategoryId: Int,
    @SerializedName("description")
    val description: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("pathImage")
    val pathImage: String
)

class SubSubCategoryItem (
    @SerializedName("subSubCategoryId")
    val subSubCategoryId: Int,
    @SerializedName("description")
    val description: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("pathImage")
    val pathImage: String
)

class PaymentMethodItem (
    @SerializedName("customerMessage")
    val customerMessage: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("establishmentMessage")
    val establishmentMessage: String ?= "",
    @SerializedName("paymentMethodId")
    val paymentMethodId: Int,
    @SerializedName("requestedAmount")
    val requestedAmount: Boolean
)