package com.android.bodegasadmin.data.network.establishment

import com.android.bodegasadmin.data.network.ResponseBodySuccess
import com.android.bodegasadmin.data.network.establishment.model.*
import com.android.bodegasadmin.data.repository.establishment.model.EstablishmentLoginEntity
import com.android.bodegasadmin.presentation.login.ForgetPassDTO
import com.android.bodegasadmin.presentation.profile.ChangePasswordDtoBody
import com.android.bodegasadmin.presentation.profile.UpdateEstablishmentBody
import com.android.bodegasadmin.presentation.profile.UpdateEstablishmentScheduledDTO
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.*

interface EstablishmentServices {

    @POST("/security/login")
    suspend fun loginEstablishmentToken(@Body userBody: UserBody): Response<EstablishmentLoginBody>

    @GET("/establishments/{establishment-id}")
    suspend fun loginUser(@Path("establishment-id") establishmentId: Int): Response<EstablishmentListBody>

    @POST("/security/createEstablishment")
    suspend fun registerEstablishment(
        @Body registerEstablishmentBodyRemote: RegisterEstablishmentBodyRemote
    ): Response<EstablishmentLoginBody>

    @PUT("/common/upload/{id}")
    suspend fun updatePhotoUser(
        @Path("id") id: Int,
        @Body file : RequestBody
    ): Response<FileResponse>

    @PUT("/establishments/{establishment-id}")
    suspend fun updateEstablishment(
        @Path("establishment-id") id: Int,
        @Body updateEstablishmentDTO : UpdateEstablishmentBody
    ): Response<EstablishmentListBody>

    @PUT("/security/changepassword")
    suspend fun updatePassword(
        @Body changePasswordDto : ChangePasswordDtoBody
    ): Response<ResponseBodySuccess>

    @POST("/common/forgetpassword")
    suspend fun recoveryPassword(
        @Body changePasswordDto : ForgetPassDTO
    ): Response<ResponseBodySuccess>

    @PUT("/establishments/{establishment-id}/scheduled")
    suspend fun updateScheduled(
        @Path("establishment-id") id: Int,
        @Body updateEstablishmentScheduledDTO : UpdateEstablishmentScheduledDTO
    ): Response<ResponseBodySuccess>
}