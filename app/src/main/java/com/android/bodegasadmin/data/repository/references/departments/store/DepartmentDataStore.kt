package com.android.bodegasadmin.data.repository.references.departments.store

import com.android.bodegasadmin.data.repository.references.departments.model.DepartmentEntity
import com.android.bodegasadmin.domain.util.Resource

interface DepartmentDataStore {
    suspend fun getDepartments(
    ): Resource<List<DepartmentEntity>>
}