package com.android.bodegasadmin.data.network.references.district

import com.android.bodegasadmin.data.network.references.district.model.DistrictListBody
import retrofit2.http.GET
import retrofit2.Response

interface DistrictService {
    @GET("/districts")
    suspend fun getDistricts() : Response<DistrictListBody>
}
