package com.android.bodegasadmin.data.repository.my_products.mapper

import com.android.bodegasadmin.data.repository.Mapper
import com.android.bodegasadmin.data.repository.my_products.model.MyProductsEntity
import com.android.bodegasadmin.data.repository.products_template.model.ProductsEntity
import com.android.bodegasadmin.domain.my_products.MyProducts


class MyProductsMapper : Mapper<MyProductsEntity, MyProducts> {

    override fun mapFromEntity(type: MyProductsEntity): MyProducts {
        return MyProducts(
            type.storeProductId,
            type.price,
            type.status,
            type.code,
            type.name,
            type.description,
            type.unitMeasure,
            type.stock,
            type.imageProduct
        )
    }

    override fun mapToEntity(type: MyProducts): MyProductsEntity {
        return MyProductsEntity(
            type.storeProductId,
            type.price,
            type.status,
            type.code,
            type.name,
            type.description,
            type.unitMeasure,
            type.stock,
            type.imageProduct
        )
    }

}