package com.android.bodegasadmin.data.network.references.district

import android.content.Context
import com.android.bodegasadmin.data.network.references.district.mapper.DistrictEntityMapper
import com.android.bodegasadmin.data.network.references.district.model.DistrictItem
import com.android.bodegasadmin.data.network.references.district.model.DistrictListBody
import com.android.bodegasadmin.data.repository.references.districts.model.DistrictEntity
import com.android.bodegasadmin.data.repository.references.districts.store.remote.DistrictRemote
import com.android.bodegasadmin.domain.util.Resource
import com.android.bodegasadmin.domain.util.Status
import com.google.gson.Gson
import kotlinx.coroutines.*
import java.io.InputStream
import java.io.InputStreamReader
import java.net.UnknownHostException
import java.nio.charset.StandardCharsets
import kotlin.coroutines.CoroutineContext


class DistrictRemoteImpl constructor(private val districtServices: DistrictService,
                                     private val districtEntityMapper: DistrictEntityMapper,
                                     private val context: Context
) : DistrictRemote, CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.IO + job

    override suspend fun getDistricts(
        id: String,
        name: String,
        province_id: String,
        department_id: String
    ): Resource<List<DistrictEntity>> {
        return coroutineScope {
            try{
                /*val result = districtServices.getDistricts()

                if (result.isSuccessful) {
                    result.body()?.let {
                        val districtEntityList = mapList(result.body()!!.districtListBody)
                        districtEntityList?.let {
                            return@coroutineScope Resource(Status.SUCCESS, districtEntityList, "")
                        }
                    }
                }
                return@coroutineScope Resource(Status.ERROR, listOf<ProvinceEntity>(), "")*/
                return@coroutineScope getMockedDistricts()
            }
            catch(e: UnknownHostException){
                return@coroutineScope Resource(
                    Status.ERROR,
                    listOf<DistrictEntity>(),
                    "Not Connection"
                )
            }
            catch(e: Throwable){
                e.printStackTrace()
                return@coroutineScope Resource(Status.ERROR, listOf<DistrictEntity>(),"")
            }
        }
    }

    private fun mapList(districtListBody: DistrictItem?): List<DistrictEntity>? {
        val districtList = ArrayList<DistrictEntity>()
        districtListBody?.districts?.forEach{
            districtList.add(districtEntityMapper.mapFromRemote(it))
        }
        return districtList
    }

    private suspend fun getMockedDistricts(): Resource<List<DistrictEntity>> {
        return coroutineScope {
            delay(1000)
            try{
                val gson = Gson()
                val inputStream: InputStream = context.assets.open("peru_distritos")
                val inputStreamReader = InputStreamReader(inputStream, StandardCharsets.UTF_8)
                val districtList: DistrictListBody = gson.fromJson(inputStreamReader, DistrictListBody::class.java)
                val districtEntityList = mutableListOf<DistrictEntity>()
                districtList.districtListBody?.districts?.forEach{ it -> districtEntityList.add(districtEntityMapper.mapFromRemote(it))}

                return@coroutineScope Resource<List<DistrictEntity>>(
                    Status.SUCCESS,
                    districtEntityList,
                    "Mocked classes list"
                )
            }
            catch (e: Throwable){
                e.printStackTrace()
                return@coroutineScope Resource(Status.ERROR, listOf<DistrictEntity>(), "")
            }
        }
    }
}

