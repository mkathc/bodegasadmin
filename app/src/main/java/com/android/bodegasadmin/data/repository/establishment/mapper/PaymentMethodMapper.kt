package com.android.bodegasadmin.data.repository.establishment.mapper

import com.android.bodegasadmin.data.repository.Mapper
import com.android.bodegasadmin.data.repository.establishment.model.PaymentMethodEntity
import com.android.bodegasadmin.domain.paymentMethod.PaymentMethod

class PaymentMethodMapper : Mapper<PaymentMethodEntity, PaymentMethod> {

    override fun mapFromEntity(type: PaymentMethodEntity): PaymentMethod {
        return PaymentMethod(
            type.paymentMethodId,
            type.requestedAmount,
            type.description,
            type.customerMessage
        )
    }

    override fun mapToEntity(type: PaymentMethod): PaymentMethodEntity {
        return PaymentMethodEntity(
            type.paymentMethodId,
            type.requestedAmount,
            type.description,
            type.customerMessage
        )
    }

}