package com.android.bodegasadmin.data.repository.references.districts

import com.android.bodegasadmin.data.repository.references.districts.mapper.DistrictMapper
import com.android.bodegasadmin.data.repository.references.districts.model.DistrictEntity
import com.android.bodegasadmin.data.repository.references.districts.store.remote.DistrictRemoteDataStorage
import com.android.bodegasadmin.domain.references.district.District
import com.android.bodegasadmin.domain.references.district.DistrictRepository
import com.android.bodegasadmin.domain.util.Resource
import com.android.bodegasadmin.domain.util.Status
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.coroutineScope
import kotlin.coroutines.CoroutineContext

class DistrictDataRepository (
    private val districtRemoteDataStore: DistrictRemoteDataStorage,
    private val districtMapper: DistrictMapper
) : DistrictRepository, CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.IO + job

    override suspend fun getDistricts(id: String, name: String, province_id: String, department_id: String): Resource<List<District>> {
        return coroutineScope {
            val districtList = districtRemoteDataStore.getDistricts(id, name, province_id, department_id)
            var resource : Resource<List<District>> = Resource(districtList.status, mutableListOf(), districtList.message)
            if(districtList.status == Status.SUCCESS)  {
                val mutableListdistricts = mapClassList(districtList.data)
                resource = Resource(Status.SUCCESS, mutableListdistricts, districtList.message)
            }
            else {
                if(districtList.status == Status.ERROR) {
                    resource = Resource(districtList.status, mutableListOf(), districtList.message)
                }
            }
            return@coroutineScope resource
        }
    }

    private fun mapClassList(districtsEntityList: List<DistrictEntity>) : List<District> {
        val districtList = mutableListOf<District>()
        districtsEntityList.forEach{
            districtList.add(districtMapper.mapFromEntity(it))
        }
        return districtList
    }
}