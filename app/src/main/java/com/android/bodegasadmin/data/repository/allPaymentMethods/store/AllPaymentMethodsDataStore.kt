package com.android.bodegasadmin.data.repository.allPaymentMethods.store

import com.android.bodegasadmin.data.repository.allPaymentMethods.model.AllPaymentMethodsEntity
import com.android.bodegasadmin.domain.util.Resource

interface AllPaymentMethodsDataStore {

    suspend fun getAllPaymentMethods(): Resource<List<AllPaymentMethodsEntity>>

}