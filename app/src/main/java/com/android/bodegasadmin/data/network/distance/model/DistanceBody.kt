package com.android.bodegasadmin.data.network.distance.model

import com.android.bodegasadmin.data.network.ResponseBodySuccess
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class DistanceBody : ResponseBodySuccess() {
    @SerializedName("data")
    @Expose
    var data: DistanceItem? = null
}

class DistanceItem(
    @SerializedName("distance")
    @Expose
    var distance: DistanceResponse
)