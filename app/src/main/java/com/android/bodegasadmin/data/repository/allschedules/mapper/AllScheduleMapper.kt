package com.android.bodegasadmin.data.repository.allschedules.mapper

import com.android.bodegasadmin.data.repository.Mapper
import com.android.bodegasadmin.data.repository.allschedules.model.AllScheduleEntity
import com.android.bodegasadmin.data.repository.establishment.model.ScheduleEntity
import com.android.bodegasadmin.domain.allschedule.AllSchedule
import com.android.bodegasadmin.domain.schedule.Schedule


class AllScheduleMapper : Mapper<AllScheduleEntity, AllSchedule> {

    override fun mapFromEntity(type: AllScheduleEntity): AllSchedule {
        return AllSchedule(
            type.scheduleId,
            type.range,
            type.type,
            type.startTime,
            type.endTime
        )
    }

    override fun mapToEntity(type: AllSchedule): AllScheduleEntity {
        return AllScheduleEntity(
            type.scheduleId,
            type.range,
            type.type,
            type.startTime,
            type.endTime
        )
    }

}