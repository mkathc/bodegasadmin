package com.android.bodegasadmin.data.network.my_products.model

import com.android.bodegasadmin.data.network.ResponseBodySuccess
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class MyProductsListBody : ResponseBodySuccess() {
    @SerializedName("data")
    @Expose
    var productsListBody: MyProductsItem? = null
}

class MyProductsItem(
    @SerializedName("total")
    val productsTotal: Int,
    @SerializedName("storeProducts")
    val storeProducts: List<MyProductsResponse>
)
