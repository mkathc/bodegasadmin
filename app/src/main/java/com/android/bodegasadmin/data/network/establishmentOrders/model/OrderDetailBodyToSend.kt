package com.android.bodegasadmin.data.network.establishmentOrders.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class OrderDetailBodyToSend (
    @SerializedName("orderDetailId")
    val orderDetailId: Int,
    @SerializedName("state")
    val state: String
)