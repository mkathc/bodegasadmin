package com.android.bodegasadmin.data.db.establishment.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.android.bodegasadmin.data.db.establishment.dao.EstablishmentConstants

@Entity(tableName = EstablishmentConstants.TABLE_NAME)
data class ScheduleDb(
    val range: String,
    val startTime: String ? = "",
    val endTime: String? = ""
) {
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0
}
