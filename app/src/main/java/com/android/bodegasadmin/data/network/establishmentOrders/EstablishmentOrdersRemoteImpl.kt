package com.android.bodegasadmin.data.network.establishmentOrders

import android.content.Context
import android.util.Log
import com.android.bodegasadmin.data.PreferencesManager
import com.android.bodegasadmin.data.network.establishmentOrders.mapper.EstablishmentOrdersEntityMapper
import com.android.bodegasadmin.data.network.establishmentOrders.mapper.OrderDetailBodyEntityMapper
import com.android.bodegasadmin.data.network.establishmentOrders.model.*
import com.android.bodegasadmin.data.repository.establishmentOrders.model.EstablishmentOrdersEntity
import com.android.bodegasadmin.data.repository.establishmentOrders.model.OrderDetailBodyEntity
import com.android.bodegasadmin.data.repository.establishmentOrders.store.remote.EstablishmentOrdersRemote
import com.android.bodegasadmin.domain.util.Resource
import com.android.bodegasadmin.domain.util.Status
import com.google.gson.Gson
import kotlinx.coroutines.*
import java.io.InputStream
import java.io.InputStreamReader
import java.net.UnknownHostException
import java.nio.charset.StandardCharsets
import kotlin.coroutines.CoroutineContext

class EstablishmentOrdersRemoteImpl constructor(
    private val establishmentOrdersServices: EstablishmentOrdersService,
    private val establishmentOrdersEntityMapper: EstablishmentOrdersEntityMapper,
    private val orderDetailBodyEntityMapper: OrderDetailBodyEntityMapper,
    private val context: Context
) : EstablishmentOrdersRemote, CoroutineScope {
    val CODE_NO_CONTENT: Int = 204
    private val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.IO + job

    override suspend fun getOrdersListByEstablishmentAndState(
        establishmentId: Int,
        orderState: String
    ): Resource<List<EstablishmentOrdersEntity>> {
        return coroutineScope {
           try {
                val result = establishmentOrdersServices.getOrdersListByEstablishmentAndState(
                    establishmentId,
                    "-creationDate",
                    orderState
                )

                if (result.isSuccessful) {
                    if (result.code() == CODE_NO_CONTENT) {
                        return@coroutineScope Resource(
                            Status.SUCCESS,
                            listOf<EstablishmentOrdersEntity>(),
                            ""
                        )
                    } else {
                        result.body()?.let {
                            val customerOrdersEntityList = mapList(result.body()!!.data)
                            customerOrdersEntityList?.let {
                                return@coroutineScope Resource(
                                    Status.SUCCESS,
                                    customerOrdersEntityList,
                                    ""
                                )
                            }
                        }
                    }
                }
                return@coroutineScope Resource(
                    Status.ERROR,
                    listOf<EstablishmentOrdersEntity>(),
                    ""
                )


            }
           //
            catch (e: UnknownHostException) {
                return@coroutineScope Resource(
                    Status.ERROR,
                    listOf<EstablishmentOrdersEntity>(),
                    "Not Connection"
                )
            } catch (e: Throwable) {
                e.printStackTrace()
                return@coroutineScope Resource(
                    Status.ERROR,
                    listOf<EstablishmentOrdersEntity>(),
                    ""
                )
            }
           // return@coroutineScope getMockedProducts()
        }
    }


    private fun mapList(customerOrdersListBody: DataItem?): List<EstablishmentOrdersEntity>? {
        val customerOrdersList = ArrayList<EstablishmentOrdersEntity>()
        customerOrdersListBody?.orders?.forEach {
            customerOrdersList.add(establishmentOrdersEntityMapper.mapFromRemote(it))
        }
        return customerOrdersList
    }

    override suspend fun updateStateOrder(
        deliveryCharge: Double,
        orderId: Int,
        state: String,
        list: List<OrderDetailBodyEntity>
    ): Resource<Boolean> {
        return coroutineScope {
            val sendListOrder = mutableListOf<OrderDetailBodyToSend>()
            list.forEach {
                sendListOrder.add(orderDetailBodyEntityMapper.mapFromRemote(it))
            }

            if(state == "A") {
                val orderBodyToUpdate = OrderBodyToUpdateWithDeliveryCharge(deliveryCharge, state, PreferencesManager.getInstance().getEstablishmentSelected().documentNumber, sendListOrder)
                val result = establishmentOrdersServices.updateStateOrderWithDeliveryCharge(PreferencesManager.getInstance().getEstablishmentSelected().storeId, orderId, orderBodyToUpdate)
                if(result.isSuccessful) {
                    val body = result.body()!!.success
                    if(body){
                        return@coroutineScope Resource(Status.SUCCESS,  true, "")
                    }else{
                        return@coroutineScope Resource(Status.SUCCESS, false, result.body()!!.message)
                    }
                }
                else {
                    return@coroutineScope Resource(Status.ERROR,  false, "")
                }
            }

            else {
                val orderBodyToUpdate = OrderBodyToUpdate(state, PreferencesManager.getInstance().getEstablishmentSelected().documentNumber, sendListOrder)
                val result = establishmentOrdersServices.updateStateOrder(PreferencesManager.getInstance().getEstablishmentSelected().storeId, orderId, orderBodyToUpdate)
                if(result.isSuccessful) {
                    val body = result.body()!!.success
                    if(body){
                        return@coroutineScope Resource(Status.SUCCESS,  true, "")
                    }else{
                        return@coroutineScope Resource(Status.SUCCESS, false, result.body()!!.message)
                    }
                }
                else {
                    return@coroutineScope Resource(Status.ERROR,  false, "")
                }
            }

          //  val orderBodyToUpdate = OrderBodyToUpdate(
           //     state, PreferencesManager.getInstance().getEstablishmentSelected().documentNumber, sendListOrder
           // )
         //   val result = establishmentOrdersServices.updateStateOrder(
          //      PreferencesManager.getInstance().getEstablishmentSelected().storeId, orderId, orderBodyToUpdate)
         /*   if(result.isSuccessful){
                val body = result.body()!!.success
                if(body){
                    return@coroutineScope Resource(Status.SUCCESS,  true, "")
                }else{
                    return@coroutineScope Resource(Status.SUCCESS, false, result.body()!!.message)
                }
            }else{
                return@coroutineScope Resource(Status.ERROR,  false, "")
            }*/
        }    }

    private suspend fun getMockedProducts(): Resource<List<EstablishmentOrdersEntity>> {
        return coroutineScope {
            delay(3000)
            try {
                val gson = Gson()
                val inputStream: InputStream =
                    context.assets.open("mocked_establishment_orders_estate_I")
                val inputStreamReader = InputStreamReader(inputStream, StandardCharsets.UTF_8)
                val establishmentList: EstablishmentListOrdersBody =
                    gson.fromJson(inputStreamReader, EstablishmentListOrdersBody::class.java)
                val establishmentEntityList = mutableListOf<EstablishmentOrdersEntity>()
                establishmentList.data?.orders?.forEach { it ->
                    establishmentEntityList.add(establishmentOrdersEntityMapper.mapFromRemote(it))
                    Log.e("debug", "order id: " + it.orderId + " , estatus:" + it.status)
                }
                return@coroutineScope Resource<List<EstablishmentOrdersEntity>>(
                    Status.SUCCESS,
                    establishmentEntityList,
                    "mocked_historialpedidos"
                )
            } catch (e: Throwable) {
                e.printStackTrace()
                return@coroutineScope Resource(
                    Status.ERROR,
                    listOf<EstablishmentOrdersEntity>(),
                    ""
                )
            }

        }
    }
}
