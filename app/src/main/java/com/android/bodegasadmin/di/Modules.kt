package com.android.bodegasadmin.di

import android.content.Context
import androidx.room.Room
import com.android.bodegasadmin.data.network.establishmentOrders.EstablishmentOrdersRemoteImpl
import com.android.bodegasadmin.data.network.establishmentOrders.EstablishmentOrdersServiceFactory
import com.android.bodegasadmin.data.network.establishmentOrders.mapper.EstablishmentDetailItemEntityMapper
import com.android.bodegasadmin.data.network.establishmentOrders.mapper.EstablishmentOrdersEntityMapper

import com.android.bodegasadmin.presentation.ordersHistory.mapper.EstablishmentOrderViewMapper
import com.android.bodegasadmin.BuildConfig
import com.android.bodegasadmin.data.PreferencesManager
import com.android.bodegasadmin.data.db.AppDatabase
import com.android.bodegasadmin.data.db.establishment.EstablishmentDbImpl
import com.android.bodegasadmin.data.db.establishment.mapper.EstablishmentDbMapper
import com.android.bodegasadmin.data.network.allPaymentMethods.AllPaymentMethodsRemoteImpl
import com.android.bodegasadmin.data.network.allPaymentMethods.AllPaymentMethodsServiceFactory
import com.android.bodegasadmin.data.network.allPaymentMethods.mapper.AllPaymentMethodsEntityMapper
import com.android.bodegasadmin.data.network.allschedules.AllSchedulesRemoteImpl
import com.android.bodegasadmin.data.network.allschedules.AllSchedulesServiceFactory
import com.android.bodegasadmin.data.network.allschedules.mapper.AllSchedulesEntityMapper
import com.android.bodegasadmin.data.network.distance.DistanceRemoteImpl
import com.android.bodegasadmin.data.network.distance.DistanceServiceFactory
import com.android.bodegasadmin.data.network.distance.mapper.DistanceEntityMapper
import com.android.bodegasadmin.data.network.establishment.EstablishmentRemoteImpl
import com.android.bodegasadmin.data.network.establishment.EstablishmentServiceFactory
import com.android.bodegasadmin.data.network.establishment.mapper.*
import com.android.bodegasadmin.data.network.establishmentOrders.mapper.OrderDetailBodyEntityMapper
import com.android.bodegasadmin.data.network.my_products.MyProductsRemoteImpl
import com.android.bodegasadmin.data.network.my_products.MyProductsServiceFactory
import com.android.bodegasadmin.data.network.my_products.mapper.MyProductsEntityMapper
import com.android.bodegasadmin.data.network.products_template.ProductsRemoteImpl
import com.android.bodegasadmin.data.network.products_template.ProductsServiceFactory
import com.android.bodegasadmin.data.network.products_template.mapper.ProductsEntityMapper
import com.android.bodegasadmin.data.network.references.departments.DepartmentRemoteImpl
import com.android.bodegasadmin.data.network.references.departments.DepartmentServiceFactory
import com.android.bodegasadmin.data.network.references.departments.mapper.DepartmentEntityMapper
import com.android.bodegasadmin.data.network.references.district.DistrictRemoteImpl
import com.android.bodegasadmin.data.network.references.district.DistrictServiceFactory
import com.android.bodegasadmin.data.network.references.district.mapper.DistrictEntityMapper
import com.android.bodegasadmin.data.network.references.provinces.ProvinceRemoteImpl
import com.android.bodegasadmin.data.network.references.provinces.ProvinceServiceFactory
import com.android.bodegasadmin.data.network.references.provinces.mapper.ProvinceEntityMapper
import com.android.bodegasadmin.data.network.searchOrders.SearchOrdersRemoteImpl
import com.android.bodegasadmin.data.network.searchOrders.SearchOrdersServiceFactory
import com.android.bodegasadmin.data.network.searchOrders.mapper.SearchDetailItemEntityMapper
import com.android.bodegasadmin.data.network.searchOrders.mapper.SearchOrdersEntityMapper
import com.android.bodegasadmin.data.repository.allPaymentMethods.AllPaymentMethodsDataRepository
import com.android.bodegasadmin.data.repository.allPaymentMethods.mapper.AllPaymentMethodsMapper
import com.android.bodegasadmin.data.repository.allPaymentMethods.store.remote.AllPaymentMethodsRemote
import com.android.bodegasadmin.data.repository.allPaymentMethods.store.remote.AllPaymentMethodsRemoteDataStore
import com.android.bodegasadmin.data.repository.allschedules.AllSchedulesDataRepository
import com.android.bodegasadmin.data.repository.allschedules.mapper.AllScheduleMapper
import com.android.bodegasadmin.data.repository.allschedules.store.remote.AllSchedulesRemote
import com.android.bodegasadmin.data.repository.allschedules.store.remote.AllSchedulesRemoteDataStore
import com.android.bodegasadmin.data.repository.distance.DistanceDataRepository
import com.android.bodegasadmin.data.repository.distance.mapper.DistanceMapper
import com.android.bodegasadmin.data.repository.distance.model.DistanceEntity
import com.android.bodegasadmin.data.repository.distance.store.remote.DistanceRemote
import com.android.bodegasadmin.data.repository.distance.store.remote.DistanceRemoteDataStore
import com.android.bodegasadmin.data.repository.establishment.EstablishmentDataRepository
import com.android.bodegasadmin.data.repository.establishment.mapper.*
import com.android.bodegasadmin.data.repository.establishment.model.PaymentMethodEntity
import com.android.bodegasadmin.data.repository.establishment.store.db.EstablishmentDb
import com.android.bodegasadmin.data.repository.establishment.store.db.EstablishmentDbDataStore
import com.android.bodegasadmin.data.repository.establishment.store.remote.EstablishmentRemote
import com.android.bodegasadmin.data.repository.establishment.store.remote.EstablishmentRemoteDataStore
import com.android.bodegasadmin.data.repository.establishmentOrders.EstablishmentOrdersDataRepository
import com.android.bodegasadmin.data.repository.establishmentOrders.mapper.EstablishmentDetailItemMapper
import com.android.bodegasadmin.data.repository.establishmentOrders.mapper.EstablishmentOrdersMapper
import com.android.bodegasadmin.data.repository.establishmentOrders.mapper.OrderDetailBodyMapper
import com.android.bodegasadmin.data.repository.establishmentOrders.store.remote.EstablishmentOrdersRemote
import com.android.bodegasadmin.data.repository.establishmentOrders.store.remote.EstablishmentOrdersRemoteDataStorage
import com.android.bodegasadmin.data.repository.my_products.mapper.MyProductsMapper
import com.android.bodegasadmin.data.repository.my_products.store.MyProductsDataRepository
import com.android.bodegasadmin.data.repository.my_products.store.remote.MyProductsRemote
import com.android.bodegasadmin.data.repository.my_products.store.remote.MyProductsRemoteDataStore
import com.android.bodegasadmin.data.repository.products_template.mapper.ProductsMapper
import com.android.bodegasadmin.data.repository.products_template.store.ProductsDataRepository
import com.android.bodegasadmin.data.repository.products_template.store.remote.ProductsRemote
import com.android.bodegasadmin.data.repository.products_template.store.remote.ProductsRemoteDataStore
import com.android.bodegasadmin.data.repository.references.departments.DepartmentDataRepository
import com.android.bodegasadmin.data.repository.references.departments.mapper.DepartmentMapper
import com.android.bodegasadmin.data.repository.references.departments.store.remote.DepartmentRemote
import com.android.bodegasadmin.data.repository.references.departments.store.remote.DepartmentRemoteDataStorage
import com.android.bodegasadmin.data.repository.references.districts.DistrictDataRepository
import com.android.bodegasadmin.data.repository.references.districts.mapper.DistrictMapper
import com.android.bodegasadmin.data.repository.references.districts.store.remote.DistrictRemote
import com.android.bodegasadmin.data.repository.references.districts.store.remote.DistrictRemoteDataStorage
import com.android.bodegasadmin.data.repository.references.provinces.ProvinceDataRepository
import com.android.bodegasadmin.data.repository.references.provinces.mapper.ProvinceMapper
import com.android.bodegasadmin.data.repository.references.provinces.store.remote.ProvinceRemote
import com.android.bodegasadmin.data.repository.references.provinces.store.remote.ProvinceRemoteDataStorage
import com.android.bodegasadmin.data.repository.searchOrders.SearchOrdersDataRepository
import com.android.bodegasadmin.data.repository.searchOrders.mapper.SearchDetailItemMapper
import com.android.bodegasadmin.data.repository.searchOrders.mapper.SearchOrdersMapper
import com.android.bodegasadmin.data.repository.searchOrders.store.remote.SearchOrdersRemote
import com.android.bodegasadmin.data.repository.searchOrders.store.remote.SearchOrdersRemoteDataStorage
import com.android.bodegasadmin.domain.allpaymentmethods.AllPaymentMethodRepository
import com.android.bodegasadmin.domain.allpaymentmethods.GetAllPaymentMethods
import com.android.bodegasadmin.domain.allschedule.AllScheduleRepository
import com.android.bodegasadmin.domain.allschedule.GetAllScheduleByType
import com.android.bodegasadmin.domain.distance.DistanceRepository
import com.android.bodegasadmin.domain.distance.GetDistance
import com.android.bodegasadmin.domain.establishment.EstablishmentRepository
import com.android.bodegasadmin.domain.establishment.login.LoginEstablishment
import com.android.bodegasadmin.domain.establishment.login.LoginEstablishmentToken
import com.android.bodegasadmin.domain.establishment.register.RegisterNewEstablishment
import com.android.bodegasadmin.domain.establishment.update.*
import com.android.bodegasadmin.domain.establishmentOrders.EstablishmentOrderRepository
import com.android.bodegasadmin.domain.establishmentOrders.GetOrdersListByEstablishmentAndState
import com.android.bodegasadmin.domain.establishmentOrders.UpdateOrderStatus
import com.android.bodegasadmin.domain.my_products.GetMyProducts
import com.android.bodegasadmin.domain.my_products.MyProductsRepository
import com.android.bodegasadmin.domain.my_products.UpdateMyProducts
import com.android.bodegasadmin.domain.products_template.AddProducts
import com.android.bodegasadmin.domain.products_template.GetProductsTemplate
import com.android.bodegasadmin.domain.products_template.ProductsRepository
import com.android.bodegasadmin.domain.references.department.DepartmentRepository
import com.android.bodegasadmin.domain.references.department.GetDepartments
import com.android.bodegasadmin.domain.references.district.DistrictRepository
import com.android.bodegasadmin.domain.references.district.GetDistricts
import com.android.bodegasadmin.domain.references.province.GetProvinces
import com.android.bodegasadmin.domain.references.province.ProvinceRepository
import com.android.bodegasadmin.domain.searchOrders.GetOrdersListByDni
import com.android.bodegasadmin.domain.searchOrders.SearchOrderRepository
import com.android.bodegasadmin.presentation.allPaymentMethods.GetAllPaymentMethodsViewModel
import com.android.bodegasadmin.presentation.allPaymentMethods.mapper.AllPaymentMethodsViewMapper
import com.android.bodegasadmin.presentation.allSchedules.GetAllSchedulesViewModel
import com.android.bodegasadmin.presentation.allSchedules.mapper.AllScheduleViewMapper
import com.android.bodegasadmin.presentation.establishment.mapper.*
import com.android.bodegasadmin.presentation.registerEstablishment.RegisterEstablishmentViewModel
import com.android.bodegasadmin.presentation.login.LoginViewModel
import com.android.bodegasadmin.presentation.myproducts.MyProductsViewModel
import com.android.bodegasadmin.presentation.myproducts.mapper.MyProductsViewMapper
import com.android.bodegasadmin.presentation.ordersHistory.*
import com.android.bodegasadmin.presentation.ordersHistory.mapper.DistanceViewMapper
import com.android.bodegasadmin.presentation.products_template.ProductsViewModel
import com.android.bodegasadmin.presentation.products_template.mapper.ProductsViewMapper
import com.android.bodegasadmin.presentation.ordersHistory.mapper.OrderDetailsViewMapper
import com.android.bodegasadmin.presentation.products_template.mapper.ProductsBodyViewMapper
import com.android.bodegasadmin.presentation.profile.ProfileViewModel
import com.android.bodegasadmin.presentation.references.mapper.DepartmentViewMapper
import com.android.bodegasadmin.presentation.references.mapper.DistrictViewMapper
import com.android.bodegasadmin.presentation.references.mapper.ProvinceViewMapper
import com.android.bodegasadmin.presentation.searchOrders.SearchOrdersViewModel
import com.android.bodegasadmin.presentation.searchOrders.mapper.SearchEstablishmentOrderViewMapper
import com.android.bodegasadmin.presentation.searchOrders.mapper.SearchOrderDetailsViewMapper
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module(override = false) {

    single { PreferencesManager() }
    single {
        Room.databaseBuilder(
            androidContext(),
            AppDatabase::class.java, "bodegasadmin.db"
        ).fallbackToDestructiveMigration()
            .build()
    }
}

val establishmentModule = module(override = false) {
    single { EstablishmentServiceFactory.makeService(BuildConfig.DEBUG,androidContext() ) }
    factory { EstablishmentDbMapper() }
    factory<EstablishmentDb> { EstablishmentDbImpl(get(), get()) }
    factory { EstablishmentDbDataStore(get()) }
    factory { ScheduleEntityMapper() }
    factory { PaymentMethodMapper() }
    factory { PaymentMethodsEntityMapper() }
    factory { PaymentMethodViewMapper() }
    factory { EstablishmentEntityMapper(get(), get()) }
    factory { RegisterEstablishmentEntityMapper() }
    factory<EstablishmentRemote> { EstablishmentRemoteImpl(get(), get(),get(),get()) }
    factory { EstablishmentRemoteDataStore(get()) }
    factory { ScheduleMapper() }
    factory { EstablishmentMapper(get(), get()) }
    factory { RegisterEstablishmentMapper() }
    factory<EstablishmentRepository> { EstablishmentDataRepository(get(), get(), get(), get(), get()) }
    single { LoginEstablishment(get()) }
    factory { ScheduleViewMapper() }
    factory { EstablishmentViewMapper(get(), get()) }
    factory { RegisterNewEstablishment(get()) }
    factory { RegisterEstablishmentBodyViewMapper() }
    factory { UpdatePhotoUser(get()) }
    factory { UpdatePassword(get()) }
    factory { RecoveryPassword(get()) }
    factory { UpdateDataEstablishment(get()) }
    factory { UpdateSchedules(get()) }
    factory { EstablishmentLoginEntityMapper() }
    factory { EstablishmentLoginMapper() }
    factory { LoginEstablishmentToken(get()) }
    factory { EstablishmentLoginTokenViewMapper() }
    viewModel { LoginViewModel(get(), get(), get(), get(), get()) }
    viewModel { ProfileViewModel(get(), get(), get(), get(), get()) }
    viewModel { RegisterEstablishmentViewModel(get(), get(), get(), get(), get(), get(), get(), get(), get(), get()) }
}


val departmentModule= module(override = false) {
    single { DepartmentServiceFactory.makeService(BuildConfig.DEBUG,androidContext()) }
    factory { DepartmentEntityMapper() }
    factory<DepartmentRemote> { DepartmentRemoteImpl(get(), get(), androidContext()) }
    factory { DepartmentRemoteDataStorage(get()) }
    factory { DepartmentMapper() }
    factory<DepartmentRepository> { DepartmentDataRepository(get(), get()) }
    single { GetDepartments(get()) }
    factory { DepartmentViewMapper() }
}

val provinceModule= module(override = false) {
    single { ProvinceServiceFactory.makeService(BuildConfig.DEBUG,androidContext()) }
    factory { ProvinceEntityMapper() }
    factory<ProvinceRemote> { ProvinceRemoteImpl(get(), get(), androidContext()) }
    factory { ProvinceRemoteDataStorage(get()) }
    factory { ProvinceMapper() }
    factory<ProvinceRepository> { ProvinceDataRepository(get(), get()) }
    single { GetProvinces(get()) }
    factory { ProvinceViewMapper() }
}

val districtModule= module(override = false) {
    single { DistrictServiceFactory.makeService(BuildConfig.DEBUG,androidContext()) }
    factory { DistrictEntityMapper() }
    factory<DistrictRemote> { DistrictRemoteImpl(get(), get(), androidContext()) }
    factory { DistrictRemoteDataStorage(get()) }
    factory { DistrictMapper() }
    factory<DistrictRepository> { DistrictDataRepository(get(), get()) }
    single { GetDistricts(get()) }
    factory { DistrictViewMapper() }
}

val allSchedulesModule = module(override = false){
    single { AllSchedulesServiceFactory.makeService(BuildConfig.DEBUG,androidContext()) }
    factory { AllSchedulesEntityMapper() }
    factory<AllSchedulesRemote> { AllSchedulesRemoteImpl(get(), get()) }
    factory { AllSchedulesRemoteDataStore(get()) }
    factory { AllScheduleMapper() }
    factory<AllScheduleRepository> { AllSchedulesDataRepository(get(), get()) }
    single { GetAllScheduleByType(get()) }
    factory { AllScheduleViewMapper() }
    viewModel { GetAllSchedulesViewModel(get(), get()) }
}

val allPaymentMethodsModule = module(override = false){
    single { AllPaymentMethodsServiceFactory.makeService(BuildConfig.DEBUG,androidContext()) }
    factory { AllPaymentMethodsEntityMapper() }
    factory<AllPaymentMethodsRemote> { AllPaymentMethodsRemoteImpl(get(), get()) }
    factory { AllPaymentMethodsRemoteDataStore(get()) }
    factory { AllPaymentMethodsMapper() }
    factory<AllPaymentMethodRepository> { AllPaymentMethodsDataRepository(get(), get()) }
    single { GetAllPaymentMethods(get()) }
    factory { AllPaymentMethodsViewMapper() }
    viewModel { GetAllPaymentMethodsViewModel(get(), get()) }
}

val establishmentOrders= module(override = false) {
    single { EstablishmentOrdersServiceFactory.makeService(BuildConfig.DEBUG,androidContext()) }
    factory { EstablishmentOrdersEntityMapper(get()) }
    factory { EstablishmentDetailItemEntityMapper() }
    factory { OrderDetailBodyEntityMapper() }
    factory<EstablishmentOrdersRemote> {  EstablishmentOrdersRemoteImpl(get(), get(),get(),androidContext()) }
    factory {  EstablishmentOrdersRemoteDataStorage(get()) }
    factory { EstablishmentDetailItemMapper() }
    factory { EstablishmentOrdersMapper( EstablishmentDetailItemMapper()) }
    factory { OrderDetailBodyMapper() }
    factory<EstablishmentOrderRepository> {  EstablishmentOrdersDataRepository(get(), get(),get()) }
    single { GetOrdersListByEstablishmentAndState(get()) }
    single { UpdateOrderStatus(get()) }
    factory {  EstablishmentOrdersViewModel(get(),get(), get()) }
    factory {  InformedOrdersViewModel(get(),get(), get()) }
    factory {  ApprovedOrdersViewModel(get(),get(), get()) }
    factory {  EstablishmentOrderViewMapper(get()) }
    factory {  OrderDetailsViewMapper() }
}

val distance= module(override = false) {
    single { DistanceServiceFactory.makeService(BuildConfig.DEBUG,androidContext()) }
    factory { DistanceEntityMapper() }
    factory<DistanceRemote> { DistanceRemoteImpl(get(), get()) }
    factory { DistanceRemoteDataStore(get()) }
    factory { DistanceMapper() }
    factory<DistanceRepository> { DistanceDataRepository(get(), get()) }
    single { GetDistance(get()) }
    factory { DistanceViewMapper() }
    viewModel { DistanceViewModel(get(), get()) }
}
val productsTemplateModule = module(override = false) {
    single { ProductsServiceFactory.makeService(BuildConfig.DEBUG,androidContext()) }
    factory { ProductsEntityMapper() }
    factory<ProductsRemote> { ProductsRemoteImpl(get(), get(), androidContext()) }
    factory { ProductsRemoteDataStore(get()) }
    factory { ProductsMapper() }
    factory<ProductsRepository> { ProductsDataRepository(get(), get()) }
    single { GetProductsTemplate(get()) }
    factory { ProductsViewMapper() }
    single { AddProducts(get()) }
    factory { ProductsBodyViewMapper() }
    viewModel { ProductsViewModel(get(), get(), get(), get()) }
}


val myProductsModule = module(override = false) {
    single { MyProductsServiceFactory.makeService(BuildConfig.DEBUG,androidContext()) }
    factory { MyProductsEntityMapper() }
    factory<MyProductsRemote> { MyProductsRemoteImpl(get(), get(), androidContext()) }
    factory { MyProductsRemoteDataStore(get()) }
    factory { MyProductsMapper() }
    factory<MyProductsRepository> { MyProductsDataRepository(get(), get()) }
    single { GetMyProducts(get()) }
    factory { MyProductsViewMapper() }
    single { UpdateMyProducts(get()) }
    viewModel { MyProductsViewModel(get(), get(),get()) }
}

val searchOrders= module(override = false) {
    single { SearchOrdersServiceFactory.makeService(BuildConfig.DEBUG,androidContext()) }
    factory { SearchOrdersEntityMapper(get()) }
    factory { SearchDetailItemEntityMapper() }
    factory<SearchOrdersRemote> {  SearchOrdersRemoteImpl(get(), get(), androidContext()) }
    factory {  SearchOrdersRemoteDataStorage(get()) }
    factory { SearchOrdersMapper( EstablishmentDetailItemMapper()) }
    factory<SearchOrderRepository> {  SearchOrdersDataRepository(get(), get()) }
    single { GetOrdersListByDni(get()) }
    factory {  SearchEstablishmentOrderViewMapper(get()) }
    factory {  SearchOrderDetailsViewMapper() }
    viewModel { SearchOrdersViewModel(get(), get()) }
}

val deliveryOrders = module(override = false) {
    viewModel { DeliveredOrdersViewModel(get(), get(), get()) }
}