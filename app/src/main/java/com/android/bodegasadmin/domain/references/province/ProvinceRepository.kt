package com.android.bodegasadmin.domain.references.province

import com.android.bodegasadmin.domain.util.Resource

interface ProvinceRepository {
    suspend fun getProvinces(
        id: String,
        name: String,
        department_id: String
    ): Resource<List<Province>>
}
