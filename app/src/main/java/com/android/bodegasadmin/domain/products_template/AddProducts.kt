package com.android.bodegasadmin.domain.products_template

import com.android.bodegasadmin.domain.util.Resource


class AddProducts(private val productsRepository: ProductsRepository) {

    suspend fun addProducts(productId:Int, productPrice:String): Resource<Boolean> {
        return productsRepository.addProducts(productId, productPrice)
    }

}
