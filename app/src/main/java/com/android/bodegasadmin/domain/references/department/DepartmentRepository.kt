package com.android.bodegasadmin.domain.references.department

import com.android.bodegasadmin.domain.util.Resource

interface DepartmentRepository {
    suspend fun getDepartments(
        id: String,
        name: String
    ): Resource<List<Department>>
}