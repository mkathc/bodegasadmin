package com.android.bodegasadmin.domain.establishment

import com.android.bodegasadmin.data.repository.establishment.model.EstablishmentEntity
import com.android.bodegasadmin.domain.establishment.register.RegisterEstablishmentBody
import com.android.bodegasadmin.domain.util.Resource
import com.android.bodegasadmin.presentation.login.ForgetPassDTO
import com.android.bodegasadmin.presentation.profile.ChangePasswordDtoBody
import com.android.bodegasadmin.presentation.profile.UpdateEstablishmentBody
import com.android.bodegasadmin.presentation.profile.UpdateEstablishmentScheduledDTO


interface EstablishmentRepository {

    suspend fun loginEstablishmentToken(email: String, password: String): Resource<EstablishmentLoginToken>

    suspend fun loginEstablishment(establishmentId: Int): Resource<Establishment>

    suspend fun cleanEstablishment()

    suspend fun registerEstablishment( registerEstablishmentBody: RegisterEstablishmentBody): Resource<EstablishmentLoginToken>

    suspend fun getEstablishment(): Resource<Establishment>

    suspend fun updatePhotoUser(customerId:Int, filePath:String): Resource<Boolean>

    suspend fun updateEstablishment(customerId: Int, body: UpdateEstablishmentBody): Resource<Establishment>

    suspend fun updatePassword(body: ChangePasswordDtoBody): Resource<Boolean>

    suspend fun recoveryPassword(body: ForgetPassDTO): Resource<Boolean>

    suspend fun updateSchedule( establishmentId:Int, body: UpdateEstablishmentScheduledDTO): Resource<Boolean>
}

