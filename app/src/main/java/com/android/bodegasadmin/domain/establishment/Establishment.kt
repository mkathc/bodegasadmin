package com.android.bodegasadmin.domain.establishment

import com.android.bodegasadmin.domain.paymentMethod.PaymentMethod
import com.android.bodegasadmin.domain.schedule.Schedule

data class Establishment(
    val establishmentId: Int,
    val address: String,
    val businessName: String,
    val cardNumber: String,
    val cardOperator: String,
    val departament: String,
    val district: String,
    val dni: String,
    val email: String,
    val lastNameMaternal: String,
    val lastNamePaternal: String,
    val latitude: Double,
    val longitude: Double,
    val name: String,
    val phoneNumber: String,
    val province: String,
    val ruc: String,
    val status: String,
    val urbanization: String,
    val delivery: String,
    val pathImage: String? ="",
    val shippingSchedule: List<Schedule>,
    val operationSchedule: List<Schedule>,
    val establishmentType: String,
    val deliveryCharge: Boolean,
    val paymentMethod : List<PaymentMethod>
)