package com.android.bodegasadmin.domain.establishment.login

import com.android.bodegasadmin.domain.establishment.Establishment
import com.android.bodegasadmin.domain.establishment.EstablishmentLoginToken
import com.android.bodegasadmin.domain.establishment.EstablishmentRepository
import com.android.bodegasadmin.domain.util.Resource


class LoginEstablishmentToken constructor(private val establishmentRepository: EstablishmentRepository) {

    suspend fun loginEstablishmentToken(email: String, password: String): Resource<EstablishmentLoginToken> {
        return establishmentRepository.loginEstablishmentToken(email, password)
    }
}