package com.android.bodegasadmin.domain.searchOrders

import com.android.bodegasadmin.domain.util.Resource
class GetOrdersListByDni(private val searchOrderRepository: SearchOrderRepository) {
    suspend fun getOrdersListByDni(
        establishmentId: Int,
        clientDni: String,
        clientName: String,
        orderStartDate: String,
        orderEndDate: String,
        orderState: String,
        page: String,
        size: String,
        sortBy: String) : Resource<List<SearchEstablishmentOrders>> {
        return searchOrderRepository.getOrdersListByDni(
            establishmentId,
            clientDni,
            clientName,
            orderStartDate,
            orderEndDate,
            orderState,
            page,
            size,
            sortBy
        )
    }
}