package com.android.bodegasadmin.domain.establishmentOrders

class OrderDetailBody (
    val orderDetailId: Int,
    val state: String
)