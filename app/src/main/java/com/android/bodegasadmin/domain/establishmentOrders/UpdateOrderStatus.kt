package com.android.bodegasadmin.domain.establishmentOrders

import com.android.bodegasadmin.domain.util.Resource


class UpdateOrderStatus(
    private val customerOrderRepository: EstablishmentOrderRepository
) {
    suspend fun updateStaterOrder(deliveryCharge: Double,orderId:Int, state:String, list: List<OrderDetailBody>): Resource<Boolean> {
        return customerOrderRepository.updateStateOrder(deliveryCharge,orderId, state, list)
    }

}