package com.android.bodegasadmin.domain.references.department

data class Department (
    val id: String,
    val name: String
)