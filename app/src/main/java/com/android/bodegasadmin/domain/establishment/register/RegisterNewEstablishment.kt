package com.android.bodegasadmin.domain.establishment.register

import com.android.bodegasadmin.domain.establishment.Establishment
import com.android.bodegasadmin.domain.establishment.EstablishmentLoginToken
import com.android.bodegasadmin.domain.establishment.EstablishmentRepository
import com.android.bodegasadmin.domain.util.Resource

class RegisterNewEstablishment constructor(private val establishmentRepository: EstablishmentRepository){

     suspend fun registerEstablishment( registerEstablishmentBody: RegisterEstablishmentBody): Resource<EstablishmentLoginToken> {
        return establishmentRepository.registerEstablishment(registerEstablishmentBody)
    }
}
