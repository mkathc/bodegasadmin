package com.android.bodegasadmin.domain.util

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}
