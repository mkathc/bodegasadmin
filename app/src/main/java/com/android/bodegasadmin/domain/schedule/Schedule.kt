package com.android.bodegasadmin.domain.schedule

data class Schedule(
    val range: String,
    val startTime: String ? ="",
    val endTime: String ? = "",
    val shippingScheduleId: Int
)