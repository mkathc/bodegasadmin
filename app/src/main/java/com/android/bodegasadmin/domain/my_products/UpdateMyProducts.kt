package com.android.bodegasadmin.domain.my_products

import com.android.bodegasadmin.domain.util.Resource
import java.util.*


class UpdateMyProducts(private val productsRepository: MyProductsRepository) {

    suspend fun updateMyProduct(
        storeProductId: Int,
        price:Double,
        stock:String
    ): Resource<Boolean> {
        return productsRepository.updateMyProduct(storeProductId, price, stock)
    }
}
