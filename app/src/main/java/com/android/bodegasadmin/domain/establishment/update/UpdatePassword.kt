package com.android.bodegasadmin.domain.establishment.update

import com.android.bodegasadmin.domain.establishment.EstablishmentRepository
import com.android.bodegasadmin.domain.util.Resource
import com.android.bodegasadmin.presentation.profile.ChangePasswordDtoBody


class UpdatePassword constructor(private val userRepository: EstablishmentRepository) {

    suspend fun updatePassword(body: ChangePasswordDtoBody): Resource<Boolean> {
        return userRepository.updatePassword(body)
    }
}