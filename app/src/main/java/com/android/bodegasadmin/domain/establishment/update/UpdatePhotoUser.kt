package com.android.bodegasadmin.domain.establishment.update

import com.android.bodegasadmin.domain.establishment.EstablishmentRepository
import com.android.bodegasadmin.domain.util.Resource


class UpdatePhotoUser constructor(private val userRepository: EstablishmentRepository) {

    suspend fun updatePhoto(customerId: Int, filePath: String): Resource<Boolean> {
        return userRepository.updatePhotoUser(customerId, filePath)
    }
}