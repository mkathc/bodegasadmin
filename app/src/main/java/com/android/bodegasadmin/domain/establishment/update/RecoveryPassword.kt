package com.android.bodegasadmin.domain.establishment.update

import com.android.bodegasadmin.domain.establishment.EstablishmentRepository
import com.android.bodegasadmin.domain.util.Resource
import com.android.bodegasadmin.presentation.login.ForgetPassDTO

class RecoveryPassword constructor(private val userRepository: EstablishmentRepository) {
    suspend fun recoveryPassword(body: ForgetPassDTO): Resource<Boolean> {
        return userRepository.recoveryPassword(body)
    }
}