package com.android.bodegasadmin.domain.references.district

import com.android.bodegasadmin.domain.util.Resource

interface DistrictRepository {
    suspend fun getDistricts(
        id: String,
        name: String,
        province_id: String,
        department_id: String
    ): Resource<List<District>>
}