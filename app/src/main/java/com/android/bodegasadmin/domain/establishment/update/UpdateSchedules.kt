package com.android.bodegasadmin.domain.establishment.update

import com.android.bodegasadmin.domain.establishment.EstablishmentRepository
import com.android.bodegasadmin.domain.util.Resource
import com.android.bodegasadmin.presentation.profile.UpdateEstablishmentScheduledDTO

class UpdateSchedules constructor(private val userRepository: EstablishmentRepository) {

    suspend fun updateSchedule(establishmentId:Int, body: UpdateEstablishmentScheduledDTO): Resource<Boolean> {
        return userRepository.updateSchedule(establishmentId, body)
    }
}